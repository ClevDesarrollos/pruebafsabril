﻿Public Class pantallaPass
    Dim columna As String
    Dim tabla As String
    Dim condicion As String
    Dim esquema As String = ObtenerEsquema()
    Dim codigoReporte As String = obtenerCodigoXML()
    Dim valores As String
    Dim pass As String

    Private Sub pantallaPass_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        columna = "PASS"
        tabla = esquema & ".U_CLEV_REP_ACCESOS"
        condicion = "WHERE REPORTE='" & codigoReporte & "'"
        valores = ""
        pass = DevolverDatoQuery(columna, tabla, condicion)
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If txtPass.Text = Desencriptar2(pass) Then
            If lblAnio.Text = "" And lblMes.Text = "" Then
                cierreMes.Show()
                Form1.Enabled = True
                Form1.Visible = False
                Me.Close()
            Else
                ejecutarComandosSQL("DELETE FROM VITALICIA.U_CLEV_ANA_CIERRE WHERE ANIO='" & lblAnio.Text & "' AND MES='" & lblMes.Text & "'")
                Form1.Enabled = True
                Form1.llenarUltimoMesCierre()
                MsgBox("Cierre del periodo " & lblMes.Text & "-" & lblAnio.Text & " anulado exitosamente")
                Me.Close()

            End If

        Else
            lblMensaje.Text = "Contraseña incorrecta"
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Form1.Enabled = True
        Me.Close()
    End Sub
End Class