﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class buscador
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(buscador))
        Me.dtgBuscador = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtParametro = New System.Windows.Forms.TextBox()
        Me.lblParametros = New System.Windows.Forms.Label()
        Me.lblbOrigen = New System.Windows.Forms.Label()
        CType(Me.dtgBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtgBuscador
        '
        Me.dtgBuscador.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgBuscador.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        Me.dtgBuscador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgBuscador.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgBuscador.Location = New System.Drawing.Point(12, 12)
        Me.dtgBuscador.Name = "dtgBuscador"
        Me.dtgBuscador.RowTemplate.Height = 24
        Me.dtgBuscador.Size = New System.Drawing.Size(514, 331)
        Me.dtgBuscador.TabIndex = 14
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.txtParametro)
        Me.Panel1.Controls.Add(Me.lblParametros)
        Me.Panel1.Location = New System.Drawing.Point(12, 349)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(514, 51)
        Me.Panel1.TabIndex = 16
        '
        'txtParametro
        '
        Me.txtParametro.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtParametro.Location = New System.Drawing.Point(181, 14)
        Me.txtParametro.Name = "txtParametro"
        Me.txtParametro.Size = New System.Drawing.Size(330, 22)
        Me.txtParametro.TabIndex = 3
        '
        'lblParametros
        '
        Me.lblParametros.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblParametros.Location = New System.Drawing.Point(6, 17)
        Me.lblParametros.Name = "lblParametros"
        Me.lblParametros.Size = New System.Drawing.Size(169, 17)
        Me.lblParametros.TabIndex = 4
        Me.lblParametros.Text = "Código:"
        Me.lblParametros.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblbOrigen
        '
        Me.lblbOrigen.AutoSize = True
        Me.lblbOrigen.Location = New System.Drawing.Point(582, 9)
        Me.lblbOrigen.Name = "lblbOrigen"
        Me.lblbOrigen.Size = New System.Drawing.Size(51, 17)
        Me.lblbOrigen.TabIndex = 15
        Me.lblbOrigen.Text = "Label1"
        Me.lblbOrigen.Visible = False
        '
        'buscador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(547, 412)
        Me.Controls.Add(Me.dtgBuscador)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblbOrigen)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "buscador"
        Me.Text = "Buscador"
        CType(Me.dtgBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtgBuscador As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtParametro As System.Windows.Forms.TextBox
    Friend WithEvents lblParametros As System.Windows.Forms.Label
    Friend WithEvents lblbOrigen As System.Windows.Forms.Label
End Class
