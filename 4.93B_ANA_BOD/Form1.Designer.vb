﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtCargo = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.radioTodos = New System.Windows.Forms.RadioButton()
        Me.radioNoAdmisible = New System.Windows.Forms.RadioButton()
        Me.radioAdmisible = New System.Windows.Forms.RadioButton()
        Me.checkResumen = New System.Windows.Forms.CheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnCierre = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.dtgBodegas = New System.Windows.Forms.DataGridView()
        Me.pictureArticulo = New System.Windows.Forms.PictureBox()
        Me.dtgCabecera = New System.Windows.Forms.DataGridView()
        Me.pictureBodega = New System.Windows.Forms.PictureBox()
        Me.checkBodega = New System.Windows.Forms.CheckBox()
        Me.checkArticulo = New System.Windows.Forms.CheckBox()
        Me.txtDescBodega = New System.Windows.Forms.Label()
        Me.txtDescArticulo = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtBodega = New System.Windows.Forms.TextBox()
        Me.txtArticulo = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dateReporte = New System.Windows.Forms.DateTimePicker()
        Me.radioDocumento = New System.Windows.Forms.RadioButton()
        Me.radioAuditoria = New System.Windows.Forms.RadioButton()
        Me.txtEntidad = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dateEntrega = New System.Windows.Forms.DateTimePicker()
        Me.dateEmision = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNit = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtResponsable = New System.Windows.Forms.TextBox()
        Me.lblFechaReporte = New System.Windows.Forms.Label()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.progress1 = New System.Windows.Forms.ProgressBar()
        Me.btnGenRep = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.btnGenExl = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtgReporte = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblCierre = New System.Windows.Forms.Label()
        Me.Panel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.dtgBodegas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureArticulo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBodega, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dtgReporte, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCargo
        '
        Me.txtCargo.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCargo.Location = New System.Drawing.Point(192, 72)
        Me.txtCargo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCargo.Name = "txtCargo"
        Me.txtCargo.Size = New System.Drawing.Size(510, 25)
        Me.txtCargo.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Controls.Add(Me.radioTodos)
        Me.Panel3.Controls.Add(Me.radioNoAdmisible)
        Me.Panel3.Controls.Add(Me.radioAdmisible)
        Me.Panel3.Controls.Add(Me.checkResumen)
        Me.Panel3.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(8, 6)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(235, 153)
        Me.Panel3.TabIndex = 170
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(4, 5)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(225, 24)
        Me.Label18.TabIndex = 149
        Me.Label18.Text = "TIPO DE REPORTE"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'radioTodos
        '
        Me.radioTodos.AutoSize = True
        Me.radioTodos.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioTodos.Location = New System.Drawing.Point(44, 83)
        Me.radioTodos.Name = "radioTodos"
        Me.radioTodos.Size = New System.Drawing.Size(180, 24)
        Me.radioTodos.TabIndex = 2
        Me.radioTodos.Text = "Admisibles y No Admisibles"
        Me.radioTodos.UseVisualStyleBackColor = True
        '
        'radioNoAdmisible
        '
        Me.radioNoAdmisible.AutoSize = True
        Me.radioNoAdmisible.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioNoAdmisible.Location = New System.Drawing.Point(44, 58)
        Me.radioNoAdmisible.Name = "radioNoAdmisible"
        Me.radioNoAdmisible.Size = New System.Drawing.Size(109, 24)
        Me.radioNoAdmisible.TabIndex = 1
        Me.radioNoAdmisible.Text = "No Admisibles"
        Me.radioNoAdmisible.UseVisualStyleBackColor = True
        '
        'radioAdmisible
        '
        Me.radioAdmisible.AutoSize = True
        Me.radioAdmisible.Checked = True
        Me.radioAdmisible.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioAdmisible.Location = New System.Drawing.Point(44, 32)
        Me.radioAdmisible.Name = "radioAdmisible"
        Me.radioAdmisible.Size = New System.Drawing.Size(90, 24)
        Me.radioAdmisible.TabIndex = 0
        Me.radioAdmisible.TabStop = True
        Me.radioAdmisible.Text = "Admisibles"
        Me.radioAdmisible.UseVisualStyleBackColor = True
        '
        'checkResumen
        '
        Me.checkResumen.AutoSize = True
        Me.checkResumen.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkResumen.Location = New System.Drawing.Point(44, 124)
        Me.checkResumen.Name = "checkResumen"
        Me.checkResumen.Size = New System.Drawing.Size(88, 24)
        Me.checkResumen.TabIndex = 155
        Me.checkResumen.Text = "Resumen"
        Me.checkResumen.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(4, 1)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(547, 24)
        Me.Label15.TabIndex = 149
        Me.Label15.Text = "PARÁMETROS DEL REPORTE"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(734, 46)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(139, 28)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Fecha del Reporte: "
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnCierre
        '
        Me.btnCierre.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCierre.BackColor = System.Drawing.SystemColors.Control
        Me.btnCierre.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnCierre.FlatAppearance.BorderSize = 2
        Me.btnCierre.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCierre.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCierre.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCierre.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCierre.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnCierre.Location = New System.Drawing.Point(936, 2)
        Me.btnCierre.Name = "btnCierre"
        Me.btnCierre.Size = New System.Drawing.Size(126, 65)
        Me.btnCierre.TabIndex = 173
        Me.btnCierre.Text = "Cierre de Mes"
        Me.btnCierre.UseVisualStyleBackColor = False
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.dtgBodegas)
        Me.Panel7.Controls.Add(Me.pictureArticulo)
        Me.Panel7.Controls.Add(Me.dtgCabecera)
        Me.Panel7.Controls.Add(Me.pictureBodega)
        Me.Panel7.Controls.Add(Me.Label15)
        Me.Panel7.Controls.Add(Me.checkBodega)
        Me.Panel7.Controls.Add(Me.checkArticulo)
        Me.Panel7.Controls.Add(Me.txtDescBodega)
        Me.Panel7.Controls.Add(Me.txtDescArticulo)
        Me.Panel7.Controls.Add(Me.Label17)
        Me.Panel7.Controls.Add(Me.Label24)
        Me.Panel7.Controls.Add(Me.txtBodega)
        Me.Panel7.Controls.Add(Me.txtArticulo)
        Me.Panel7.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel7.Location = New System.Drawing.Point(489, 6)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(552, 153)
        Me.Panel7.TabIndex = 153
        '
        'dtgBodegas
        '
        Me.dtgBodegas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgBodegas.Location = New System.Drawing.Point(475, -1)
        Me.dtgBodegas.Name = "dtgBodegas"
        Me.dtgBodegas.RowTemplate.Height = 24
        Me.dtgBodegas.Size = New System.Drawing.Size(38, 79)
        Me.dtgBodegas.TabIndex = 157
        Me.dtgBodegas.Visible = False
        '
        'pictureArticulo
        '
        Me.pictureArticulo.BackColor = System.Drawing.Color.White
        Me.pictureArticulo.Enabled = False
        Me.pictureArticulo.Image = CType(resources.GetObject("pictureArticulo.Image"), System.Drawing.Image)
        Me.pictureArticulo.Location = New System.Drawing.Point(217, 85)
        Me.pictureArticulo.Name = "pictureArticulo"
        Me.pictureArticulo.Size = New System.Drawing.Size(23, 19)
        Me.pictureArticulo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureArticulo.TabIndex = 156
        Me.pictureArticulo.TabStop = False
        '
        'dtgCabecera
        '
        Me.dtgCabecera.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgCabecera.Location = New System.Drawing.Point(475, 103)
        Me.dtgCabecera.Name = "dtgCabecera"
        Me.dtgCabecera.RowTemplate.Height = 24
        Me.dtgCabecera.Size = New System.Drawing.Size(58, 24)
        Me.dtgCabecera.TabIndex = 174
        Me.dtgCabecera.Visible = False
        '
        'pictureBodega
        '
        Me.pictureBodega.BackColor = System.Drawing.Color.White
        Me.pictureBodega.Enabled = False
        Me.pictureBodega.Image = CType(resources.GetObject("pictureBodega.Image"), System.Drawing.Image)
        Me.pictureBodega.Location = New System.Drawing.Point(217, 31)
        Me.pictureBodega.Name = "pictureBodega"
        Me.pictureBodega.Size = New System.Drawing.Size(23, 21)
        Me.pictureBodega.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureBodega.TabIndex = 156
        Me.pictureBodega.TabStop = False
        '
        'checkBodega
        '
        Me.checkBodega.AutoSize = True
        Me.checkBodega.Checked = True
        Me.checkBodega.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkBodega.Location = New System.Drawing.Point(100, 58)
        Me.checkBodega.Name = "checkBodega"
        Me.checkBodega.Size = New System.Drawing.Size(125, 21)
        Me.checkBodega.TabIndex = 155
        Me.checkBodega.Text = "Todas las Bodegas"
        Me.checkBodega.UseVisualStyleBackColor = True
        '
        'checkArticulo
        '
        Me.checkArticulo.AutoSize = True
        Me.checkArticulo.Checked = True
        Me.checkArticulo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkArticulo.Location = New System.Drawing.Point(100, 106)
        Me.checkArticulo.Name = "checkArticulo"
        Me.checkArticulo.Size = New System.Drawing.Size(123, 21)
        Me.checkArticulo.TabIndex = 155
        Me.checkArticulo.Text = "Todos los Artículos"
        Me.checkArticulo.UseVisualStyleBackColor = True
        '
        'txtDescBodega
        '
        Me.txtDescBodega.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescBodega.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescBodega.Location = New System.Drawing.Point(246, 31)
        Me.txtDescBodega.Name = "txtDescBodega"
        Me.txtDescBodega.Size = New System.Drawing.Size(297, 45)
        Me.txtDescBodega.TabIndex = 154
        '
        'txtDescArticulo
        '
        Me.txtDescArticulo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescArticulo.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescArticulo.Location = New System.Drawing.Point(246, 81)
        Me.txtDescArticulo.Name = "txtDescArticulo"
        Me.txtDescArticulo.Size = New System.Drawing.Size(293, 46)
        Me.txtDescArticulo.TabIndex = 153
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(26, 30)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(56, 20)
        Me.Label17.TabIndex = 150
        Me.Label17.Text = "Bodega"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(26, 81)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(58, 20)
        Me.Label24.TabIndex = 150
        Me.Label24.Text = "Artículo"
        '
        'txtBodega
        '
        Me.txtBodega.Enabled = False
        Me.txtBodega.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBodega.Location = New System.Drawing.Point(100, 30)
        Me.txtBodega.Name = "txtBodega"
        Me.txtBodega.Size = New System.Drawing.Size(143, 22)
        Me.txtBodega.TabIndex = 108
        '
        'txtArticulo
        '
        Me.txtArticulo.Enabled = False
        Me.txtArticulo.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArticulo.Location = New System.Drawing.Point(100, 82)
        Me.txtArticulo.Name = "txtArticulo"
        Me.txtArticulo.Size = New System.Drawing.Size(143, 22)
        Me.txtArticulo.TabIndex = 109
        '
        'Panel5
        '
        Me.Panel5.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label10)
        Me.Panel5.Controls.Add(Me.dateReporte)
        Me.Panel5.Controls.Add(Me.radioDocumento)
        Me.Panel5.Controls.Add(Me.radioAuditoria)
        Me.Panel5.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel5.Location = New System.Drawing.Point(251, 5)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(229, 155)
        Me.Panel5.TabIndex = 111
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(4, 2)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(219, 28)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "MES DEL REPORTE"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dateReporte
        '
        Me.dateReporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateReporte.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dateReporte.Location = New System.Drawing.Point(58, 53)
        Me.dateReporte.Margin = New System.Windows.Forms.Padding(4)
        Me.dateReporte.Name = "dateReporte"
        Me.dateReporte.Size = New System.Drawing.Size(148, 24)
        Me.dateReporte.TabIndex = 9
        '
        'radioDocumento
        '
        Me.radioDocumento.AutoSize = True
        Me.radioDocumento.Checked = True
        Me.radioDocumento.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioDocumento.Location = New System.Drawing.Point(11, 84)
        Me.radioDocumento.Name = "radioDocumento"
        Me.radioDocumento.Size = New System.Drawing.Size(168, 24)
        Me.radioDocumento.TabIndex = 0
        Me.radioDocumento.TabStop = True
        Me.radioDocumento.Text = "Por fecha de Documento"
        Me.radioDocumento.UseVisualStyleBackColor = True
        '
        'radioAuditoria
        '
        Me.radioAuditoria.AutoSize = True
        Me.radioAuditoria.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioAuditoria.Location = New System.Drawing.Point(11, 112)
        Me.radioAuditoria.Name = "radioAuditoria"
        Me.radioAuditoria.Size = New System.Drawing.Size(151, 24)
        Me.radioAuditoria.TabIndex = 0
        Me.radioAuditoria.Text = "Por fecha de Auditoría"
        Me.radioAuditoria.UseVisualStyleBackColor = True
        '
        'txtEntidad
        '
        Me.txtEntidad.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntidad.Location = New System.Drawing.Point(192, 14)
        Me.txtEntidad.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEntidad.Name = "txtEntidad"
        Me.txtEntidad.Size = New System.Drawing.Size(510, 25)
        Me.txtEntidad.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(13, 164)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(171, 28)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Tipo de Cambio: "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Panel7)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Location = New System.Drawing.Point(14, 278)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1046, 169)
        Me.Panel2.TabIndex = 169
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 21.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(-1, 2)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(1061, 41)
        Me.Label2.TabIndex = 161
        Me.Label2.Text = "ALMACENES DE MATERIALES E INSUMOS"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtTipoCambio)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.dateEntrega)
        Me.Panel1.Controls.Add(Me.dateEmision)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txtNit)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtResponsable)
        Me.Panel1.Controls.Add(Me.lblFechaReporte)
        Me.Panel1.Controls.Add(Me.txtTelefono)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txtCargo)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtEntidad)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(14, 69)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1046, 201)
        Me.Panel1.TabIndex = 166
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipoCambio.Location = New System.Drawing.Point(192, 165)
        Me.txtTipoCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(510, 25)
        Me.txtTipoCambio.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 11)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 28)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre de la Entidad: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dateEntrega
        '
        Me.dateEntrega.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.dateEntrega.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateEntrega.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateEntrega.Location = New System.Drawing.Point(881, 87)
        Me.dateEntrega.Margin = New System.Windows.Forms.Padding(4)
        Me.dateEntrega.Name = "dateEntrega"
        Me.dateEntrega.Size = New System.Drawing.Size(148, 25)
        Me.dateEntrega.TabIndex = 8
        '
        'dateEmision
        '
        Me.dateEmision.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.dateEmision.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateEmision.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateEmision.Location = New System.Drawing.Point(881, 13)
        Me.dateEmision.Margin = New System.Windows.Forms.Padding(4)
        Me.dateEmision.Name = "dateEmision"
        Me.dateEmision.Size = New System.Drawing.Size(148, 25)
        Me.dateEmision.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 39)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(171, 28)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Responsable: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 69)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(171, 28)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Cargo: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 99)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(171, 28)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "N.I.T. : "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNit
        '
        Me.txtNit.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNit.Location = New System.Drawing.Point(192, 102)
        Me.txtNit.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNit.Name = "txtNit"
        Me.txtNit.Size = New System.Drawing.Size(510, 25)
        Me.txtNit.TabIndex = 4
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label16.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(734, 84)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(139, 28)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Fecha de Entrega: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label8.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(734, 9)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(139, 28)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Fecha de Emisión: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtResponsable
        '
        Me.txtResponsable.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResponsable.Location = New System.Drawing.Point(192, 42)
        Me.txtResponsable.Margin = New System.Windows.Forms.Padding(4)
        Me.txtResponsable.Name = "txtResponsable"
        Me.txtResponsable.Size = New System.Drawing.Size(510, 25)
        Me.txtResponsable.TabIndex = 2
        '
        'lblFechaReporte
        '
        Me.lblFechaReporte.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblFechaReporte.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaReporte.Location = New System.Drawing.Point(880, 46)
        Me.lblFechaReporte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFechaReporte.Name = "lblFechaReporte"
        Me.lblFechaReporte.Size = New System.Drawing.Size(149, 28)
        Me.lblFechaReporte.TabIndex = 0
        Me.lblFechaReporte.Text = "Nombre de la Entidad: "
        Me.lblFechaReporte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTelefono
        '
        Me.txtTelefono.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelefono.Location = New System.Drawing.Point(192, 133)
        Me.txtTelefono.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(510, 25)
        Me.txtTelefono.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 131)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(171, 28)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Teléfono: "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'progress1
        '
        Me.progress1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.progress1.Location = New System.Drawing.Point(266, 455)
        Me.progress1.Margin = New System.Windows.Forms.Padding(4)
        Me.progress1.Name = "progress1"
        Me.progress1.Size = New System.Drawing.Size(542, 43)
        Me.progress1.TabIndex = 168
        '
        'btnGenRep
        '
        Me.btnGenRep.Font = New System.Drawing.Font("Arial", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenRep.Location = New System.Drawing.Point(362, 595)
        Me.btnGenRep.Margin = New System.Windows.Forms.Padding(4)
        Me.btnGenRep.Name = "btnGenRep"
        Me.btnGenRep.Size = New System.Drawing.Size(244, 43)
        Me.btnGenRep.TabIndex = 162
        Me.btnGenRep.Text = "Vista Previa"
        Me.btnGenRep.UseVisualStyleBackColor = True
        Me.btnGenRep.Visible = False
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 4.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(997, 749)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 12)
        Me.Label13.TabIndex = 171
        Me.Label13.Text = "Clever LTDA"
        '
        'btnGenExl
        '
        Me.btnGenExl.BackColor = System.Drawing.SystemColors.Control
        Me.btnGenExl.Enabled = False
        Me.btnGenExl.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnGenExl.FlatAppearance.BorderSize = 2
        Me.btnGenExl.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnGenExl.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnGenExl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnGenExl.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenExl.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnGenExl.Location = New System.Drawing.Point(14, 455)
        Me.btnGenExl.Margin = New System.Windows.Forms.Padding(4)
        Me.btnGenExl.Name = "btnGenExl"
        Me.btnGenExl.Size = New System.Drawing.Size(244, 43)
        Me.btnGenExl.TabIndex = 163
        Me.btnGenExl.Text = "Generar Reporte"
        Me.btnGenExl.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSalir.BackColor = System.Drawing.SystemColors.Control
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSalir.FlatAppearance.BorderSize = 2
        Me.btnSalir.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSalir.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSalir.Location = New System.Drawing.Point(816, 455)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(244, 43)
        Me.btnSalir.TabIndex = 164
        Me.btnSalir.Text = "Volver"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(14, 43)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(1046, 24)
        Me.Label11.TabIndex = 165
        Me.Label11.Text = "FORMULARIO SEG/4.93B"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtgReporte
        '
        Me.dtgReporte.AllowUserToResizeColumns = False
        Me.dtgReporte.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgReporte.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dtgReporte.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dtgReporte.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgReporte.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgReporte.ColumnHeadersHeight = 46
        Me.dtgReporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgReporte.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgReporte.EnableHeadersVisualStyles = False
        Me.dtgReporte.GridColor = System.Drawing.Color.Black
        Me.dtgReporte.Location = New System.Drawing.Point(14, 506)
        Me.dtgReporte.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtgReporte.Name = "dtgReporte"
        Me.dtgReporte.RowTemplate.Height = 24
        Me.dtgReporte.Size = New System.Drawing.Size(1046, 239)
        Me.dtgReporte.TabIndex = 175
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(855, 39)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 176
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'btnAnular
        '
        Me.btnAnular.BackColor = System.Drawing.SystemColors.Control
        Me.btnAnular.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnAnular.FlatAppearance.BorderSize = 2
        Me.btnAnular.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAnular.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAnular.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAnular.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnAnular.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnAnular.Location = New System.Drawing.Point(12, 2)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(172, 41)
        Me.btnAnular.TabIndex = 173
        Me.btnAnular.Text = "Anular Cierre"
        Me.btnAnular.UseVisualStyleBackColor = False
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(5, 43)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(114, 24)
        Me.Label12.TabIndex = 165
        Me.Label12.Text = "Último cierre:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCierre
        '
        Me.lblCierre.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCierre.Location = New System.Drawing.Point(114, 43)
        Me.lblCierre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCierre.Name = "lblCierre"
        Me.lblCierre.Size = New System.Drawing.Size(78, 24)
        Me.lblCierre.TabIndex = 165
        Me.lblCierre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1068, 769)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.dtgReporte)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnCierre)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.progress1)
        Me.Controls.Add(Me.btnGenRep)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.btnGenExl)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.lblCierre)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "4.93 B ADMISIBLE/NO ADMISIBLE"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.dtgBodegas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureArticulo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBodega, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dtgReporte, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCargo As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents radioTodos As System.Windows.Forms.RadioButton
    Friend WithEvents radioNoAdmisible As System.Windows.Forms.RadioButton
    Friend WithEvents radioAdmisible As System.Windows.Forms.RadioButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnCierre As System.Windows.Forms.Button
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents txtDescBodega As System.Windows.Forms.Label
    Friend WithEvents txtDescArticulo As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtBodega As System.Windows.Forms.TextBox
    Friend WithEvents txtArticulo As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents dateReporte As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtEntidad As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtTipoCambio As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dateEntrega As System.Windows.Forms.DateTimePicker
    Friend WithEvents dateEmision As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNit As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtResponsable As System.Windows.Forms.TextBox
    Friend WithEvents lblFechaReporte As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents progress1 As System.Windows.Forms.ProgressBar
    Friend WithEvents btnGenRep As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnGenExl As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents checkBodega As System.Windows.Forms.CheckBox
    Friend WithEvents checkArticulo As System.Windows.Forms.CheckBox
    Friend WithEvents radioDocumento As System.Windows.Forms.RadioButton
    Friend WithEvents radioAuditoria As System.Windows.Forms.RadioButton
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtgCabecera As System.Windows.Forms.DataGridView
    Friend WithEvents checkResumen As System.Windows.Forms.CheckBox
    Friend WithEvents pictureArticulo As System.Windows.Forms.PictureBox
    Friend WithEvents pictureBodega As System.Windows.Forms.PictureBox
    Friend WithEvents dtgReporte As System.Windows.Forms.DataGridView
    Friend WithEvents dtgBodegas As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblCierre As System.Windows.Forms.Label

End Class
