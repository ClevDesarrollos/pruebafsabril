﻿Public Class buscador

    Dim query As String
    Dim esquema As String = ObtenerEsquema()
#Region "Carga de datos"

    Sub alinearDatos()
        dtgBuscador.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        dtgBuscador.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
    End Sub
    Sub CargarDatosArticulo()
        If lblbOrigen.Text = "bodega" Then
            query = " SELECT DISTINCT BODEGA, "
            query += " ISNULL((SELECT NOMBRE FROM " & esquema & ".BODEGA"
            query += " WHERE A.BODEGA=BODEGA),'NO ASIGNADA')NOMBRE"
            query += " FROM " & esquema & ".U_CLEV_ANA_TOT_DOC A"
            llenarTabla(query, dtgBuscador)
            alinearDatos()
        Else
            query = " SELECT ARTICULO,DESCRIPCION FROM " & esquema & ".ARTICULO ORDER BY ARTICULO "
            llenarTabla(query, dtgBuscador)
            alinearDatos()
        End If

    End Sub


    Sub buscarDatosArticulo(ByVal col As String, ByVal col2 As String)
        If lblbOrigen.Text = "bodega" Then
            If col = "BODEGA" Then
                query = " SELECT DISTINCT " & col & ", "
                query += " ISNULL((SELECT NOMBRE FROM " & esquema & ".BODEGA"
                query += " WHERE A.BODEGA=BODEGA),'NO ASIGNADA') NOMBRE"
                query += " FROM " & esquema & ".U_CLEV_ANA_TOT_DOC A"
                query += "  WHERE " & col & " LIKE '%" & txtParametro.Text & "%'"
                query += " ORDER BY BODEGA"
                'MsgBox(query)
                llenarTabla(query, dtgBuscador)

            Else
                query = " SELECT DISTINCT "
                query += " ISNULL((SELECT NOMBRE FROM " & esquema & ".BODEGA"
                query += " WHERE A.BODEGA=BODEGA),'NO ASIGNADA')NOMBRE, " & col2
                query += " FROM " & esquema & ".U_CLEV_ANA_TOT_DOC A"
                query += "  WHERE ISNULL((SELECT NOMBRE FROM VITALERP.VITALICIA.BODEGA WHERE A.BODEGA=BODEGA),'NO ASIGNADA') LIKE '%" & txtParametro.Text & "%'"
                query += " ORDER BY BODEGA"

                llenarTabla(query, dtgBuscador)
            End If
            
        Else
            query = " SELECT " & col & ", " & col2 & " FROM " & esquema & ".ARTICULO WHERE " & dtgBuscador.Columns(0).Name.ToString & " LIKE '%" & txtParametro.Text & "%' ORDER BY ARTICULO"

            llenarTabla(query, dtgBuscador)
        End If


        alinearDatos()
    End Sub
#End Region













#Region "Eventos Formulario"
    Private Sub buscador_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim origen As String
        origen = lblbOrigen.Text
        CargarDatosArticulo()
        txtParametro.Select()

    End Sub

    Private Sub dtgBuscador_ColumnHeaderMouseClick(ByVal sender As Object, _
   ByVal e As DataGridViewCellMouseEventArgs) _
   Handles dtgBuscador.ColumnHeaderMouseClick
        If e.ColumnIndex <> 0 Then
            Dim newColumn As DataGridViewColumn = _
           dtgBuscador.Columns(e.ColumnIndex)
            Dim oldColumn As DataGridViewColumn = _
               dtgBuscador.Columns(e.ColumnIndex - 1)

            dtgBuscador.Columns(newColumn.Name.ToString).DisplayIndex = 0
            dtgBuscador.Columns(oldColumn.Name.ToString).DisplayIndex = 1
            newColumn.HeaderCell.SortGlyphDirection = SortOrder.Descending
            txtParametro.Clear()
            txtParametro.Select()
            lblParametros.Text = newColumn.Name.ToString


            buscarDatosArticulo(newColumn.Name.ToString, oldColumn.Name.ToString)


        End If
        txtParametro.Select()
    End Sub

#End Region

    Private Sub dtgBuscador_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgBuscador.CellClick
        dtgBuscador.Rows(dtgBuscador.CurrentCell.RowIndex).Selected = True
        txtParametro.Select()
    End Sub



    Private Sub txtParametro_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtParametro.TextChanged
        buscarDatosArticulo(dtgBuscador.Columns(0).Name.ToString, dtgBuscador.Columns(1).Name.ToString)

        txtParametro.Select()

    End Sub

    Private Sub dtgBuscador_CellContentDoubbleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgBuscador.CellDoubleClick
        If lblbOrigen.Text = "ARTD" Then
            Form1.txtArticulo.Text = dtgBuscador.Rows(e.RowIndex).Cells("ARTICULO").Value.ToString()
        Else
            If lblbOrigen.Text = "Ajuste" Then
                Ajustes.txtArticuloDesde.Text = dtgBuscador.Rows(e.RowIndex).Cells("ARTICULO").Value.ToString()
            Else
                Form1.txtBodega.Text = dtgBuscador.Rows(e.RowIndex).Cells("BODEGA").Value.ToString()
            End If

        End If
        Me.Close()
        txtParametro.Select()
    End Sub


    'Private Sub dtgBuscador_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dtgBuscador.KeyUp
    '    If e.KeyCode = Keys.Enter Then
    '        MsgBox("holas")
    '        If lblbOrigen.Text = "NITD" Then
    '            Form1.txtNITDesde.Text = dtgBuscador.Rows(dtgBuscador.CurrentRow.Index).Cells("NIT").Value.ToString()
    '        Else
    '            If lblbOrigen.Text = "NITH" Then
    '                Form1.txtNITHasta.Text = dtgBuscador.Rows(dtgBuscador.CurrentRow.Index).Cells("NIT").Value.ToString()
    '            Else
    '                If lblbOrigen.Text = "CUCD" Then
    '                    Form1.txtCucDesde.Text = dtgBuscador.Rows(dtgBuscador.CurrentRow.Index).Cells("CUENTA_CONTABLE").Value.ToString()
    '                Else
    '                    Form1.txtCucHasta.Text = dtgBuscador.Rows(dtgBuscador.CurrentRow.Index).Cells("CUENTA_CONTABLE").Value.ToString()
    '                End If
    '            End If

    '        End If
    '        Me.Close()
    '        txtParametro.Select()
    '    End If


    'End Sub

    Private Sub ON_ENTER(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtParametro.KeyUp
        If e.KeyCode = Keys.Enter Then

            If lblbOrigen.Text = "ARTD" Then
                Form1.txtArticulo.Text = dtgBuscador.Rows(dtgBuscador.CurrentRow.Index).Cells("ARTICULO").Value.ToString()
            Else
                If lblbOrigen.Text = "Ajuste" Then
                    Ajustes.txtArticuloDesde.Text = dtgBuscador.Rows(dtgBuscador.CurrentRow.Index).Cells("ARTICULO").Value.ToString()
                Else
                    Form1.txtBodega.Text = dtgBuscador.Rows(dtgBuscador.CurrentRow.Index).Cells("BODEGA").Value.ToString()
                End If

            End If
            Me.Close()
            txtParametro.Select()
        End If


    End Sub


    Private Sub ON_ESCAPE(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtParametro.KeyUp
        ' DETERMINAR CUANDO LA TECLA ESCAPE A SIDO PRESIONADA

        If e.KeyCode = Keys.Escape Then
            ' CERRAR EL FORMULARIO
            Me.Close()
        End If
    End Sub



End Class