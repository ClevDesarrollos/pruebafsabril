﻿Imports System.ComponentModel
Imports System.Math
Public Class cierreMes
    Dim query As String
    Dim esquema As String = ObtenerEsquema()

    ' variables para devolución de datos
    Dim tabla As String
    Dim columna As String
    Dim condicion As String

    ' variables para la inserción de datos
    Dim valores As String

    'VARIABLES PARA HILO EN SEGUNDO PLANO
    Private errorP As Boolean = False
    Private iteraciones As Integer
    Private iteracionesAud As Integer
    Private highestPercentageReached As Integer = 0
    Private highestPercentageReachedAud As Integer = 0
    Public anioGlobal As String = ""
    Public mesGlobal As String = ""
    Public registros As String = ""
    Public registrosAud As String = ""
#Region "TRABAJO EN SEGUNDO PLANO"
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles hiloSegundoPlano.DoWork
        'Obtener el objeto BackgroundWorker que provocó este evento
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)

        ' Assign the result of the computation
        ' to the Result property of the DoWorkEventArgs
        ' object. This is will be available to the 
        ' RunWorkerCompleted eventhandler.
        'Asignar el resultado de la computación a la propiedad Result 
        'del objeto DoWorkEventArgs
        e.Result = cerrarMes(mesGlobal, anioGlobal, worker, e)
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles hiloSegundoPlano.ProgressChanged
        progress.Value = e.ProgressPercentage
        lInfo.Text = CStr(iteraciones) & " / " & registros
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles hiloSegundoPlano.RunWorkerCompleted
        'Manejar el caso en que se produzca un error o excepción
        If (e.Error IsNot Nothing) Then
            MsgBox(e.Error.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        Else
            If e.Cancelled Then
                'Manejar el caso en que el usuario haya cancelado la operación. 
                MsgBox("No se pudo completar el proceso de cierre.")
                Me.Close()
                Form1.Visible = True

            Else
                'Manejar el caso en que la operación haya finalizado con éxito
                If errorP = False Then
                    lInfo.Text = ("Proceso de cierre por fecha de Documento completado exitosamente")
                    hiloSegundoPlanoAud.RunWorkerAsync(0)
                    'query = "INSERT INTO " & esquema & ".U_CLEV_ANA_CIERRE(ANIO,MES) VALUES ('" & anioGlobal & "','" & mesGlobal & "')"
                    'ejecutarComandosSQL(query)
                    'Me.Close()
                    'Form1.Visible = True
                End If


            End If
        End If


    End Sub
#End Region

#Region "TRABAJO EN SEGUNDO PLANO AUDITORIA"
    Private Sub BackgroundWorker2_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles hiloSegundoPlanoAud.DoWork
        'Obtener el objeto BackgroundWorker que provocó este evento
        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)

        ' Assign the result of the computation
        ' to the Result property of the DoWorkEventArgs
        ' object. This is will be available to the 
        ' RunWorkerCompleted eventhandler.
        'Asignar el resultado de la computación a la propiedad Result 
        'del objeto DoWorkEventArgs
        e.Result = cerrarMesAud(mesGlobal, anioGlobal, worker, e)
    End Sub

    Private Sub BackgroundWorker2_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles hiloSegundoPlanoAud.ProgressChanged
        progressAud.Value = e.ProgressPercentage
        lInfoAud.Text = CStr(iteracionesAud) & " / " & registrosAud
    End Sub

    Private Sub BackgroundWorker2_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles hiloSegundoPlanoAud.RunWorkerCompleted
        'Manejar el caso en que se produzca un error o excepción
        If (e.Error IsNot Nothing) Then
            MsgBox(e.Error.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        Else
            If e.Cancelled Then
                'Manejar el caso en que el usuario haya cancelado la operación. 
                MsgBox("No se pudo completar el proceso de cierre por fecha de Auditoria.")
                Me.Close()
                Form1.Visible = True

            Else
                'Manejar el caso en que la operación haya finalizado con éxito
                If errorP = False Then
                    MsgBox("Proceso de cierre por fecha de Auditoria completatado exitosamente")
                    query = "INSERT INTO " & esquema & ".U_CLEV_ANA_CIERRE(ANIO,MES) VALUES ('" & anioGlobal & "','" & mesGlobal & "')"
                    ejecutarComandosSQL(query)
                    Form1.llenarUltimoMesCierre()
                    Me.Close()

                    Form1.Visible = True
                End If

            End If
        End If


    End Sub
#End Region


#Region "ADMISIBLE"
    Function obtenerCantidadAnteriorAdm(ByVal bod As String, ByVal art As String, ByVal codFec As Integer, ByVal fechaDe As String) As Double
        
        Dim a As Double = 0

        Dim presup As Double
        Dim consAnt As Double
        Dim cons As Double
        Dim cantFin As Double

        Dim consum As Double
        Dim porConsum As Double
        If codFec <> 0 Then
            columna = "PRESUPUESTADO"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec





            presup = CDbl(DevolverDatoQuery(columna, tabla, condicion))
            '14116.069


            columna = "CONSUMIDO"
            cons = CDbl(DevolverDatoQuery(columna, tabla, condicion))
            '900

            columna = "CONSUMIDO_ANT"
            consAnt = CDbl(DevolverDatoQuery(columna, tabla, condicion))
            '12364
            '

            columna = "CANTIDAD_FINAL"
            cantFin = CDbl(DevolverDatoQuery(columna, tabla, condicion))
            '5206 
            'MsgBox(cantFin.ToString)
            '14116.069,900,12364
            consum = consumidoObras(presup, cons, consAnt) '852.069
            porConsum = totalPorConsumir(presup, cons, consAnt) '852.069



            If cantFin > porConsum Then
                a = porConsum
            Else
                a = cantFin
            End If
        End If


        Return a

    End Function

    Function consumidoObras(ByVal presup As Double, ByVal consumido As Double, ByVal consumidoAnt As Double) As Double
        Dim a As Double

        a = presup - consumidoAnt - consumido
        If a < 0 Then
            a = presup - consumidoAnt
            If a < 0 Then
                a = 0
            End If
        End If
        Return a

    End Function

    Function totalPorConsumir(ByVal presup As Double, ByVal consumido As Double, ByVal consumidoAnt As Double) As Double
        Dim a As Double

        a = presup - consumidoAnt - consumido
        If a < 0 Then
            a = 0
        End If
        Return a

    End Function

    Function calcularCostoPromAdm(ByVal bod As String, ByVal art As String, ByVal codFec As Integer, ByVal cantEntradas As Double, ByVal costoEntradas As Double, ByVal fechaDe As String) As Double


        Dim costoProm As Double = 0
        Dim cantidadFinal As Double
        Dim costoFinal As Double

        If codFec <> 0 Then

            columna = "CANTIDAD_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_ADM_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_ADM_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec


            cantidadFinal = CDbl(DevolverDatoQuery(columna, tabla, condicion))



            columna = "COSTO_FINAL"
            costoFinal = CDbl(DevolverDatoQuery(columna, tabla, condicion))

            'Try
            '    costoProm = (costoFinal + costoEntradas) / (cantidadFinal + cantEntradas)
            'Catch ex As DivideByZeroException
            '    Try
            '        costoProm = costoFinal / cantidadFinal
            '    Catch em As DivideByZeroException
            '        Try
            '            costoProm = costoEntradas / cantEntradas
            '        Catch en As DivideByZeroException
            '            costoProm = 0
            '        End Try
            '    End Try
            'End Try
            If (cantidadFinal + cantEntradas) = 0 Then
                If (cantidadFinal) = 0 Then
                    If (cantEntradas) = 0 Then
                        costoProm = 0

                    Else
                        costoProm = Abs((costoEntradas) / (cantEntradas))

                    End If

                Else
                    costoProm = (costoFinal) / (cantidadFinal)

                End If

            Else
                costoProm = (costoFinal + costoEntradas) / (cantidadFinal + cantEntradas)

            End If

        End If



        Return costoProm

    End Function



    Function obtenerCostoAnteriorAdm(ByVal bod As String, ByVal art As String, ByVal codFec As Integer, ByVal fechaDe As String) As Double
        Dim a As Double = 0

        'Dim cantAnterior As Double
        Dim costoProm As Double
        If codFec <> 0 Then

            columna = "COSTO_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_ADM_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_ADM_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec


            costoProm = CDbl(DevolverDatoQuery(columna, tabla, condicion))

            'cantAnterior = obtenerCantidadAnteriorAdm(bod, art, codFec, fechaDe)
            a = costoProm
        End If

        Return a
    End Function

#End Region


#Region "CIERRE DE MES"

    Sub llenarTablaMes()

        query = " SELECT "
        query += " ARTICULO, "
        query += " BODEGA,"
        query += " ANIO,"
        query += " MES, "
        query += " CANTIDAD_ANTERIOR,"
        query += " ENTRADAS,"
        query += " SALIDAS,"
        query += " CANTIDAD_FINAL,"
        query += " COSTO_PROM,"
        query += " COSTO_ANTERIOR,"
        query += " COSTO_ENTRADAS,"
        query += " COSTO_SALIDAS,"
        query += " COSTO_FINAL,"
        query += " PRESUPUESTADO,"
        query += " CONSUMIDO,"
        query += " CONSUMIDO_ANT"
        query += " FROM " & esquema & ".U_CLEV_ANA_TOT_DOC WHERE ANIO='" & anioGlobal & "' AND MES='" & mesGlobal & "'"

        llenarTabla(query, dtgMes)
    End Sub

    Sub llenarTablaMesAud()

        query = " SELECT "
        query += " ARTICULO, "
        query += " BODEGA,"
        query += " ANIO,"
        query += " MES, "
        query += " CANTIDAD_ANTERIOR,"
        query += " ENTRADAS,"
        query += " SALIDAS,"
        query += " CANTIDAD_FINAL,"
        query += " COSTO_PROM,"
        query += " COSTO_ANTERIOR,"
        query += " COSTO_ENTRADAS,"
        query += " COSTO_SALIDAS,"
        query += " COSTO_FINAL,"
        query += " PRESUPUESTADO,"
        query += " CONSUMIDO,"
        query += " CONSUMIDO_ANT"
        query += " FROM " & esquema & ".U_CLEV_ANA_TOT_AUD WHERE ANIO='" & anioGlobal & "' AND MES='" & mesGlobal & "'"


        llenarTabla(query, dtgMesAud)
    End Sub

    Function obtenerCodigoFechaAnterior(ByVal mes As Integer, ByVal anio As Integer, ByVal fechaDe As String) As Integer
        Dim codFec As Integer

        columna = "DISTINCT TOP 1 CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2)"

        If fechaDe = "D" Then
            tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
        Else
            tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
        End If


        condicion = " WHERE CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))< CONVERT(INT,CONVERT(VARCHAR(4)," & anio & ")+RIGHT('0'+CONVERT(VARCHAR(2)," & mes & "),2))"
        condicion += " ORDER BY CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2) DESC"

        codFec = CInt(DevolverDatoQuery(columna, tabla, condicion))

        Return codFec

    End Function

    Function obtenerCostoFinal(ByVal codfec As Integer, ByVal bod As String, ByVal art As String, ByVal fechaDe As String) As Double
        Dim costoFin As Double = 0
        If codfec <> 0 Then
            columna = "COSTO_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codfec


            costoFin = CDbl(DevolverDatoQuery(columna, tabla, condicion))
        End If
        Return costoFin

    End Function

    Function obtenerCantidadFinalAdm(ByVal codfec As Integer, ByVal bod As String, ByVal art As String, ByVal fechaDe As String) As Double
        Dim cantFinal As Double = 0
        If codfec <> 0 Then
            columna = "CANTIDAD_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_ADM_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_ADM_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"

            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codfec


            cantFinal = CDbl(DevolverDatoQuery(columna, tabla, condicion))
        End If
        Return cantFinal

    End Function

   
    Function cerrarMes(ByVal mess As String, ByVal anioo As String, ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs) As Integer

        Dim articulo As String
        Dim bodega As String

        Dim mes As String = mess
        Dim anio As String = anioo

        Dim cantAnt As Double
        Dim cantEnt As Double
        Dim cantSal As Double
        Dim cantFin As Double

        Dim costoAnt As Double
        Dim costoEnt As Double
        Dim costoSal As Double
        Dim costoFin As Double
        Dim validadorCostFin As Double
        Dim validadorCostFinAdm As Double
        Dim validadorCantFin As Double

        Dim cantAntAdm As Double
        Dim cantEntAdm As Double
        Dim cantSalAdm As Double
        Dim cantFinAdm As Double

        Dim costoAntAdm As Double
        Dim costoEntAdm As Double
        Dim costoSalAdm As Double
        Dim costoFinAdm As Double
        Dim cantEntSal As Double = 0

        Dim presupuesto As Double
        Dim consumido As Double
        Dim consumido2 As Double
        Dim consumidoAnt As Double
        Dim porConsumir As Double

        Dim costoProm As Double
        Dim costoPromNuevo As Double
        Dim costoPromNuevoAdm As Double

        Dim codFec As Integer = CInt(anio & Microsoft.VisualBasic.Strings.Right("0" & mes, 2))
        Dim codFecAnt As Integer = obtenerCodigoFechaAnterior(CInt(mes), CInt(anio), "D")




        Dim cantFilas As Integer
        Dim cantCol As Integer

        'Dim cantFilasProm As Integer
        'Dim cantColProm As Integer

        cantFilas = dtgMes.RowCount - 1
        cantCol = dtgMes.ColumnCount

        'progress.Maximum = cantFilas - 1
        Try
            For i As Integer = 0 To cantFilas - 1 Step 1
                'progress.Value = i

                Dim percentComplete As Integer = _
                   CSng(iteraciones) / CSng(cantFilas - 1) * 100
                If percentComplete > highestPercentageReached Then

                    highestPercentageReached = percentComplete
                    worker.ReportProgress(percentComplete)


                End If

                iteraciones = iteraciones + 1
                'DEFINIMOS LAS VARIABLES
                articulo = dtgMes.Rows(i).Cells("ARTICULO").Value.ToString()
                bodega = dtgMes.Rows(i).Cells("BODEGA").Value.ToString()

                cantAnt = dtgMes.Rows(i).Cells("CANTIDAD_ANTERIOR").Value.ToString()
                cantEnt = dtgMes.Rows(i).Cells("ENTRADAS").Value.ToString()
                cantSal = dtgMes.Rows(i).Cells("SALIDAS").Value.ToString()
                cantFin = cantAnt + cantEnt - cantSal

                'quitamos las cantidades negativas de los reportes

                If cantAnt < 0 Then
                    cantAnt = 0
                End If

                If cantFin < 0 Then
                    cantFin = 0
                End If

                costoProm = dtgMes.Rows(i).Cells("COSTO_PROM").Value.ToString()

                costoAnt = obtenerCostoFinal(codFecAnt, bodega, articulo, "D")
                costoEnt = dtgMes.Rows(i).Cells("COSTO_ENTRADAS").Value.ToString()
                costoSal = dtgMes.Rows(i).Cells("COSTO_SALIDAS").Value.ToString()



                Try
                    costoPromNuevo = Abs(costoEnt - costoSal) / Abs(cantEnt - cantSal)
                    If Double.IsNaN(costoPromNuevo) Or Double.IsInfinity(costoPromNuevo) Then
                        costoPromNuevo = 0
                    End If
                    If costoPromNuevo = 0 Then
                        costoPromNuevo = costoProm
                    End If
                    'MsgBox(costoPromNuevo.ToString + " try")
                Catch ex As Exception
                    costoPromNuevo = costoProm
                    ' MsgBox(costoPromNuevo.ToString + " cantch")
                End Try
                ' EN caso de que el costo promedio sea 0 volvemos al procesos de obtener un costo promedio para las entradas y salidas en base a
                'la division de CostoEntradas/CantidadEntradas o CostoSalidas/CantidadSalidas
                If costoPromNuevo = 0 Then
                    If cantEnt = 0 Then
                        If cantSal = 0 Then
                            costoPromNuevo = 0
                        Else
                            costoPromNuevo = costoSal / cantSal
                        End If
                    Else
                        costoPromNuevo = costoEnt / cantEnt
                    End If
                End If
                '**************************ajustamos el cambio de cantidades finales diferentes a negativo*****************
                validadorCantFin = cantAnt + cantEnt - cantSal
                If validadorCantFin <> cantFin Then
                    validadorCantFin = cantFin - validadorCantFin
                    If validadorCantFin < 0 Then
                        If cantSal <> 0 Then
                            cantSal = cantSal + Abs(validadorCantFin)
                        Else
                            If cantEnt - Abs(validadorCantFin) >= 0 Then
                                cantEnt = cantEnt - Abs(validadorCantFin)
                            Else
                                cantSal = cantSal + Abs(validadorCantFin)
                            End If
                        End If


                    Else
                        If cantSal <> 0 Then
                            cantEnt = cantEnt + Abs(validadorCantFin)
                        Else
                            If cantSal - Abs(validadorCantFin) >= 0 Then
                                cantSal = cantSal - Abs(validadorCantFin)
                            Else
                                cantEnt = cantEnt + Abs(validadorCantFin)
                            End If
                        End If

                    End If
                End If


                '**********************************************************************


                costoEnt = cantEnt * costoPromNuevo
                costoSal = cantSal * costoPromNuevo

                costoFin = cantFin * costoProm

                validadorCostFin = costoAnt + costoEnt - costoSal
                If validadorCostFin <> costoFin Then
                    validadorCostFin = costoFin - validadorCostFin
                    If validadorCostFin < 0 Then
                        If costoSal <> 0 Then
                            costoSal = costoSal + Abs(validadorCostFin)
                        Else
                            If costoEnt - Abs(validadorCostFin) >= 0 Then
                                costoEnt = costoEnt - Abs(validadorCostFin)
                            Else
                                costoSal = costoSal + Abs(validadorCostFin)
                            End If
                        End If


                    Else
                        If costoSal <> 0 Then
                            costoEnt = costoEnt + Abs(validadorCostFin)
                        Else
                            If costoSal - Abs(validadorCostFin) >= 0 Then
                                costoSal = costoSal - Abs(validadorCostFin)
                            Else
                                costoEnt = costoEnt + Abs(validadorCostFin)
                            End If
                        End If

                    End If
                End If

                query = "UPDATE VITALICIA.U_CLEV_ANA_TOT_DOC SET CANTIDAD_ANTERIOR=" & cantAnt & ",ENTRADAS=" & cantEnt & ",SALIDAS=" & cantSal & ", CANTIDAD_FINAL=" & cantFin & ", COSTO_ANTERIOR=" & costoAnt & ",COSTO_ENTRADAS=" & costoEnt & ", COSTO_SALIDAS=" & costoSal & ", COSTO_FINAL=" & costoFin & ", COSTO_PROM=" & costoPromNuevo & ""

                query += " WHERE ARTICULO ='" & articulo & "'"
                query += " AND BODEGA='" & bodega & "'"
                query += " AND ANIO ='" & anio & "'"
                query += " AND MES ='" & mes & "'"

                ejecutarComandosSQL(query)

                '************OBTENEMOS LAS CANTIDADES ADMISIBLES *****************




                cantAntAdm = obtenerCantidadFinalAdm(codFecAnt, bodega, articulo, "D")
                'If cantEnt = 0 And cantSal = 0 Then
                '    cantFinAdm = cantAntAdm
                'Else
                cantFinAdm = obtenerCantidadAnteriorAdm(bodega, articulo, codFec, "D")
                'End If

                presupuesto = dtgMes.Rows(i).Cells("PRESUPUESTADO").Value.ToString()
                consumido = dtgMes.Rows(i).Cells("CONSUMIDO").Value.ToString()
                consumidoAnt = dtgMes.Rows(i).Cells("CONSUMIDO_ANT").Value.ToString()

                porConsumir = presupuesto - consumido - consumidoAnt
                If porConsumir < 0 Then
                    porConsumir = 0
                End If

                consumido2 = consumido + consumidoAnt
                'totConsumido = consumidoObras(presupuesto, consumido, consumidoAnt)
                'porConsumir = totalPorConsumir(presupuesto, consumido, consumidoAnt)
                If consumido2 > presupuesto Then
                    consumido = presupuesto - consumidoAnt
                    If consumido < 0 Then
                        consumido = 0
                    End If
                End If



                If Abs(cantAntAdm - cantSal - cantFinAdm) > cantEnt Then
                    If Abs(cantAntAdm - cantSal - cantFinAdm + cantEnt) <= porConsumir Then
                        cantSalAdm = Abs(cantAntAdm - cantSal - cantFinAdm + cantEnt)
                    Else
                        cantSalAdm = porConsumir

                    End If
                Else
                    If cantSal <= porConsumir Then
                        cantSalAdm = cantSal

                    Else
                        cantSalAdm = porConsumir

                    End If
                End If

                'MsgBox(codFec2.ToString)


                cantEntAdm = cantSalAdm + cantFinAdm - cantAntAdm



                'CALCULAMOS EL COSTO PROMEDIO ADMISIBLE
                costoPromNuevoAdm = costoPromNuevo 'calcularCostoPromAdm(bodega, articulo, codFecAnt, cantEntAdm, costoEntAdm, "D", costoProm)


                If cantEntAdm < 0 Then
                    cantEntSal = Abs(cantEntAdm)
                    cantEntAdm = 0
                End If

                cantSalAdm += cantEntSal


                If cantAnt = 0 Then
                    costoAntAdm = 0
                Else
                    costoAntAdm = cantAntAdm * costoAnt / cantAnt
                    If Double.IsNaN(costoAntAdm) Or Double.IsInfinity(costoAntAdm) Then
                        costoAntAdm = 0
                    End If
                End If

                If cantEnt = 0 Then
                    costoEntAdm = cantEntAdm * costoPromNuevoAdm
                Else
                    costoEntAdm = cantEntAdm * costoEnt / cantEnt
                    If Double.IsNaN(costoEntAdm) Or Double.IsInfinity(costoEntAdm) Then
                        costoEntAdm = 0
                    End If
                End If

                If cantSal = 0 Then
                    costoSalAdm = cantSalAdm * costoPromNuevoAdm
                Else
                    costoSalAdm = cantSalAdm * costoSal / cantSal
                    If Double.IsNaN(costoSalAdm) Or Double.IsInfinity(costoSalAdm) Then
                        costoSalAdm = 0
                    End If
                End If

                If cantFin = 0 Then
                    costoFinAdm = 0
                Else
                    costoFinAdm = cantFinAdm * costoFin / cantFin
                    If Double.IsNaN(costoFinAdm) Or Double.IsInfinity(costoFinAdm) Then
                        costoFinAdm = 0
                    End If

                End If

                If cantEntAdm = 0 Then
                    If costoEntAdm < 0 Then
                        costoSalAdm = costoSalAdm - costoEntAdm
                        costoEntAdm = 0
                    End If
                End If

                If cantSalAdm = 0 Then
                    If costoSalAdm < 0 Then
                        costoEntAdm = costoEntAdm - costoSalAdm
                        costoSalAdm = 0
                    End If
                End If



                validadorCostFinAdm = costoAntAdm + costoEntAdm - costoSalAdm
                If validadorCostFinAdm <> costoFinAdm Then
                    validadorCostFinAdm = costoFinAdm - validadorCostFinAdm
                    If validadorCostFinAdm < 0 Then
                        If costoSalAdm <> 0 Then
                            costoSalAdm = costoSalAdm + Abs(validadorCostFinAdm)
                        Else
                            If costoEntAdm - Abs(validadorCostFinAdm) >= 0 Then
                                costoEntAdm = costoEntAdm - Abs(validadorCostFinAdm)
                            Else
                                costoSalAdm = costoSalAdm + Abs(validadorCostFinAdm)
                            End If
                        End If


                    Else
                        If costoEntAdm <> 0 Then
                            costoEntAdm = costoEntAdm + Abs(validadorCostFinAdm)
                        Else
                            If costoSalAdm - Abs(validadorCostFinAdm) >= 0 Then
                                costoSalAdm = costoSalAdm - Abs(validadorCostFinAdm)
                            Else
                                costoEntAdm = costoEntAdm + Abs(validadorCostFinAdm)
                            End If
                        End If

                    End If
                End If
                query = " UPDATE VITALICIA.U_CLEV_ANA_ADM_DOC SET CANTIDAD_ANTERIOR=" & cantAntAdm & ",ENTRADAS=" & cantEntAdm & ",SALIDAS=" & cantSalAdm & ",CANTIDAD_FINAL=" & cantFinAdm & ",COSTO_ANTERIOR=" & costoAntAdm & ",COSTO_ENTRADAS=" & costoEntAdm & ",COSTO_SALIDAS=" & costoSalAdm & ",COSTO_FINAL=" & costoFinAdm & ", COSTO_PROM=" & costoPromNuevoAdm & ", AJUSTES=" & cantEntSal & ""
                query += " WHERE ARTICULO ='" & articulo & "'"
                query += " AND BODEGA='" & bodega & "'"
                query += " AND ANIO ='" & anio & "'"
                query += " AND MES ='" & mes & "'"

                ejecutarComandosSQL(query)
                cantEntSal = 0
            Next
        Catch ex As Exception
            MsgBox("No se pudo completar el proceso de cierre.")
            errorP = True
        End Try


        Return 0
    End Function


    Function cerrarMesAud(ByVal mess As String, ByVal anioo As String, ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs) As Integer

        Dim articulo As String
        Dim bodega As String

        Dim mes As String = mess
        Dim anio As String = anioo

        Dim cantAnt As Double
        Dim cantEnt As Double
        Dim cantSal As Double
        Dim cantFin As Double

        Dim costoAnt As Double
        Dim costoEnt As Double
        Dim costoSal As Double
        Dim costoFin As Double
        Dim validadorCostFin As Double
        Dim validadorCostFinAdm As Double
        Dim validadorCantFin As Double

        Dim cantAntAdm As Double
        Dim cantEntAdm As Double
        Dim cantSalAdm As Double
        Dim cantFinAdm As Double

        Dim costoAntAdm As Double
        Dim costoEntAdm As Double
        Dim costoSalAdm As Double
        Dim costoFinAdm As Double
        Dim cantEntSal As Double = 0

        Dim presupuesto As Double
        Dim consumido As Double
        Dim consumido2 As Double
        Dim consumidoAnt As Double
        Dim porConsumir As Double

        Dim costoProm As Double
        Dim costoPromNuevo As Double
        Dim costoPromNuevoAdm As Double

        Dim codFec As Integer = CInt(anio & Microsoft.VisualBasic.Strings.Right("0" & mes, 2))
        Dim codFecAnt As Integer = obtenerCodigoFechaAnterior(CInt(mes), CInt(anio), "D")


        Dim cantFilas As Integer
        Dim cantCol As Integer

        'Dim cantFilasProm As Integer
        'Dim cantColProm As Integer

        cantFilas = dtgMesAud.RowCount - 1
        cantCol = dtgMesAud.ColumnCount

        'progress.Maximum = cantFilas - 1
        Try
            For i As Integer = 0 To cantFilas - 1 Step 1
                'progress.Value = i

                Dim percentComplete As Integer = _
                   CSng(iteracionesAud) / CSng(cantFilas - 1) * 100
                If percentComplete > highestPercentageReachedAud Then

                    highestPercentageReachedAud = percentComplete
                    worker.ReportProgress(percentComplete)


                End If

                iteracionesAud = iteracionesAud + 1
                'DEFINIMOS LAS VARIABLES
                articulo = dtgMesAud.Rows(i).Cells("ARTICULO").Value.ToString()
                bodega = dtgMesAud.Rows(i).Cells("BODEGA").Value.ToString()

                cantAnt = dtgMesAud.Rows(i).Cells("CANTIDAD_ANTERIOR").Value.ToString()
                cantEnt = dtgMesAud.Rows(i).Cells("ENTRADAS").Value.ToString()
                cantSal = dtgMesAud.Rows(i).Cells("SALIDAS").Value.ToString()
                cantFin = cantAnt + cantEnt - cantSal


                'quitamos las cantidades negativas de los reportes

                If cantAnt < 0 Then
                    cantAnt = 0
                End If

                If cantFin < 0 Then
                    cantFin = 0
                End If

                costoProm = dtgMesAud.Rows(i).Cells("COSTO_PROM").Value.ToString()

                costoAnt = obtenerCostoFinal(codFecAnt, bodega, articulo, "D")
                costoEnt = dtgMesAud.Rows(i).Cells("COSTO_ENTRADAS").Value.ToString()
                costoSal = dtgMesAud.Rows(i).Cells("COSTO_SALIDAS").Value.ToString()



                Try
                    costoPromNuevo = Abs(costoEnt - costoSal) / Abs(cantEnt - cantSal)
                    If Double.IsNaN(costoPromNuevo) Or Double.IsInfinity(costoPromNuevo) Then
                        costoPromNuevo = 0
                    End If
                    If costoPromNuevo = 0 Then
                        costoPromNuevo = costoProm
                    End If
                    'MsgBox(costoPromNuevo.ToString + " try")
                Catch ex As Exception
                    costoPromNuevo = costoProm
                    ' MsgBox(costoPromNuevo.ToString + " cantch")
                End Try
                ' EN caso de que el costo promedio sea 0 volvemos al procesos de obtener un costo promedio para las entradas y salidas en base a
                'la division de CostoEntradas/CantidadEntradas o CostoSalidas/CantidadSalidas
                If costoPromNuevo = 0 Then
                    If cantEnt = 0 Then
                        If cantSal = 0 Then
                            costoPromNuevo = 0
                        Else
                            costoPromNuevo = costoSal / cantSal
                        End If
                    Else
                        costoPromNuevo = costoEnt / cantEnt
                    End If
                End If
                '**************************ajustamos el cambio de cantidades finales diferentes a negativo*****************
                validadorCantFin = cantAnt + cantEnt - cantSal
                If validadorCantFin <> cantFin Then
                    validadorCantFin = cantFin - validadorCantFin
                    If validadorCantFin < 0 Then
                        If cantSal <> 0 Then
                            cantSal = cantSal + Abs(validadorCantFin)
                        Else
                            If cantEnt - Abs(validadorCantFin) >= 0 Then
                                cantEnt = cantEnt - Abs(validadorCantFin)
                            Else
                                cantSal = cantSal + Abs(validadorCantFin)
                            End If
                        End If


                    Else
                        If cantSal <> 0 Then
                            cantEnt = cantEnt + Abs(validadorCantFin)
                        Else
                            If cantSal - Abs(validadorCantFin) >= 0 Then
                                cantSal = cantSal - Abs(validadorCantFin)
                            Else
                                cantEnt = cantEnt + Abs(validadorCantFin)
                            End If
                        End If

                    End If
                End If


                '**********************************************************************

                costoEnt = cantEnt * costoPromNuevo
                costoSal = cantSal * costoPromNuevo

                costoFin = cantFin * costoProm

                validadorCostFin = costoAnt + costoEnt - costoSal
                If validadorCostFin <> costoFin Then
                    validadorCostFin = costoFin - validadorCostFin
                    If validadorCostFin < 0 Then
                        If costoSal <> 0 Then
                            costoSal = costoSal + Abs(validadorCostFin)
                        Else
                            If costoEnt - Abs(validadorCostFin) >= 0 Then
                                costoEnt = costoEnt - Abs(validadorCostFin)
                            Else
                                costoSal = costoSal + Abs(validadorCostFin)
                            End If
                        End If


                    Else
                        If costoSal <> 0 Then
                            costoEnt = costoEnt + Abs(validadorCostFin)
                        Else
                            If costoSal - Abs(validadorCostFin) >= 0 Then
                                costoSal = costoSal - Abs(validadorCostFin)
                            Else
                                costoEnt = costoEnt + Abs(validadorCostFin)
                            End If
                        End If

                    End If
                End If

                query = "UPDATE VITALICIA.U_CLEV_ANA_TOT_AUD SET CANTIDAD_ANTERIOR=" & cantAnt & ",ENTRADAS=" & cantEnt & ",SALIDAS=" & cantSal & ", CANTIDAD_FINAL=" & cantFin & ", COSTO_ANTERIOR=" & costoAnt & ",COSTO_ENTRADAS=" & costoEnt & ", COSTO_SALIDAS=" & costoSal & ", COSTO_FINAL=" & costoFin & ", COSTO_PROM=" & costoPromNuevo & ""
                query += " WHERE ARTICULO ='" & articulo & "'"
                query += " AND BODEGA='" & bodega & "'"
                query += " AND ANIO ='" & anio & "'"
                query += " AND MES ='" & mes & "'"

                ejecutarComandosSQL(query)


                '************OBTENEMOS LAS CANTIDADES ADMISIBLES *****************




                cantAntAdm = obtenerCantidadFinalAdm(codFecAnt, bodega, articulo, "A")
                'If cantEnt = 0 And cantSal = 0 Then
                '    cantFinAdm = cantAntAdm
                'Else
                cantFinAdm = obtenerCantidadAnteriorAdm(bodega, articulo, codFec, "A")
                'End If

                presupuesto = dtgMesAud.Rows(i).Cells("PRESUPUESTADO").Value.ToString()
                consumido = dtgMesAud.Rows(i).Cells("CONSUMIDO").Value.ToString()
                consumidoAnt = dtgMesAud.Rows(i).Cells("CONSUMIDO_ANT").Value.ToString()

                porConsumir = presupuesto - consumido - consumidoAnt
                If porConsumir < 0 Then
                    porConsumir = 0
                End If

                consumido2 = consumido + consumidoAnt
                'totConsumido = consumidoObras(presupuesto, consumido, consumidoAnt)
                'porConsumir = totalPorConsumir(presupuesto, consumido, consumidoAnt)
                If consumido2 > presupuesto Then
                    consumido = presupuesto - consumidoAnt
                    If consumido < 0 Then
                        consumido = 0
                    End If
                End If



                If Abs(cantAntAdm - cantSal - cantFinAdm) > cantEnt Then
                    If Abs(cantAntAdm - cantSal - cantFinAdm + cantEnt) <= porConsumir Then
                        cantSalAdm = Abs(cantAntAdm - cantSal - cantFinAdm + cantEnt)
                    Else
                        cantSalAdm = porConsumir

                    End If
                Else
                    If cantSal <= porConsumir Then
                        cantSalAdm = cantSal

                    Else
                        cantSalAdm = porConsumir

                    End If
                End If

                'MsgBox(codFec2.ToString)


                cantEntAdm = cantSalAdm + cantFinAdm - cantAntAdm



                'CALCULAMOS EL COSTO PROMEDIO ADMISIBLE
                costoPromNuevoAdm = costoPromNuevo 'calcularCostoPromAdm(bodega, articulo, codFecAnt, cantEntAdm, costoEntAdm, "A", costoProm)


                If cantEntAdm < 0 Then
                    cantEntSal = Abs(cantEntAdm)
                    cantEntAdm = 0
                End If

                cantSalAdm += cantEntSal

                If cantAnt = 0 Then
                    costoAntAdm = 0
                Else
                    costoAntAdm = cantAntAdm * costoAnt / cantAnt
                    If Double.IsNaN(costoAntAdm) Or Double.IsInfinity(costoAntAdm) Then
                        costoAntAdm = 0
                    End If
                End If

                If cantEnt = 0 Then
                    costoEntAdm = cantEntAdm * costoPromNuevoAdm
                Else
                    costoEntAdm = cantEntAdm * costoEnt / cantEnt
                    If Double.IsNaN(costoEntAdm) Or Double.IsInfinity(costoEntAdm) Then
                        costoEntAdm = 0
                    End If
                End If

                If cantSal = 0 Then
                    costoSalAdm = cantSalAdm * costoPromNuevoAdm
                Else
                    costoSalAdm = cantSalAdm * costoSal / cantSal
                    If Double.IsNaN(costoSalAdm) Or Double.IsInfinity(costoSalAdm) Then
                        costoSalAdm = 0
                    End If
                End If

                If cantFin = 0 Then
                    costoFinAdm = 0
                Else
                    costoFinAdm = cantFinAdm * costoFin / cantFin
                    If Double.IsNaN(costoFinAdm) Or Double.IsInfinity(costoFinAdm) Then
                        costoFinAdm = 0
                    End If

                End If

                If cantEntAdm = 0 Then
                    If costoEntAdm < 0 Then
                        costoSalAdm = costoSalAdm - costoEntAdm
                        costoEntAdm = 0
                    End If
                End If

                If cantSalAdm = 0 Then
                    If costoSalAdm < 0 Then
                        costoEntAdm = costoEntAdm - costoSalAdm
                        costoSalAdm = 0
                    End If
                End If



                validadorCostFinAdm = costoAntAdm + costoEntAdm - costoSalAdm
                If validadorCostFinAdm <> costoFinAdm Then
                    validadorCostFinAdm = costoFinAdm - validadorCostFinAdm
                    If validadorCostFinAdm < 0 Then
                        If costoSalAdm <> 0 Then
                            costoSalAdm = costoSalAdm + Abs(validadorCostFinAdm)
                        Else
                            If costoEntAdm - Abs(validadorCostFinAdm) >= 0 Then
                                costoEntAdm = costoEntAdm - Abs(validadorCostFinAdm)
                            Else
                                costoSalAdm = costoSalAdm + Abs(validadorCostFinAdm)
                            End If
                        End If


                    Else
                        If costoSalAdm <> 0 Then
                            costoEntAdm = costoEntAdm + Abs(validadorCostFinAdm)
                        Else
                            If costoSalAdm - Abs(validadorCostFinAdm) >= 0 Then
                                costoSalAdm = costoSalAdm - Abs(validadorCostFinAdm)
                            Else
                                costoEntAdm = costoEntAdm + Abs(validadorCostFinAdm)
                            End If
                        End If

                    End If
                End If

                query = " UPDATE VITALICIA.U_CLEV_ANA_ADM_AUD SET CANTIDAD_ANTERIOR=" & cantAntAdm & ",ENTRADAS=" & cantEntAdm & ",SALIDAS=" & cantSalAdm & ",CANTIDAD_FINAL=" & cantFinAdm & ",COSTO_ANTERIOR=" & costoAntAdm & ",COSTO_ENTRADAS=" & costoEntAdm & ",COSTO_SALIDAS=" & costoSalAdm & ",COSTO_FINAL=" & costoFinAdm & ", COSTO_PROM=" & costoPromNuevoAdm & ", AJUSTES=" & cantEntSal & ""
                query += " WHERE ARTICULO ='" & articulo & "'"
                query += " AND BODEGA='" & bodega & "'"
                query += " AND ANIO ='" & anio & "'"
                query += " AND MES ='" & mes & "'"

                ejecutarComandosSQL(query)
                cantEntSal = 0
            Next
        Catch ex As Exception
            MsgBox("No se pudo completar el proceso de cierre.")
            errorP = True
        End Try


        Return 0

    End Function
#End Region




    Function calcularCostoEntradas(ByVal bod As String, ByVal art As String, ByVal codFecAnt As Integer, ByVal codFec As Integer, ByVal cantEntradas As Double, ByVal entradasCosto As Double, ByVal costoProm As Double, ByVal fechaDe As String) As Double


        Dim costoEntradas As Double = 0
        Dim ajuste As Integer


        columna = " ISNULL(SUM(COSTO_TOT_FISC_LOC),0)"

        tabla = esquema & ".TRANSACCION_INV"

        condicion = " WHERE NATURALEZA='C'"
        condicion += " AND ARTICULO='" & art & "'"
        If fechaDe = "D" Then
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),YEAR(FECHA))+RIGHT('0'+CONVERT(VARCHAR(2),MONTH(FECHA)),2))=" & codFec
        Else
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),YEAR(FECHA_HORA_TRANSAC))+RIGHT('0'+CONVERT(VARCHAR(2),MONTH(FECHA_HORA_TRANSAC)),2))=" & codFec
        End If


        ajuste = CInt(DevolverDatoQuery(columna, tabla, condicion))

        If ajuste <> 0 Then
            If bod.ToUpper.Contains("DC") Then
                'columna = "CANTIDAD_FINAL"

                'If fechaDe = "D" Then
                '    tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
                'Else
                '    tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
                'End If

                'condicion = " WHERE ARTICULO='" & art & "'"
                'condicion += " AND BODEGA='" & bod & "'"
                'condicion += " AND R_EVERSE(ANIO)+MES=" & codFecAnt

                'cantidadFinalAnt = CDbl(DevolverDatoQuery(columna, tabla, condicion))



                'columna = "COSTO_FINAL"
                'costoFinalAnt = CDbl(DevolverDatoQuery(columna, tabla, condicion))
                ajuste = ajuste / divisorAjuste(bod, art, codFec)
                costoEntradas = entradasCosto + ajuste '((cantEntradas + cantidadFinalAnt) * costoProm) - costoFinalAnt
                'MsgBox(costoEntradas.ToString)
                'If codFec = 6104 Then

                '    MsgBox("bodega " & bod)
                '    MsgBox("arituculo " & art)
                '    MsgBox("codfec " & codFec)
                '    MsgBox("codfecatn " & codFecAnt)
                '    MsgBox("cantEntrada " & cantEntradas)
                '    MsgBox("costoentradas " & costoEntradas.ToString)
                '    MsgBox("cantEntrada " & cantEntradas)
                '    MsgBox("costoentradas " & costoEntradas.ToString)
                '    MsgBox("cantidadfinalAnterior" & cantidadFinalAnt)
                '    MsgBox("costofinalAnterior" & cantidadFinalAnt)
                '    MsgBox("costo prom " & costoProm)

                'End If
            Else
                costoEntradas = entradasCosto
            End If
        Else
            costoEntradas = entradasCosto
        End If
        Return costoEntradas
    End Function


    Function divisorAjuste(ByVal bod As String, ByVal art As String, ByVal codFec As Integer) As Integer
        Dim divisor As Integer

        query = " SELECT count('a') FROM " & esquema & ".U_CLEV_ANA_TOT_DOC"
        query += " WHERE ARTICULO='" & art & "'"
        query += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec

        query += " AND CANTIDAD_ANTERIOR+ENTRADAS-SALIDAS<>0"

        divisor = DevolverDatoQuery2(query)
        If divisor = 0 Then
            divisor = 1
        End If

        Return divisor
    End Function


#Region "FUNCIONES"
    Sub llenarComboMes()
        cmbMes.Items.Clear()
        Dim contador As Integer
        columna = "COUNT(*)"
        tabla = "(SELECT DISTINCT MONTH(FECHA) MES, YEAR (FECHA) ANIO FROM " & esquema & ".TRANSACCION_INV A "
        tabla += " WHERE CONCAT(MONTH(FECHA),YEAR(FECHA)) NOT IN (SELECT CONCAT(MES,ANIO) FROM VITALICIA.U_CLEV_ANA_CIERRE))VISTA"
        condicion = ""

        contador = CInt(DevolverDatoQuery(columna, tabla, condicion))

        query = " SELECT DISTINCT TOP 1 MONTH(FECHA) MES, YEAR (FECHA) ANIO,CONVERT(INT,CONVERT(VARCHAR(4),YEAR(FECHA))+RIGHT('0'+CONVERT(VARCHAR(2),MONTH(FECHA)),2)) COD"
        query += " FROM " & esquema & ".TRANSACCION_INV A "
        query += " WHERE CONCAT(MONTH(FECHA),YEAR(FECHA)) NOT IN (SELECT CONCAT(MES,ANIO) FROM VITALICIA.U_CLEV_ANA_CIERRE )"
        query += " ORDER BY COD asc  "

        llenarComboBoxOpciones(cmbMes, query, "MES", "ANIO")

        If contador > 0 Then
            cmbMes.SelectedIndex = 0
        End If

    End Sub


    Sub llenarPresupuestos()
        query = " INSERT INTO " & esquema & ".U_CLEV_ANA_PROYECTOS"
        query += " (PROYECTO,AUTORIZADO_APS,ACTIVO,REPORTE)"
        query += " SELECT PROYECTO,0,0,0 "
        query += " FROM " & esquema & ".PROYECTO_PY"
        query += " WHERE PROYECTO NOT IN (SELECT PROYECTO "
        query += " FROM " & esquema & ".U_CLEV_ANA_PROYECTOS)"
        ejecutarComandosSQL(query)
    End Sub

#End Region
    Sub validarCheck()
        Dim estado As Boolean
        estado = checkActivos.Checked + checkNoActivos.Checked + checkNoAutorizados.Checked
        If estado = False Then
            lblError.Text = "Seleccione al menos una opción"
            btnCerrar.Enabled = False
        Else
            btnCerrar.Enabled = True
            lblError.Text = ""
        End If

    End Sub

    Private Sub cerrarMes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        llenarComboMes()

        llenarPresupuestos()
        Dim toolTip1 As New ToolTip()

        ' Set up the delays for the ToolTip.
        toolTip1.AutoPopDelay = 5000
        toolTip1.InitialDelay = 1000
        toolTip1.ReshowDelay = 500
        ' Force the ToolTip text to be displayed whether or not the form is active.
        toolTip1.ShowAlways = True

        ' Set up the ToolTip text for the Button and Checkbox.
        toolTip1.SetToolTip(Me.btnCerrar, "Realizar el cierre del mes seleccionado")
        toolTip1.SetToolTip(Me.btnCancelar, "Volver a la pantalla principal del reporte")
        toolTip1.SetToolTip(Me.btnAjustes, "Aplicar ajustes posteriores al cierre de mes.")
        toolTip1.SetToolTip(Me.btnPasadas, "Mostrar transacciones no incluidas en el cierre de mes")
        toolTip1.SetToolTip(Me.btnPresup, "Definir los presupuestos incluidos en el cierre de mes")
        toolTip1.SetToolTip(Me.btnPass, "Cambiar la contraseña de acceso al cierre de mes")
        toolTip1.SetToolTip(Me.btnBodegas, "Definir las bodegas incluidas en el cierre de mes")


    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        query = " UPDATE " & esquema & ".U_CLEV_ANA_PROYECTOS SET REPORTE=0 "
        ejecutarComandosSQL(query)

        If checkActivos.Checked = True Then
            query = " UPDATE " & esquema & ".U_CLEV_ANA_PROYECTOS SET REPORTE=1 WHERE AUTORIZADO_APS=1 AND ACTIVO=1"
            ejecutarComandosSQL(query)
        End If

        If checkNoActivos.Checked = True Then
            query = " UPDATE " & esquema & ".U_CLEV_ANA_PROYECTOS SET REPORTE=1 WHERE AUTORIZADO_APS=1"
            ejecutarComandosSQL(query)
        End If

        If checkNoAutorizados.Checked = True Then
            query = " UPDATE " & esquema & ".U_CLEV_ANA_PROYECTOS SET REPORTE=1 WHERE AUTORIZADO_APS=0"
            ejecutarComandosSQL(query)
        End If



        bodegasObra.Show()
        Me.Enabled = False

        'btnCerrar.Enabled = False
        ''btnPass.Enabled = False
        ''btnPasadas.Enabled = False

        'anioGlobal = separarCadena(cmbMes.SelectedItem.ToString, True, " --- ")
        'mesGlobal = separarCadena(cmbMes.SelectedItem.ToString, False, " --- ")
        '' lInfo.Text = "Espere por favor..."

        'query = " EXEC " & esquema & ".U_CLEV_ANA_SP_NUEVO_MES"
        'query += " @MES = " & mesGlobal & ", "
        'query += " @ANIO = " & anioGlobal & ""

        'ejecutarComandosSQL(query)

        'llenarTablaMes()
        'llenarTablaMesAud()
        'registros = CStr(dtgMes.RowCount - 1)
        'registrosAud = CStr(dtgMesAud.RowCount - 1)
        'hiloSegundoPlano.RunWorkerAsync(0)

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
        Form1.Visible = True
    End Sub

    Private Sub btnAjustes_Click(sender As System.Object, e As System.EventArgs) Handles btnAjustes.Click
        Ajustes.Show()
        Me.Enabled = False
    End Sub

    Private Sub btnPasadas_Click(sender As System.Object, e As System.EventArgs) Handles btnPasadas.Click
        transaccionesPasadas.Show()
        Me.Enabled = False
    End Sub

    Private Sub checkMostrar_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles checkMostrar.CheckedChanged
        If checkMostrar.Checked = True Then
            pnlOpciones.Visible = True
        Else
            pnlOpciones.Visible = False
        End If
    End Sub

    Private Sub btnPass_Click(sender As System.Object, e As System.EventArgs) Handles btnPass.Click
        cambiarPassword.Show()
        Me.Enabled = False
    End Sub

    Private Sub btnPresup_Click(sender As System.Object, e As System.EventArgs) Handles btnPresup.Click, btnBodegas.Click
        Presupuestos.Show()
        Me.Enabled = False
    End Sub


    Private Sub checkActivos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles checkActivos.CheckedChanged
        validarCheck()


    End Sub

    Private Sub checkNoActivos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles checkNoActivos.CheckedChanged
        validarCheck()

    End Sub

    Private Sub checkNoAutorizados_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles checkNoAutorizados.CheckedChanged
        validarCheck()
    End Sub

    Private Sub btnCerrarTodas_Click(sender As System.Object, e As System.EventArgs)
        Dim contar As Integer
        query = " SELECT COUNT('A')CONTAR FROM ("
        query += " SELECT DISTINCT MONTH(FECHA) MES, YEAR (FECHA) ANIO"
        query += " FROM  VITALERP.VITALICIA.TRANSACCION_INV A "
        query += " WHERE CONCAT(MONTH(FECHA),YEAR(FECHA)) NOT IN (SELECT CONCAT(MES,ANIO) FROM VITALERP.VITALICIA.U_CLEV_ANA_CIERRE )"
        query += " )VISTA"

        contar = CInt(DevolverDatoQuery2(query))

        For i As Integer = 0 To contar Step 1
            highestPercentageReached = 0
            highestPercentageReachedAud = 0


            btnCerrar.Enabled = False

            query = " SELECT ANIO FROM ("
            query += " SELECT DISTINCT TOP 1 MONTH(FECHA) MES, YEAR (FECHA) ANIO"
            query += " FROM  VITALERP.VITALICIA.TRANSACCION_INV A "
            query += " WHERE CONCAT(MONTH(FECHA),YEAR(FECHA)) NOT IN (SELECT CONCAT(MES,ANIO) FROM VITALERP.VITALICIA.U_CLEV_ANA_CIERRE )"
            query += " ORDER BY ANIO,MES"
            query += " )VISTA"
            anioGlobal = DevolverDatoQuery2(query)

            query = " SELECT MES FROM ("
            query += " SELECT DISTINCT TOP 1 MONTH(FECHA) MES, YEAR (FECHA) ANIO"
            query += " FROM  VITALERP.VITALICIA.TRANSACCION_INV A "
            query += " WHERE CONCAT(MONTH(FECHA),YEAR(FECHA)) NOT IN (SELECT CONCAT(MES,ANIO) FROM VITALERP.VITALICIA.U_CLEV_ANA_CIERRE )"
            query += " ORDER BY ANIO,MES"
            query += " )VISTA"
            mesGlobal = DevolverDatoQuery2(query)
            ' lInfo.Text = "Espere por favor..."

            query = " EXEC " & esquema & ".U_CLEV_ANA_SP_NUEVO_MES"
            query += " @MES = " & mesGlobal & ", "
            query += " @ANIO = " & anioGlobal & ""

            ejecutarComandosSQL(query)

            llenarTablaMes()
            llenarTablaMesAud()
            registros = CStr(dtgMes.RowCount - 1)
            registrosAud = CStr(dtgMesAud.RowCount - 1)
            hiloSegundoPlano.RunWorkerAsync(0)





        Next


        query = " SELECT DISTINCT TOP 1 MONTH(FECHA) MES, YEAR (FECHA) ANIO"
        query += " FROM  VITALERP.VITALICIA.TRANSACCION_INV A "
        query += " WHERE CONCAT(MONTH(FECHA),YEAR(FECHA)) NOT IN (SELECT CONCAT(MES,ANIO) FROM VITALERP.VITALICIA.U_CLEV_ANA_CIERRE )"
        query += " ORDER BY ANIO, MES"

    End Sub
End Class