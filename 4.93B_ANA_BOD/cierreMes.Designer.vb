﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cierreMes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cierreMes))
        Me.progress = New System.Windows.Forms.ProgressBar()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbMes = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.hiloSegundoPlano = New System.ComponentModel.BackgroundWorker()
        Me.dtgMes = New System.Windows.Forms.DataGridView()
        Me.lInfo = New System.Windows.Forms.Label()
        Me.hiloSegundoPlanoAud = New System.ComponentModel.BackgroundWorker()
        Me.lInfoAud = New System.Windows.Forms.Label()
        Me.progressAud = New System.Windows.Forms.ProgressBar()
        Me.dtgMesAud = New System.Windows.Forms.DataGridView()
        Me.btnAjustes = New System.Windows.Forms.Button()
        Me.btnPasadas = New System.Windows.Forms.Button()
        Me.btnPass = New System.Windows.Forms.Button()
        Me.checkMostrar = New System.Windows.Forms.CheckBox()
        Me.checkActivos = New System.Windows.Forms.CheckBox()
        Me.checkNoActivos = New System.Windows.Forms.CheckBox()
        Me.checkNoAutorizados = New System.Windows.Forms.CheckBox()
        Me.btnPresup = New System.Windows.Forms.Button()
        Me.pnlOpciones = New System.Windows.Forms.Panel()
        Me.btnBodegas = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblError = New System.Windows.Forms.Label()
        CType(Me.dtgMes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgMesAud, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOpciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'progress
        '
        Me.progress.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.progress.Location = New System.Drawing.Point(50, 284)
        Me.progress.Name = "progress"
        Me.progress.Size = New System.Drawing.Size(400, 31)
        Me.progress.TabIndex = 163
        '
        'btnCerrar
        '
        Me.btnCerrar.BackColor = System.Drawing.SystemColors.Control
        Me.btnCerrar.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnCerrar.FlatAppearance.BorderSize = 2
        Me.btnCerrar.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCerrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCerrar.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrar.ForeColor = System.Drawing.Color.Black
        Me.btnCerrar.Location = New System.Drawing.Point(12, 234)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(236, 44)
        Me.btnCerrar.TabIndex = 162
        Me.btnCerrar.Text = "Cerrar Mes"
        Me.btnCerrar.UseVisualStyleBackColor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnCancelar.FlatAppearance.BorderSize = 2
        Me.btnCancelar.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnCancelar.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.Color.Black
        Me.btnCancelar.Location = New System.Drawing.Point(254, 234)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(238, 44)
        Me.btnCancelar.TabIndex = 161
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(-1, 84)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(505, 28)
        Me.Label3.TabIndex = 160
        Me.Label3.Text = "SELECCIONE EL MES QUE DESEA CERRAR"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmbMes
        '
        Me.cmbMes.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cmbMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMes.FormattingEnabled = True
        Me.cmbMes.Location = New System.Drawing.Point(153, 129)
        Me.cmbMes.Name = "cmbMes"
        Me.cmbMes.Size = New System.Drawing.Size(207, 24)
        Me.cmbMes.TabIndex = 159
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(1, 51)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(503, 37)
        Me.Label2.TabIndex = 158
        Me.Label2.Text = "PROCESO DE CIERRE DE MES"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(1, -1)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(503, 36)
        Me.Label1.TabIndex = 166
        Me.Label1.Text = "ALMACENES DE MATERIALES E INSUMOS"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(2, 29)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(502, 26)
        Me.Label11.TabIndex = 167
        Me.Label11.Text = "FORMULARIO SEG/4.93B"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hiloSegundoPlano
        '
        Me.hiloSegundoPlano.WorkerReportsProgress = True
        Me.hiloSegundoPlano.WorkerSupportsCancellation = True
        '
        'dtgMes
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgMes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgMes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgMes.DefaultCellStyle = DataGridViewCellStyle2
        Me.dtgMes.Location = New System.Drawing.Point(419, 529)
        Me.dtgMes.Name = "dtgMes"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgMes.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dtgMes.RowTemplate.Height = 24
        Me.dtgMes.Size = New System.Drawing.Size(72, 23)
        Me.dtgMes.TabIndex = 168
        Me.dtgMes.Visible = False
        '
        'lInfo
        '
        Me.lInfo.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lInfo.Location = New System.Drawing.Point(50, 318)
        Me.lInfo.Name = "lInfo"
        Me.lInfo.Size = New System.Drawing.Size(400, 25)
        Me.lInfo.TabIndex = 169
        Me.lInfo.Text = "..."
        Me.lInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'hiloSegundoPlanoAud
        '
        Me.hiloSegundoPlanoAud.WorkerReportsProgress = True
        Me.hiloSegundoPlanoAud.WorkerSupportsCancellation = True
        '
        'lInfoAud
        '
        Me.lInfoAud.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lInfoAud.Location = New System.Drawing.Point(50, 381)
        Me.lInfoAud.Name = "lInfoAud"
        Me.lInfoAud.Size = New System.Drawing.Size(400, 25)
        Me.lInfoAud.TabIndex = 170
        Me.lInfoAud.Text = "..."
        Me.lInfoAud.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'progressAud
        '
        Me.progressAud.Location = New System.Drawing.Point(50, 346)
        Me.progressAud.Name = "progressAud"
        Me.progressAud.Size = New System.Drawing.Size(400, 31)
        Me.progressAud.TabIndex = 163
        '
        'dtgMesAud
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgMesAud.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dtgMesAud.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgMesAud.DefaultCellStyle = DataGridViewCellStyle5
        Me.dtgMesAud.Location = New System.Drawing.Point(419, 558)
        Me.dtgMesAud.Name = "dtgMesAud"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgMesAud.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dtgMesAud.RowTemplate.Height = 24
        Me.dtgMesAud.Size = New System.Drawing.Size(72, 23)
        Me.dtgMesAud.TabIndex = 168
        Me.dtgMesAud.Visible = False
        '
        'btnAjustes
        '
        Me.btnAjustes.BackColor = System.Drawing.SystemColors.Control
        Me.btnAjustes.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnAjustes.FlatAppearance.BorderSize = 2
        Me.btnAjustes.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAjustes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAjustes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAjustes.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAjustes.ForeColor = System.Drawing.Color.Black
        Me.btnAjustes.Location = New System.Drawing.Point(48, 3)
        Me.btnAjustes.Name = "btnAjustes"
        Me.btnAjustes.Size = New System.Drawing.Size(197, 69)
        Me.btnAjustes.TabIndex = 171
        Me.btnAjustes.Text = "Aplicar ajustes"
        Me.btnAjustes.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAjustes.UseVisualStyleBackColor = False
        '
        'btnPasadas
        '
        Me.btnPasadas.BackColor = System.Drawing.SystemColors.Control
        Me.btnPasadas.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnPasadas.FlatAppearance.BorderSize = 2
        Me.btnPasadas.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPasadas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPasadas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPasadas.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPasadas.ForeColor = System.Drawing.Color.Black
        Me.btnPasadas.Location = New System.Drawing.Point(248, 3)
        Me.btnPasadas.Name = "btnPasadas"
        Me.btnPasadas.Size = New System.Drawing.Size(197, 69)
        Me.btnPasadas.TabIndex = 172
        Me.btnPasadas.Text = "Ver transacciones no Incluidas"
        Me.btnPasadas.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnPasadas.UseVisualStyleBackColor = False
        '
        'btnPass
        '
        Me.btnPass.BackColor = System.Drawing.SystemColors.Control
        Me.btnPass.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnPass.FlatAppearance.BorderSize = 2
        Me.btnPass.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPass.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPass.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPass.ForeColor = System.Drawing.Color.Black
        Me.btnPass.Location = New System.Drawing.Point(248, 78)
        Me.btnPass.Name = "btnPass"
        Me.btnPass.Size = New System.Drawing.Size(197, 69)
        Me.btnPass.TabIndex = 173
        Me.btnPass.Text = "Cambiar Contraseña"
        Me.btnPass.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnPass.UseVisualStyleBackColor = False
        '
        'checkMostrar
        '
        Me.checkMostrar.AutoSize = True
        Me.checkMostrar.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkMostrar.Location = New System.Drawing.Point(141, 406)
        Me.checkMostrar.Name = "checkMostrar"
        Me.checkMostrar.Size = New System.Drawing.Size(219, 26)
        Me.checkMostrar.TabIndex = 174
        Me.checkMostrar.Text = "Mostrar Opciones Avanzadas"
        Me.checkMostrar.UseVisualStyleBackColor = True
        '
        'checkActivos
        '
        Me.checkActivos.AutoSize = True
        Me.checkActivos.Checked = True
        Me.checkActivos.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkActivos.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkActivos.Location = New System.Drawing.Point(140, 159)
        Me.checkActivos.Name = "checkActivos"
        Me.checkActivos.Size = New System.Drawing.Size(228, 24)
        Me.checkActivos.TabIndex = 175
        Me.checkActivos.Text = "Presupuestos autorizados ACTIVOS"
        Me.checkActivos.UseVisualStyleBackColor = True
        '
        'checkNoActivos
        '
        Me.checkNoActivos.AutoSize = True
        Me.checkNoActivos.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkNoActivos.Location = New System.Drawing.Point(140, 183)
        Me.checkNoActivos.Name = "checkNoActivos"
        Me.checkNoActivos.Size = New System.Drawing.Size(250, 24)
        Me.checkNoActivos.TabIndex = 175
        Me.checkNoActivos.Text = "Presupuestos autorizados NO ACTIVOS"
        Me.checkNoActivos.UseVisualStyleBackColor = True
        '
        'checkNoAutorizados
        '
        Me.checkNoAutorizados.AutoSize = True
        Me.checkNoAutorizados.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkNoAutorizados.Location = New System.Drawing.Point(140, 206)
        Me.checkNoAutorizados.Name = "checkNoAutorizados"
        Me.checkNoAutorizados.Size = New System.Drawing.Size(220, 24)
        Me.checkNoAutorizados.TabIndex = 175
        Me.checkNoAutorizados.Text = "Presupuestos NO AUTORIZADOS"
        Me.checkNoAutorizados.UseVisualStyleBackColor = True
        '
        'btnPresup
        '
        Me.btnPresup.BackColor = System.Drawing.SystemColors.Control
        Me.btnPresup.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnPresup.FlatAppearance.BorderSize = 2
        Me.btnPresup.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPresup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPresup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPresup.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPresup.ForeColor = System.Drawing.Color.Black
        Me.btnPresup.Location = New System.Drawing.Point(46, 78)
        Me.btnPresup.Name = "btnPresup"
        Me.btnPresup.Size = New System.Drawing.Size(197, 69)
        Me.btnPresup.TabIndex = 173
        Me.btnPresup.Text = "Definir Presupuestos habilitados"
        Me.btnPresup.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnPresup.UseVisualStyleBackColor = False
        '
        'pnlOpciones
        '
        Me.pnlOpciones.Controls.Add(Me.btnAjustes)
        Me.pnlOpciones.Controls.Add(Me.btnPasadas)
        Me.pnlOpciones.Controls.Add(Me.btnPass)
        Me.pnlOpciones.Controls.Add(Me.btnBodegas)
        Me.pnlOpciones.Controls.Add(Me.btnPresup)
        Me.pnlOpciones.Location = New System.Drawing.Point(3, 434)
        Me.pnlOpciones.Name = "pnlOpciones"
        Me.pnlOpciones.Size = New System.Drawing.Size(501, 235)
        Me.pnlOpciones.TabIndex = 176
        Me.pnlOpciones.Visible = False
        '
        'btnBodegas
        '
        Me.btnBodegas.BackColor = System.Drawing.SystemColors.Control
        Me.btnBodegas.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBodegas.FlatAppearance.BorderSize = 2
        Me.btnBodegas.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnBodegas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnBodegas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnBodegas.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBodegas.ForeColor = System.Drawing.Color.Black
        Me.btnBodegas.Location = New System.Drawing.Point(152, 153)
        Me.btnBodegas.Name = "btnBodegas"
        Me.btnBodegas.Size = New System.Drawing.Size(197, 69)
        Me.btnBodegas.TabIndex = 173
        Me.btnBodegas.Text = "Bodegas Incluidas"
        Me.btnBodegas.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBodegas.UseVisualStyleBackColor = False
        Me.btnBodegas.Visible = False
        '
        'lblError
        '
        Me.lblError.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblError.ForeColor = System.Drawing.Color.Red
        Me.lblError.Location = New System.Drawing.Point(-1, 206)
        Me.lblError.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(505, 28)
        Me.lblError.TabIndex = 160
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cierreMes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(504, 674)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlOpciones)
        Me.Controls.Add(Me.checkNoAutorizados)
        Me.Controls.Add(Me.checkNoActivos)
        Me.Controls.Add(Me.checkActivos)
        Me.Controls.Add(Me.checkMostrar)
        Me.Controls.Add(Me.lInfoAud)
        Me.Controls.Add(Me.lInfo)
        Me.Controls.Add(Me.dtgMesAud)
        Me.Controls.Add(Me.dtgMes)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.progressAud)
        Me.Controls.Add(Me.progress)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbMes)
        Me.Controls.Add(Me.Label2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "cierreMes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cierre de Mes"
        CType(Me.dtgMes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgMesAud, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOpciones.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents progress As System.Windows.Forms.ProgressBar
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbMes As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents hiloSegundoPlano As System.ComponentModel.BackgroundWorker
    Friend WithEvents dtgMes As System.Windows.Forms.DataGridView
    Friend WithEvents lInfo As System.Windows.Forms.Label
    Friend WithEvents hiloSegundoPlanoAud As System.ComponentModel.BackgroundWorker
    Friend WithEvents lInfoAud As System.Windows.Forms.Label
    Friend WithEvents progressAud As System.Windows.Forms.ProgressBar
    Friend WithEvents dtgMesAud As System.Windows.Forms.DataGridView
    Friend WithEvents btnAjustes As System.Windows.Forms.Button
    Friend WithEvents btnPasadas As System.Windows.Forms.Button
    Friend WithEvents btnPass As System.Windows.Forms.Button
    Friend WithEvents checkMostrar As System.Windows.Forms.CheckBox
    Friend WithEvents checkActivos As System.Windows.Forms.CheckBox
    Friend WithEvents checkNoActivos As System.Windows.Forms.CheckBox
    Friend WithEvents checkNoAutorizados As System.Windows.Forms.CheckBox
    Friend WithEvents btnPresup As System.Windows.Forms.Button
    Friend WithEvents pnlOpciones As System.Windows.Forms.Panel
    Friend WithEvents btnBodegas As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblError As System.Windows.Forms.Label
End Class
