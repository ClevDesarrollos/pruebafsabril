﻿Imports Microsoft.Office.Interop

Public Class Form1

    Dim esquema As String = ObtenerEsquema()
    Public codigoReporte As String = obtenerCodigoXML()
    Dim plantilla As String
    Dim filadatos As String
    Dim columnadatos As String
    Dim ext As String
    ' variables para devolución de datos
    Dim tabla As String
    Dim columna As String
    Dim condicion As String

    ' variables para la inserción de datos
    Dim valores As String
    Dim query As String


    Dim objVentana As Object
    Dim objexcel As Object
    Dim objlibro As Object 'Excel.Workbook
    Dim objHojaExcel As Object
    Dim Archivo As String
    Dim reg As String
    Dim unidad As String
    Dim fechaAux As Date
    Dim ruta As String = obtenerRutaReportes()

    Dim SUBT(30, 3) As String
    Dim Matriz(366, 2) As String
    Dim TotalSubtitulos As Int16
#Region "Querys"
    Sub llenarTablaReporte()
        Dim queryAux As String = ""
        dtgReporte.Columns.Clear()
        Dim anio As Integer = dateReporte.Value.Year
        Dim mes As Integer = dateReporte.Value.Month
        Dim articulo As String = txtArticulo.Text
        Dim bodega As String = txtBodega.Text
        Dim origen As String = ""
        Dim datos As String = ""
        Dim otrasCondiciones As String = ""

        If checkResumen.Checked = True Then
            If radioDocumento.Checked = True Then
                If radioAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_ADM_DOC A"
                    datos = "A.BODEGA, SUM(COSTO_ANTERIOR)COSTO_ANTERIOR,SUM(COSTO_ENTRADAS)COSTO_ENTRADAS,SUM(COSTO_SALIDAS)COSTO_SALIDAS,SUM(COSTO_FINAL)COSTO_FINAL"
                End If
                If radioNoAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_DOC A,VITALERP.VITALICIA.U_CLEV_ANA_ADM_DOC B"
                    datos = " A.BODEGA, SUM(A.COSTO_ANTERIOR)-SUM(B.COSTO_ANTERIOR)COSTO_ANTERIOR,SUM(A.COSTO_ENTRADAS)-SUM(B.COSTO_ENTRADAS)COSTO_ENTRADAS,SUM(A.COSTO_SALIDAS)-SUM(B.COSTO_SALIDAS)COSTO_SALIDAS,SUM(A.COSTO_FINAL)-SUM(B.COSTO_FINAL)COSTO_FINAL"
                    otrasCondiciones = " AND A.ANIO=B.ANIO AND A.ARTICULO=B.ARTICULO AND A.MES=B.MES AND A.BODEGA=B.BODEGA"
                End If
                If radioTodos.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_DOC A"
                    datos = "A.BODEGA, SUM(COSTO_ANTERIOR)COSTO_ANTERIOR,SUM(COSTO_ENTRADAS)COSTO_ENTRADAS,SUM(COSTO_SALIDAS)COSTO_SALIDAS,SUM(COSTO_FINAL)COSTO_FINAL"
                End If
            Else
                If radioAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_ADM_AUD A"
                    datos = " A.BODEGA, SUM(COSTO_ANTERIOR)COSTO_ANTERIOR,SUM(COSTO_ENTRADAS)COSTO_ENTRADAS,SUM(COSTO_SALIDAS)COSTO_SALIDAS,SUM(COSTO_FINAL)COSTO_FINAL"
                End If
                If radioNoAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_AUD A,VITALERP.VITALICIA.U_CLEV_ANA_ADM_AUD B"
                    datos = " A.BODEGA, SUM(A.COSTO_ANTERIOR)-SUM(B.COSTO_ANTERIOR)COSTO_ANTERIOR,SUM(A.COSTO_ENTRADAS)-SUM(B.COSTO_ENTRADAS)COSTO_ENTRADAS,SUM(A.COSTO_SALIDAS)-SUM(B.COSTO_SALIDAS)COSTO_SALIDAS,SUM(A.COSTO_FINAL)-SUM(B.COSTO_FINAL)COSTO_FINAL"
                    otrasCondiciones = " AND A.ANIO=B.ANIO AND A.ARTICULO=B.ARTICULO AND A.MES=B.MES AND A.BODEGA=B.BODEGA"
                End If
                If radioTodos.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_AUD A"
                    datos = "A.BODEGA, SUM(COSTO_ANTERIOR)COSTO_ANTERIOR,SUM(COSTO_ENTRADAS)COSTO_ENTRADAS,SUM(COSTO_SALIDAS)COSTO_SALIDAS,SUM(COSTO_FINAL)COSTO_FINAL"
                End If
            End If


            query = " SELECT "
            query += datos
            query += origen
            query += " WHERE A.ANIO='" & anio & "'"
            query += " AND A.MES='" & mes & "'"
            query += otrasCondiciones
            If checkBodega.Checked = True Then
                query += ""
            Else
                query += " AND A.BODEGA='" & bodega & "'"
            End If
            query += " GROUP BY A.BODEGA"
            llenarTabla(query, dtgReporte)

            If dtgReporte.Columns.Count <> 0 Then

                dtgReporte.Columns("COSTO_ANTERIOR").HeaderText = "COSTO ANTERIOR"
                dtgReporte.Columns("COSTO_ENTRADAS").HeaderText = "COSTO ENTRADAS"
                dtgReporte.Columns("COSTO_SALIDAS").HeaderText = "COSTO SALIDAS"
                dtgReporte.Columns("COSTO_FINAL").HeaderText = "COSTO FINAL"
                dtgReporte.Columns("BODEGA").HeaderText = "BODEGA"


                dtgReporte.Columns("COSTO_ANTERIOR").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("COSTO_ENTRADAS").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("COSTO_SALIDAS").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("COSTO_FINAL").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("BODEGA").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

                dtgReporte.Columns("COSTO_ANTERIOR").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("COSTO_ENTRADAS").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("COSTO_SALIDAS").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("COSTO_FINAL").DefaultCellStyle.Format = "###,##0.00 "

            End If
        Else
            datos = " A.BODEGA BODEGA,A.ARTICULO CODIGO, (SELECT DESCRIPCION FROM VITALICIA.ARTICULO WHERE A.ARTICULO=ARTICULO) ARTICULO, (SELECT UNIDAD_ALMACEN FROM VITALICIA.ARTICULO WHERE A.ARTICULO=ARTICULO) UM,"
            If radioDocumento.Checked = True Then
                If radioAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_ADM_DOC A"
                    datos += " CANTIDAD_ANTERIOR,ENTRADAS,SALIDAS,CANTIDAD_FINAL,COSTO_ANTERIOR,COSTO_ENTRADAS,COSTO_SALIDAS,COSTO_FINAL"
                End If
                If radioNoAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_DOC A,VITALERP.VITALICIA.U_CLEV_ANA_ADM_DOC B"
                    datos += " (A.CANTIDAD_ANTERIOR-B.CANTIDAD_ANTERIOR)CANTIDAD_ANTERIOR,(A.ENTRADAS-B.ENTRADAS)ENTRADAS,(A.SALIDAS-B.SALIDAS)SALIDAS,(A.CANTIDAD_FINAL-B.CANTIDAD_FINAL)CANTIDAD_FINAL,(A.COSTO_ANTERIOR-B.COSTO_ANTERIOR)COSTO_ANTERIOR,(A.COSTO_ENTRADAS-B.COSTO_ENTRADAS)COSTO_ENTRADAS,(A.COSTO_SALIDAS-B.COSTO_SALIDAS)COSTO_SALIDAS,(A.COSTO_FINAL-B.COSTO_FINAL)COSTO_FINAL"
                    otrasCondiciones = " AND A.ANIO=B.ANIO AND A.ARTICULO=B.ARTICULO AND A.MES=B.MES AND A.BODEGA=B.BODEGA"
                End If
                If radioTodos.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_DOC A"
                    datos += " CANTIDAD_ANTERIOR,ENTRADAS,SALIDAS,CANTIDAD_FINAL,COSTO_ANTERIOR,COSTO_ENTRADAS,COSTO_SALIDAS,COSTO_FINAL"
                End If
            Else
                If radioAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_ADM_AUD A"
                    datos += " CANTIDAD_ANTERIOR,ENTRADAS,SALIDAS,CANTIDAD_FINAL,COSTO_ANTERIOR,COSTO_ENTRADAS,COSTO_SALIDAS,COSTO_FINAL"
                End If
                If radioNoAdmisible.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_AUD A,VITALERP.VITALICIA.U_CLEV_ANA_ADM_AUD B"
                    datos += " (A.CANTIDAD_ANTERIOR-B.CANTIDAD_ANTERIOR)CANTIDAD_ANTERIOR,(A.ENTRADAS-B.ENTRADAS)ENTRADAS,(A.SALIDAS-B.SALIDAS)SALIDAS,(A.CANTIDAD_FINAL-B.CANTIDAD_FINAL)CANTIDAD_FINAL,(A.COSTO_ANTERIOR-B.COSTO_ANTERIOR)COSTO_ANTERIOR,(A.COSTO_ENTRADAS-B.COSTO_ENTRADAS)COSTO_ENTRADAS,(A.COSTO_SALIDAS-B.COSTO_SALIDAS)COSTO_SALIDAS,(A.COSTO_FINAL-B.COSTO_FINAL)COSTO_FINAL"
                    otrasCondiciones = " AND A.ANIO=B.ANIO AND A.ARTICULO=B.ARTICULO AND A.MES=B.MES AND A.BODEGA=B.BODEGA"
                End If
                If radioTodos.Checked = True Then
                    origen = " FROM VITALERP.VITALICIA.U_CLEV_ANA_TOT_AUD A"
                    datos += " CANTIDAD_ANTERIOR,ENTRADAS,SALIDAS,CANTIDAD_FINAL,COSTO_ANTERIOR,COSTO_ENTRADAS,COSTO_SALIDAS,COSTO_FINAL"
                End If
            End If



            query = " SELECT "


            query += datos

            query += origen

            query += " WHERE A.ANIO='" & anio & "'"
            query += " AND A.MES='" & mes & "'"
            If checkArticulo.Checked = True Then
                query += ""
            Else
                query += " AND A.ARTICULO='" & articulo & "'"
            End If
            If checkBodega.Checked = True Then
                query += ""
            Else
                query += " AND A.BODEGA='" & bodega & "'"
            End If
            query += otrasCondiciones
            queryAux = query

            query += " ORDER BY BODEGA,ARTICULO "


            llenarTabla(query, dtgReporte)
            If dtgReporte.Columns.Count <> 0 Then

                dtgReporte.Columns("CANTIDAD_ANTERIOR").HeaderText = "CANTIDAD ANTERIOR"
                dtgReporte.Columns("ENTRADAS").HeaderText = "CANTIDAD ENTRADAS"
                dtgReporte.Columns("SALIDAS").HeaderText = "CANTIDAD SALIDAS"
                dtgReporte.Columns("CANTIDAD_FINAL").HeaderText = "CANTIDAD FINAL"
                dtgReporte.Columns("COSTO_ANTERIOR").HeaderText = "COSTO ANTERIOR"
                dtgReporte.Columns("COSTO_ENTRADAS").HeaderText = "COSTO ENTRADAS"
                dtgReporte.Columns("COSTO_SALIDAS").HeaderText = "COSTO SALIDAS"
                dtgReporte.Columns("COSTO_FINAL").HeaderText = "COSTO FINAL"
                dtgReporte.Columns("BODEGA").HeaderText = "BODEGA"
                dtgReporte.Columns("CODIGO").HeaderText = "CODIGO DE ARTICULO"
                dtgReporte.Columns("UM").HeaderText = "UM"
                dtgReporte.Columns("ARTICULO").HeaderText = "DESCRIPCIÓN DE ARTÍCULO"

                dtgReporte.Columns("CANTIDAD_ANTERIOR").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("ENTRADAS").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("SALIDAS").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("CANTIDAD_FINAL").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("COSTO_ANTERIOR").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("COSTO_ENTRADAS").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("COSTO_SALIDAS").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("COSTO_FINAL").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("BODEGA").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("CODIGO").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dtgReporte.Columns("UM").Width = 100
                dtgReporte.Columns("ARTICULO").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

                dtgReporte.Columns("CANTIDAD_ANTERIOR").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("ENTRADAS").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("SALIDAS").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("CANTIDAD_FINAL").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("COSTO_ANTERIOR").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("COSTO_ENTRADAS").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("COSTO_SALIDAS").DefaultCellStyle.Format = "###,##0.00 "
                dtgReporte.Columns("COSTO_FINAL").DefaultCellStyle.Format = "###,##0.00 "



            End If



        End If



        If queryAux = "" Then
            Dim A As Integer = 0
        Else
            query = "SELECT DISTINCT BODEGA FROM ( " + queryAux + " )VISTA  ORDER BY BODEGA"
            llenarTabla(query, dtgBodegas)
        End If


        'Dim datosV(datos.Split(",").Count()) As String
        'datosV = datos.Split(",")
        'For Each dato As String In datosV
        '    If dato = "CANTIDAD_ANTERIOR" Then
        '        MsgBox(dato)
        '       
        '    End If
        '    'If dato = "ENTRADAS" Then
        '    '    dtgReporte.Columns("ENTRADAS").HeaderText = "CANTIDAD ENTRADAS"
        '    'End If
        '    'If dato = "SALIDAS" Then
        '    '    dtgReporte.Columns("SALIDAS").HeaderText = "CANTIDAD SALIDAS"
        '    'End If
        '    'If dato = "CANTIDAD_FINAL" Then
        '    '    dtgReporte.Columns("CANTIDAD_FINAL").HeaderText = "CANTIDAD FINAL"
        '    'End If

        '    'If dato = "COSTO_ANTERIOR" Then
        '    '    dtgReporte.Columns("COSTO_ANTERIOR").HeaderText = "COSTO ANTERIOR"
        '    'End If
        '    'If dato = "COSTO_ENTRADAS" Then
        '    '    dtgReporte.Columns("COSTO_ENTRADAS").HeaderText = "COSTO ENTRADAS"
        '    'End If
        '    'If dato = "COSTO_SALIDAS" Then
        '    '    dtgReporte.Columns("COSTO_SALIDAS").HeaderText = "COSTO SALIDAS"
        '    'End If
        '    'If dato = "COSTO_FINAL" Then
        '    '    dtgReporte.Columns("COSTO_FINAL").HeaderText = "COSTO FINAL"
        '    'End If


        '    'dtgReporte.Columns(dato).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

        'Next


        If dtgReporte.RowCount = 1 Then
            btnGenExl.Enabled = False
        Else
            btnGenExl.Enabled = True
        End If
    End Sub



#End Region
#Region "FUNCIONES"

    'Sub llenarTablaBodegas()
    '    If txtBodega.Text = "" Then
    '        query = " SELECT DISTINCT BODEGA "
    '        query += " FROM " & esquema & ".U_CLEV_ANA_TOT_DOC A ORDER BY BODEGA"
    '        llenarTabla(query, dtgBodegas)
    '    Else
    '        query = " SELECT DISTINCT BODEGA "
    '        query += " FROM " & esquema & ".U_CLEV_ANA_TOT_DOC A WHERE BODEGA='" & txtBodega.Text & "'  ORDER BY BODEGA"
    '        llenarTabla(query, dtgBodegas)
    '    End If
    'End Sub
    Sub crearPassword()
        Dim cont As Integer

        columna = "COUNT(*)"
        tabla = esquema & ".U_CLEV_REP_ACCESOS"
        condicion = "WHERE REPORTE='" & codigoReporte & "'"
        valores = ""

        cont = CInt(DevolverDatoQuery(columna, tabla, condicion))
        If cont = 0 Then
            columna = "REPORTE,PASS"
            tabla = esquema & ".U_CLEV_REP_ACCESOS"
            condicion = ""
            valores = "'" & codigoReporte & "','" & Encriptar("Clever2016") & "'"
            insertarDatos(tabla, columna, valores)
        End If
    End Sub

    Sub llenarUltimoMesCierre()
        query = "SELECT TOP 1 CONCAT(MES,'-',ANIO),anio,mes FROM VITALICIA.U_CLEV_ANA_CIERRE ORDER BY RecordDate desc"
        lblCierre.Text = DevolverDatoQuery2(query)
    End Sub

    Function mostrarDescArticulo(ByVal articulo As String) As String
        Dim desc As String
        If articulo = "" Then
            desc = "---"
        Else
            query = "SELECT DESCRIPCION FROM " & esquema & ".ARTICULO WHERE ARTICULO='" & articulo & "'"
            Try
                desc = DevolverDatoQuery2(query)
            Catch ex As Exception
                desc = ""
            End Try
        End If

        If desc = "" Then
            desc = "ARTÍCULO NO VÁLIDO"
        End If

        Return desc
    End Function

    Function mostrarDescBodega(ByVal articulo As String) As String
        Dim desc As String
        If articulo = "" Then
            desc = "---"
        Else
            query = "SELECT NOMBRE FROM " & esquema & ".BODEGA WHERE BODEGA='" & articulo & "'"
            Try
                desc = DevolverDatoQuery2(query)
            Catch ex As Exception
                desc = ""
            End Try
        End If

        If desc = "" Then
            desc = "BODEGA NO VÁLIDA"
        End If

        Return desc
    End Function
#Region "Cabecera"
    Function obtenerIndiceCabecera() As Integer
        Dim indice As Integer
        Dim sql As String
        Dim tabla As String
        sql = "select count(*) "
        sql += "from " & esquema & ".U_DATOS_REPS_CABS"
        tabla = "" & esquema & ".U_DATOS_REPS_CABS"
        indice = CInt(DevolverDatoQuery2(sql)) + 1
        Return indice
    End Function

    Sub cargarCabecera()
        Dim sql As String
        Dim tabla As String
        Dim cantFilas As Integer
        Dim cantColumnas As Integer
        sql = "select U_ENTIDAD ENTIDAD, u_nit NIT, u_responsable RESPONSABLE, U_cargo CARGO, u_telefono TELEFONO, u_codigo "
        sql += "from " & esquema & ".U_DATOS_REPS_CABS where u_codproy = '" & codigoReporte & "' and U_ESTADO = 'ACTIVO'"
        tabla = "" & esquema & ".U_DATOS_REPS_CABS"

        llenarTabla(sql, dtgCabecera)

        cantFilas = dtgCabecera.RowCount - 1
        cantColumnas = dtgCabecera.ColumnCount
        Dim cabecera(cantColumnas) As String

        If cantFilas > 0 Then
            For i As Int16 = 0 To cantColumnas - 1 Step 1
                cabecera(i) = dtgCabecera.Item(i, 0).Value.ToString()
            Next

            txtEntidad.Text = cabecera(0)
            txtNit.Text = cabecera(1)
            txtResponsable.Text = cabecera(2)
            txtCargo.Text = cabecera(3)
            txtTelefono.Text = cabecera(4)
        Else
            MsgBox("No existen datos disponibles para la cabecera del reporte seleccionado. Llene los campos vacios con los datos del reporte.")
            txtEntidad.Text = ""
            txtNit.Text = ""
            txtResponsable.Text = ""
            txtCargo.Text = ""
            txtTelefono.Text = ""

        End If
    End Sub
#End Region


#Region "generar excel"
    Sub generarExcel(ByVal bod As String)

        Dim cantFilas As Integer = dtgReporte.RowCount - 1
        Dim cantColumnas As Integer = dtgReporte.ColumnCount + 3
        Dim bodega(cantFilas) As String
        Dim admisible(cantFilas) As String
        Dim localizacion(cantFilas) As String
        Dim articulo(cantFilas) As String
        Dim descripcion(cantFilas) As String
        Dim unidad(cantFilas) As String
        Dim anteriorCantidad(cantFilas) As Double
        Dim entradasCantidad(cantFilas) As Double
        Dim salidasCantidad(cantFilas) As Double
        Dim finalCantidad(cantFilas) As Double
        Dim anteriorCosto(cantFilas) As Double
        Dim entradasCosto(cantFilas) As Double
        Dim salidasCosto(cantFilas) As Double
        Dim finalCosto(cantFilas) As Double
        Dim sumAntCosto As Double = 0
        Dim sumEntCosto As Double = 0
        Dim sumSalCosto As Double = 0
        Dim sumFinCosto As Double = 0
        Dim corr As Integer = 0
        For i As Integer = 0 To cantFilas - 1 Step 1

            If dtgReporte.Rows(i).Cells("BODEGA").Value.ToString() = bod Then
                bodega(corr) = dtgReporte.Rows(i).Cells("BODEGA").Value.ToString()
                admisible(corr) = ""

                localizacion(corr) = ""
                articulo(corr) = dtgReporte.Rows(i).Cells("CODIGO").Value.ToString()
                descripcion(corr) = (dtgReporte.Rows(i).Cells("ARTICULO").Value.ToString())
                unidad(corr) = (dtgReporte.Rows(i).Cells("UM").Value.ToString())


                anteriorCantidad(corr) = CDbl(dtgReporte.Rows(i).Cells("CANTIDAD_ANTERIOR").Value.ToString())
                entradasCantidad(corr) = CDbl(dtgReporte.Rows(i).Cells("ENTRADAS").Value.ToString())
                salidasCantidad(corr) = CDbl(dtgReporte.Rows(i).Cells("SALIDAS").Value.ToString())
                finalCantidad(corr) = CDbl(dtgReporte.Rows(i).Cells("CANTIDAD_FINAL").Value.ToString())

                anteriorCosto(corr) = CDbl(dtgReporte.Rows(i).Cells("COSTO_ANTERIOR").Value.ToString())
                entradasCosto(corr) = CDbl(dtgReporte.Rows(i).Cells("COSTO_ENTRADAS").Value.ToString())
                salidasCosto(corr) = CDbl(dtgReporte.Rows(i).Cells("COSTO_SALIDAS").Value.ToString())
                finalCosto(corr) = CDbl(dtgReporte.Rows(i).Cells("COSTO_FINAL").Value.ToString())


                sumAntCosto += anteriorCosto(corr)
                sumEntCosto += entradasCosto(corr)
                sumFinCosto += finalCosto(corr)
                sumSalCosto += salidasCosto(corr)
                corr += 1
            End If

        Next





        Dim RutaTemp, Extension, ArchivoFecha As String
        Dim NombrePlantilla As String = ""
        RutaTemp = ruta
        Archivo = plantilla
        Extension = ext
        ArchivoFecha = Archivo & "_" & Now.ToString("yyyyMMddhhmmss")
        ArchivoFecha += Extension
        Archivo += Extension

        Try

            If RutaTemp = "" Then RutaTemp = My.Application.Info.DirectoryPath '& "\Config.xml"
            If My.Computer.FileSystem.FileExists(RutaTemp & "\" & Archivo & Extension) Then My.Computer.FileSystem.DeleteFile(RutaTemp & "\" & Archivo & Extension)
            My.Computer.FileSystem.CopyFile(My.Application.Info.DirectoryPath & "\PLANTILLAS\" + Archivo, RutaTemp & "\" & ArchivoFecha) ' & Now.ToString("yyyyMMddhhmmss") & ".xls")
            objexcel = CreateObject("Excel.Application")
            objlibro = objexcel.Workbooks.Open(RutaTemp & "\" & ArchivoFecha)  'CON ESTA INSTRUCCION ABRE UN ARCHIVO EXISTENTE
            objexcel.Visible = False
            objHojaExcel = objlibro.Worksheets(1)
            objHojaExcel.Activate()

            ' ***********************LLENAR SUBTITULOS MEDIANTE CONFIG.XML***********************
            Dim ter As String

            LlenarMatrizSubtitulos(SUBT, TotalSubtitulos)
            For i As Int16 = 1 To TotalSubtitulos
                ter = SUBT(i, 2)

                objHojaExcel.Columns(Val(SUBT(i, 2))).Rows(Val(SUBT(i, 1))).Value = SUBT(i, 0)
            Next
            ' ******************************************************************************************
            cantFilas = corr
            progress1.Maximum = cantFilas

            For i As Int16 = 0 To corr - 1 Step 1
                progress1.Value = i


                objHojaExcel.Columns(1).Rows(i + CInt(filadatos)).Value = bodega(i)
                objHojaExcel.Columns(2).Rows(i + CInt(filadatos)).Value = localizacion(i)
                objHojaExcel.Columns(3).Rows(i + CInt(filadatos)).Value = admisible(i)
                objHojaExcel.Columns(4).Rows(i + CInt(filadatos)).Value = articulo(i)
                objHojaExcel.Columns(5).Rows(i + CInt(filadatos)).Value = descripcion(i)
                objHojaExcel.Columns(6).Rows(i + CInt(filadatos)).Value = unidad(i)
                objHojaExcel.Columns(7).Rows(i + CInt(filadatos)).Value = anteriorCantidad(i)
                objHojaExcel.Columns(8).Rows(i + CInt(filadatos)).Value = entradasCantidad(i)
                objHojaExcel.Columns(9).Rows(i + CInt(filadatos)).Value = salidasCantidad(i)
                objHojaExcel.Columns(10).Rows(i + CInt(filadatos)).Value = finalCantidad(i)
                objHojaExcel.Columns(11).Rows(i + CInt(filadatos)).Value = anteriorCosto(i)
                objHojaExcel.Columns(12).Rows(i + CInt(filadatos)).Value = entradasCosto(i)
                objHojaExcel.Columns(13).Rows(i + CInt(filadatos)).Value = salidasCosto(i)
                objHojaExcel.Columns(14).Rows(i + CInt(filadatos)).Value = finalCosto(i)

                objHojaExcel.Columns(1).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(2).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(3).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(4).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(5).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(6).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(7).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(8).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(9).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(10).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(11).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(12).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(13).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(14).Rows(i + CInt(filadatos)).Borders.LineStyle = 1

                objHojaExcel.Columns(1).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                objHojaExcel.Columns(2).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                objHojaExcel.Columns(3).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                objHojaExcel.Columns(4).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                objHojaExcel.Columns(5).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                objHojaExcel.Columns(6).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()

            Next

            objHojaExcel.Columns(6).Rows(8).Value = txtEntidad.Text
            objHojaExcel.Columns(6).Rows(9).Value = txtResponsable.Text


            objHojaExcel.Columns(6).Rows(11).Value = ""
            fechaAux = dateEmision.Text
            objHojaExcel.Columns(6).Rows(12).Value = Format(fechaAux, "yyyy/MM/dd")

            fechaAux = lblFechaReporte.Text
            objHojaExcel.Columns(6).Rows(13).Value = Format(fechaAux, "yyyy/MM/dd")

            fechaAux = dateEntrega.Text
            objHojaExcel.Columns(6).Rows(14).Value = Format(fechaAux, "yyyy/MM/dd")

            objHojaExcel.Columns(6).Rows(15).Value = txtTipoCambio.Text

            objHojaExcel.Columns(4).Rows(5).Value = "(Expresado en Bs)"

            objHojaExcel.Columns(11).Rows(8).Value = txtNit.Text
            objHojaExcel.Columns(14).Rows(12).Value = txtTelefono.Text
            objHojaExcel.Columns(11).Rows(11).Value = txtCargo.Text

            objHojaExcel.Columns(4).Rows(4).Value = ConvertirMes(dateReporte.Value.Month)

            objHojaExcel.Columns(cantColumnas - 6).Rows(cantFilas + CInt(filadatos)).Value = "Total Depósito "

            objHojaExcel.Columns(cantColumnas - 4).Rows(cantFilas + CInt(filadatos)).Value = sumAntCosto
            objHojaExcel.Columns(cantColumnas - 4).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            objHojaExcel.Columns(cantColumnas - 4).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()
            objHojaExcel.Columns(cantColumnas - 3).Rows(cantFilas + CInt(filadatos)).Value = sumEntCosto
            objHojaExcel.Columns(cantColumnas - 3).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            objHojaExcel.Columns(cantColumnas - 3).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()
            objHojaExcel.Columns(cantColumnas - 2).Rows(cantFilas + CInt(filadatos)).Value = sumSalCosto
            objHojaExcel.Columns(cantColumnas - 2).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            objHojaExcel.Columns(cantColumnas - 2).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()
            objHojaExcel.Columns(cantColumnas - 1).Rows(cantFilas + CInt(filadatos)).Value = sumFinCosto
            objHojaExcel.Columns(cantColumnas - 1).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            objHojaExcel.Columns(cantColumnas - 1).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()

            progress1.Value = 0
        Catch ex As Exception
            If objexcel Is Nothing Then
            Else
                objexcel.Quit()
                objexcel = Nothing
            End If
            MsgBox(ex.ToString)
            objexcel.Visible = True
            Me.Cursor = Cursors.Arrow
            Exit Sub
        End Try
        objexcel.Visible = True




    End Sub


    Sub generarExcelResumen()

        Dim cantFilas As Integer = dtgReporte.RowCount - 1
        Dim cantColumnas As Integer = dtgReporte.ColumnCount + 3
        Dim bodega(cantFilas) As String
        Dim admisible(cantFilas) As String
        Dim localizacion(cantFilas) As String
        Dim articulo(cantFilas) As String
        Dim descripcion(cantFilas) As String
        Dim unidad(cantFilas) As String
        Dim anteriorCosto(cantFilas) As Double
        Dim entradasCosto(cantFilas) As Double
        Dim salidasCosto(cantFilas) As Double
        Dim finalCosto(cantFilas) As Double
        Dim sumAntCosto As Double = 0
        Dim sumEntCosto As Double = 0
        Dim sumSalCosto As Double = 0
        Dim sumFinCosto As Double = 0

        For i As Integer = 0 To cantFilas - 1 Step 1

            bodega(i) = dtgReporte.Rows(i).Cells("BODEGA").Value.ToString()
            admisible(i) = ""
            localizacion(i) = ""
            anteriorCosto(i) = CDbl(dtgReporte.Rows(i).Cells("COSTO_ANTERIOR").Value.ToString())
            entradasCosto(i) = CDbl(dtgReporte.Rows(i).Cells("COSTO_ENTRADAS").Value.ToString())
            salidasCosto(i) = CDbl(dtgReporte.Rows(i).Cells("COSTO_SALIDAS").Value.ToString())
            finalCosto(i) = CDbl(dtgReporte.Rows(i).Cells("COSTO_FINAL").Value.ToString())


            sumAntCosto += anteriorCosto(i)
            sumEntCosto += entradasCosto(i)
            sumFinCosto += finalCosto(i)
            sumSalCosto += salidasCosto(i)
        Next





        Dim RutaTemp, Extension, ArchivoFecha As String
        Dim NombrePlantilla As String = ""
        RutaTemp = ruta
        Archivo = "4.93B - C"
        Extension = ext
        ArchivoFecha = Archivo & "_" & Now.ToString("yyyyMMddhhmmss")
        ArchivoFecha += Extension
        Archivo += Extension

        Try

            If RutaTemp = "" Then RutaTemp = My.Application.Info.DirectoryPath '& "\Config.xml"
            If My.Computer.FileSystem.FileExists(RutaTemp & "\" & Archivo & Extension) Then My.Computer.FileSystem.DeleteFile(RutaTemp & "\" & Archivo & Extension)
            My.Computer.FileSystem.CopyFile(My.Application.Info.DirectoryPath & "\PLANTILLAS\" + Archivo, RutaTemp & "\" & ArchivoFecha) ' & Now.ToString("yyyyMMddhhmmss") & ".xls")
            objexcel = CreateObject("Excel.Application")
            objlibro = objexcel.Workbooks.Open(RutaTemp & "\" & ArchivoFecha)  'CON ESTA INSTRUCCION ABRE UN ARCHIVO EXISTENTE
            objexcel.Visible = False
            objHojaExcel = objlibro.Worksheets(1)
            objHojaExcel.Activate()

            ' ***********************LLENAR SUBTITULOS MEDIANTE CONFIG.XML***********************
            'Dim ter As String

            'LlenarMatrizSubtitulos(SUBT, TotalSubtitulos)
            'For i As Int16 = 1 To TotalSubtitulos
            '    ter = SUBT(i, 2)

            '    objHojaExcel.Columns(Val(SUBT(i, 2))).Rows(Val(SUBT(i, 1))).Value = SUBT(i, 0)
            'Next
            ' ******************************************************************************************

            progress1.Maximum = cantFilas

            For i As Int16 = 0 To cantFilas - 1 Step 1
                progress1.Value = i


                objHojaExcel.Columns(1).Rows(i + CInt(filadatos)).Value = (i + 1)
                objHojaExcel.Columns(2).Rows(i + CInt(filadatos)).Value = bodega(i)
                objHojaExcel.Columns(3).Rows(i + CInt(filadatos)).Value = anteriorCosto(i)
                objHojaExcel.Columns(4).Rows(i + CInt(filadatos)).Value = entradasCosto(i)
                objHojaExcel.Columns(5).Rows(i + CInt(filadatos)).Value = salidasCosto(i)
                objHojaExcel.Columns(6).Rows(i + CInt(filadatos)).Value = finalCosto(i)


                objHojaExcel.Columns(1).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(2).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(3).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(4).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(5).Rows(i + CInt(filadatos)).Borders.LineStyle = 1
                objHojaExcel.Columns(6).Rows(i + CInt(filadatos)).Borders.LineStyle = 1

                'objHojaExcel.Columns(1).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                'objHojaExcel.Columns(2).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                'objHojaExcel.Columns(3).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                'objHojaExcel.Columns(4).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                'objHojaExcel.Columns(5).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()
                'objHojaExcel.Columns(6).Rows(i + CInt(filadatos)).EntireColumn.AutoFit()

            Next

            '************************************* LLENADO DE SUBTITULOS***************************
            objHojaExcel.Columns(1).Rows(8).Value = objHojaExcel.Columns(1).Rows(8).Value + txtEntidad.Text
            objHojaExcel.Columns(1).Rows(9).Value = objHojaExcel.Columns(1).Rows(9).Value + txtResponsable.Text

            objHojaExcel.Columns(6).Rows(9).Value = txtNit.Text

            objHojaExcel.Columns(6).Rows(11).Value = ""
            fechaAux = dateEmision.Text
            objHojaExcel.Columns(3).Rows(12).Value = Format(fechaAux, "yyyy/MM/dd")

            fechaAux = lblFechaReporte.Text
            objHojaExcel.Columns(3).Rows(13).Value = Format(fechaAux, "yyyy/MM/dd")

            fechaAux = dateEntrega.Text
            objHojaExcel.Columns(3).Rows(14).Value = Format(fechaAux, "yyyy/MM/dd")

            objHojaExcel.Columns(3).Rows(15).Value = txtTipoCambio.Text

            objHojaExcel.Columns(5).Rows(12).Value = objHojaExcel.Columns(5).Rows(12).Value + txtCargo.Text


            objHojaExcel.Columns(6).Rows(13).Value = txtTelefono.Text


            objHojaExcel.Columns(1).Rows(4).Value = ConvertirMes(dateReporte.Value.Month)

            objHojaExcel.Columns(cantColumnas - 6).Rows(cantFilas + CInt(filadatos)).Value = "Total Depósito "

            objHojaExcel.Columns(cantColumnas - 5).Rows(cantFilas + CInt(filadatos)).Value = sumAntCosto
            objHojaExcel.Columns(cantColumnas - 5).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            'objHojaExcel.Columns(cantColumnas - 5).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()
            objHojaExcel.Columns(cantColumnas - 4).Rows(cantFilas + CInt(filadatos)).Value = sumEntCosto
            objHojaExcel.Columns(cantColumnas - 4).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            'objHojaExcel.Columns(cantColumnas - 4).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()
            objHojaExcel.Columns(cantColumnas - 3).Rows(cantFilas + CInt(filadatos)).Value = sumSalCosto
            objHojaExcel.Columns(cantColumnas - 3).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            'objHojaExcel.Columns(cantColumnas - 3).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()
            objHojaExcel.Columns(cantColumnas - 2).Rows(cantFilas + CInt(filadatos)).Value = sumFinCosto
            objHojaExcel.Columns(cantColumnas - 2).Rows(cantFilas + CInt(filadatos)).Borders.LineStyle = 1
            'objHojaExcel.Columns(cantColumnas - 2).Rows(cantFilas + CInt(filadatos)).EntireColumn.AutoFit()

            progress1.Value = 0
        Catch ex As Exception
            If objexcel Is Nothing Then
            Else
                objexcel.Quit()
                objexcel = Nothing
            End If
            MsgBox(ex.ToString)
            objexcel.Visible = True
            Me.Cursor = Cursors.Arrow
            Exit Sub
        End Try
        objexcel.Visible = True




    End Sub


    Function ConvertirMes(ByRef number As Integer)
        Dim mes As String

        Select Case number
            Case 1
                mes = "Del 01-01-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 31-01-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 2
                Dim a As String
                If CInt(dateReporte.Value.Year) Mod 4 = 0 Then
                    a = "29"
                Else
                    a = "28"
                End If

                mes = "Del 01-02-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al " & a & "-02-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 3
                mes = "Del 01-03-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 31-03-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 4
                mes = "Del 01-04-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 30-04-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 5
                mes = "Del 01-05-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 31-05-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 6
                mes = "Del 01-06-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 30-06-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 7
                mes = "Del 01-07-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 31-07-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 8
                mes = "Del 01-08-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 31-08-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 9
                mes = "Del 01-09-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 30-09-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 10
                mes = "Del 01-10-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 31-10-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 11
                mes = "Del 01-11-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 30-11-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case 12
                mes = "Del 01-12-" & dateReporte.Value.Year.ToString.Substring(2, 2) & " al 31-12-" & dateReporte.Value.Year.ToString.Substring(2, 2)
            Case Else
                mes = "No encontrado"
        End Select
        Return mes
    End Function


#End Region

#End Region

    Private Sub btnCierre_Click(sender As System.Object, e As System.EventArgs) Handles btnCierre.Click
        pantallaPass.lblAnio.Text = ""
        pantallaPass.lblMes.Text = ""
        pantallaPass.Show()
        Me.Enabled = False
    End Sub



    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        crearPassword()
        llenarUltimoMesCierre()
        obtenerDatosReportes(plantilla, ext, filadatos, columnadatos)
        dateReporte.CustomFormat = "MM / yyyy"
        lblFechaReporte.Text = DateTime.Now().ToString("dd/MM/yyyy")
        cargarCabecera()
        txtDescArticulo.Text = "---"
        txtDescBodega.Text = "---"
    End Sub

    Private Sub txtTelefono_Leave(sender As System.Object, e As System.EventArgs) Handles txtTelefono.Leave
        Dim indice = obtenerIndiceCabecera()
        Dim update As String = "update " & esquema & ".U_DATOS_REPS_CABS set U_estado = 'INACTIVO' WHERE U_estado = 'ACTIVO'AND U_CODPROY = '" & codigoReporte & "'"
        ejecutarComandosSQL(update)
        Dim insertar As String = "INSERT INTO " & esquema & ".U_DATOS_REPS_CABS (U_CODIGO, U_DESCRIP, U_ENTIDAD, U_NIT ,U_RESPONSABLE, U_TELEFONO,U_ESTADO,U_CODPROY, U_CARGO)"
        insertar += "values ('" & (indice) & "','Anticipo a proveedores de materiales e insumos','" + txtEntidad.Text + "','" + txtNit.Text + "','" + txtResponsable.Text + "','" + txtTelefono.Text + "','ACTIVO','" & codigoReporte & "', '" & txtCargo.Text & "') "
        ejecutarComandosSQL(insertar)
    End Sub

    Private Sub btnGenExl_Click(sender As System.Object, e As System.EventArgs) Handles btnGenExl.Click
        Dim cantFilas As Integer = dtgBodegas.Rows.Count - 1
        If checkResumen.Checked = True Then
            generarExcelResumen()

        Else
            For i As Integer = 0 To cantFilas - 1 Step 1

                generarExcel(dtgBodegas.Rows(i).Cells("BODEGA").Value.ToString())
            Next

        End If

    End Sub

    Private Sub btnGenRep_Click(sender As System.Object, e As System.EventArgs) Handles btnGenRep.Click
        If txtDescArticulo.Text = "ARTÍCULO NO VÁLIDO" Or txtDescBodega.Text = "ARTÍCULO NO VÁLIDO" Then
            MsgBox("Ingrese Artículos válidos.")
        Else
            llenarTablaReporte()
            btnGenExl.Enabled = True

        End If
    End Sub

    Private Sub txtArticulo_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtArticulo.TextChanged
        txtDescArticulo.Text = mostrarDescArticulo(txtArticulo.Text)
        llenarTablaReporte()
    End Sub

    Private Sub checkBodega_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles checkBodega.CheckedChanged
        If checkBodega.Checked = True Then
            txtBodega.Enabled = False
            pictureBodega.Enabled = False
        Else
            txtBodega.Enabled = True
            pictureBodega.Enabled = True
        End If
        txtBodega.Text = ""
        llenarTablaReporte()
    End Sub

    Private Sub checkArticulo_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles checkArticulo.CheckedChanged
        If checkArticulo.Checked = True Then
            txtArticulo.Enabled = False
            pictureArticulo.Enabled = False
        Else
            txtArticulo.Enabled = True
            pictureArticulo.Enabled = True
        End If
        txtArticulo.Text = ""
        llenarTablaReporte()

    End Sub

    Private Sub radioAdmisible_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radioAdmisible.CheckedChanged
        llenarTablaReporte()
    End Sub

    Private Sub radioNoAdmisible_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radioNoAdmisible.CheckedChanged
        llenarTablaReporte()
    End Sub

    Private Sub radioTodos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radioTodos.CheckedChanged
        llenarTablaReporte()
    End Sub

    Private Sub radioDocumento_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radioDocumento.CheckedChanged
        llenarTablaReporte()
    End Sub

    Private Sub radioAuditoria_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radioAuditoria.CheckedChanged
        llenarTablaReporte()
    End Sub


    Private Sub dateReporte_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dateReporte.ValueChanged
        llenarTablaReporte()
    End Sub



    Private Sub txtBodega_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtBodega.TextChanged
        txtDescBodega.Text = mostrarDescBodega(txtBodega.Text)

        llenarTablaReporte()
    End Sub

#Region "funciones F1"
    Private Sub txtCcDesde_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtArticulo.KeyUp
        ' DETERMINAR CUANDO LA TECLA F1 A SIDO PRESIONADA

        If e.KeyCode = Keys.F1 Then
            ' LLMAR A OTRO FORMULARIO ENVIANDO UN PARAMETRO INICIAL
            buscador.lblbOrigen.Text = "ARTD"
            buscador.Show()
            buscador.txtParametro.Text = txtArticulo.Text
        End If
    End Sub
    Private Sub txtCccHasta_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBodega.KeyUp
        ' DETERMINAR CUANDO LA TECLA F1 A SIDO PRESIONADA

        If e.KeyCode = Keys.F1 Then
            ' LLMAR A OTRO FORMULARIO ENVIANDO UN PARAMETRO INICIAL
            buscador.lblbOrigen.Text = "bodega"
            buscador.Show()
            buscador.txtParametro.Text = txtBodega.Text
        End If
    End Sub


#End Region

#Region "funciones doble click"
    Private Sub txtCcDesde_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtArticulo.DoubleClick

        buscador.lblbOrigen.Text = "ARTD"
        buscador.Show()
        buscador.txtParametro.Text = txtArticulo.Text
    End Sub
    Private Sub txtCccHasta_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBodega.DoubleClick

        buscador.lblbOrigen.Text = "bodega"
        buscador.Show()
        buscador.txtParametro.Text = txtBodega.Text


    End Sub


#End Region

    Private Sub checResumen_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles checkResumen.CheckedChanged
        If checkResumen.Checked = True Then
            checkArticulo.Checked = True
            checkArticulo.Enabled = False
        Else
            checkArticulo.Enabled = True
        End If
        llenarTablaReporte()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Application.Exit()
    End Sub


    Private Sub pictureBodega_Click(sender As System.Object, e As System.EventArgs) Handles pictureBodega.Click
        buscador.lblbOrigen.Text = "bodega"
        buscador.Show()
        buscador.txtParametro.Text = txtBodega.Text
    End Sub

    Private Sub pictureArticulo_Click(sender As System.Object, e As System.EventArgs) Handles pictureArticulo.Click
        buscador.lblbOrigen.Text = "ARTD"
        buscador.Show()
        buscador.txtParametro.Text = txtArticulo.Text
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        'GridAExcel(dtgReporte)
        ExportarExcel()
    End Sub

    Private Sub ExportarExcel()
        Try

            'Variables del programa
            Dim rutaPlantilla As String = "D:\Clever\VITALICIA ERP\FS\Desarrollos\NET\APS - 4.93 B A-NA BODEGAS\Código Fuente\Nuevo\4.93B_ANA_BOD\4.93B_ANA_BOD\bin\Debug\PLANTILLAS\4.93B.xlsx"
            Dim rutaGuardado As String = "D:\Clever\VITALICIA ERP\FS\Desarrollos\NET\APS - 4.93 B A-NA BODEGAS\Código Fuente\Nuevo\4.93B_ANA_BOD\4.93B_ANA_BOD\bin\Debug\PLANTILLAS\modificado.xls"
            Dim xlApp As Excel.Application = New Excel.Application()
            Dim _libroExcel As Excel.Workbook = Nothing
            Dim _HojaExcel As Excel.Worksheet = Nothing
            Dim _Rango As Excel.Range = Nothing
            Dim misValue As Object = System.Reflection.Missing.Value

            _libroExcel = xlApp.Workbooks.Open(rutaPlantilla, misValue, misValue, misValue _
                    , misValue, misValue, misValue, misValue _
                   , misValue, misValue, misValue, misValue _
                  , misValue, misValue, misValue)


            _libroExcel = xlApp.ActiveWorkbook

            _HojaExcel = CType(_libroExcel.Worksheets.Item(1), Excel.Worksheet)
            'Creamos una copia de la hoja 1 delante de ella
            _libroExcel.Worksheets(1).Copy(After:=_libroExcel.Worksheets(_libroExcel.Sheets.Count))
            xlApp.ScreenUpdating = False
            xlApp.DisplayAlerts = False

            Try
                xlApp.ScreenUpdating = False
                'Parte del programa que inserta datos en la hoja activa en las celdas marcadas
                'Columna de DATUMS
                _Rango = CType(_HojaExcel.Cells.Range("Z5"), Excel.Range)
                _Rango.Value2 = "Prueba escritura DATUM"

                'Columna de Types
                _Rango = CType(_HojaExcel.Cells.Range("B5"), Excel.Range)
                _Rango.Value2 = "D"
                'Columna de Nominal
                _Rango = CType(_HojaExcel.Cells.Range("C5"), Excel.Range)
                _Rango.Value2 = "0.14"
                'Columna de tolerancia Superior
                _Rango = CType(_HojaExcel.Cells.Range("D5"), Excel.Range)
                _Rango.Value2 = "0.01"
                'Columna de tolerancia inferior
                _Rango = CType(_HojaExcel.Cells.Range("E5"), Excel.Range)
                _Rango.Value2 = "-0.01"
                'A partir de la columbna j para las medidas
                _Rango = CType(_HojaExcel.Cells.Range("j5"), Excel.Range)
                _Rango.Value2 = "0.12"

                _HojaExcel.Columns.EntireColumn.AutoFit()
                xlApp.ScreenUpdating = True
                'Guardamos el libro 
                _libroExcel.SaveAs(rutaGuardado, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue)
                _libroExcel.Close(True, misValue, misValue)
                xlApp.Quit()
                MessageBox.Show("Datos Exportados" & rutaPlantilla)

            Catch ex As System.Exception

                MessageBox.Show(ex.Message & "\n\n=======  Error al escribir el excel:  ======\n\n" & _
                   ex.StackTrace)

            Finally

                releaseObject(_Rango)
                releaseObject(_HojaExcel)
                releaseObject(_libroExcel)
                releaseObject(xlApp)
                'Matamos el proceso excel por si se ha quedado abierto
                Dim p As Process
                For Each p In Process.GetProcesses
                    If (Not IsNothing(p) AndAlso (p.ProcessName = "EXCEL")) Then
                        p.Kill()
                    End If
                Next


            End Try
        Catch exl As System.Exception

            MessageBox.Show(exl.Message & _
             "\n\n=======   Error al abrir el archivo  ======\n\n" & _
             exl.StackTrace)

        End Try
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try

            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing

        Catch ex As System.Exception
            obj = Nothing
            MessageBox.Show("Error al limpiar memoria \n" + ex.ToString())
        Finally
            If (Not obj Is Nothing) Then
                Dim pos As Integer = GC.GetGeneration(obj)
                GC.Collect(pos)

            Else
                GC.Collect()
                GC.WaitForPendingFinalizers()
            End If
        End Try

    End Sub


    Public Function GridAExcel(ByVal DGV As DataGridView) As Boolean

        'Creamos las variables

        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet

        Try

            exLibro = exApp.Workbooks.Add
            exHoja = exLibro.Worksheets.Add()

            ' ¿Cuantas columnas y cuantas filas?
            Dim NCol As Integer = DGV.ColumnCount
            Dim NRow As Integer = DGV.RowCount
            'recorremos todas las filas, y por cada fila todas las columnas
            'y vamos escribiendo.
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = DGV.Columns(i - 1).Name.ToString
            Next

            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) =
                    DGV.Rows(Fila).Cells(Col).Value()
                Next

            Next

            'Titulo en negrita, Alineado
            exHoja.Rows.Item(1).Font.Bold = 1
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            exHoja.Columns.AutoFit()
            'para visualizar el libro
            exApp.Application.Visible = True
            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")

            Return False
        End Try
        Return True
    End Function

    Private Sub btnAnular_Click(sender As System.Object, e As System.EventArgs) Handles btnAnular.Click
        Dim anio As String = separarCadena(lblCierre.Text, True, "-")
        Dim mes As String = separarCadena(lblCierre.Text, False, "-")
        Dim res As Integer
        res = MsgBox("¿Seguro que desea anular el cierre del periodo " & mes & "-" & anio & " ?", MsgBoxStyle.YesNo)
        If res = 6 Then
            pantallaPass.lblAnio.Text = anio
            pantallaPass.lblMes.Text = mes
            pantallaPass.Show()
            Me.Enabled = False

        End If



    End Sub
End Class
