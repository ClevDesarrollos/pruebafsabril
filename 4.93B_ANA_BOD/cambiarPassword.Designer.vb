﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cambiarPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cambiarPassword))
        Me.btnAtras = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.txtPassAnt = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtPassNuevo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPassConfirmar = New System.Windows.Forms.TextBox()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAtras
        '
        Me.btnAtras.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAtras.Location = New System.Drawing.Point(323, 247)
        Me.btnAtras.Name = "btnAtras"
        Me.btnAtras.Size = New System.Drawing.Size(179, 52)
        Me.btnAtras.TabIndex = 166
        Me.btnAtras.Text = "Cancelar"
        Me.btnAtras.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrar.Location = New System.Drawing.Point(138, 247)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(179, 52)
        Me.btnCerrar.TabIndex = 165
        Me.btnCerrar.Text = "Cambiar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'lblMensaje
        '
        Me.lblMensaje.AutoSize = True
        Me.lblMensaje.BackColor = System.Drawing.Color.Transparent
        Me.lblMensaje.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.ForeColor = System.Drawing.Color.Red
        Me.lblMensaje.Location = New System.Drawing.Point(232, 210)
        Me.lblMensaje.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(204, 22)
        Me.lblMensaje.TabIndex = 164
        Me.lblMensaje.Text = "CONFIRMAR CONTRASEÑA:"
        Me.lblMensaje.Visible = False
        '
        'txtPassAnt
        '
        Me.txtPassAnt.BackColor = System.Drawing.Color.White
        Me.txtPassAnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassAnt.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassAnt.ForeColor = System.Drawing.Color.Black
        Me.txtPassAnt.Location = New System.Drawing.Point(281, 80)
        Me.txtPassAnt.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPassAnt.Name = "txtPassAnt"
        Me.txtPassAnt.Size = New System.Drawing.Size(338, 30)
        Me.txtPassAnt.TabIndex = 157
        Me.txtPassAnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtPassAnt.UseSystemPasswordChar = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(46, 84)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(190, 22)
        Me.Label4.TabIndex = 163
        Me.Label4.Text = "CONTRASEÑA ANTERIOR:"
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'txtPassNuevo
        '
        Me.txtPassNuevo.BackColor = System.Drawing.Color.White
        Me.txtPassNuevo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassNuevo.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassNuevo.ForeColor = System.Drawing.Color.Black
        Me.txtPassNuevo.Location = New System.Drawing.Point(281, 124)
        Me.txtPassNuevo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPassNuevo.Name = "txtPassNuevo"
        Me.txtPassNuevo.Size = New System.Drawing.Size(338, 30)
        Me.txtPassNuevo.TabIndex = 158
        Me.txtPassNuevo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtPassNuevo.UseSystemPasswordChar = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(32, 171)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(204, 22)
        Me.Label6.TabIndex = 162
        Me.Label6.Text = "CONFIRMAR CONTRASEÑA:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(71, 128)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(165, 22)
        Me.Label5.TabIndex = 161
        Me.Label5.Text = "NUEVA CONTRASEÑA:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(162, 9)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(325, 33)
        Me.Label3.TabIndex = 160
        Me.Label3.Text = "CAMBIO DE CONTRASEÑA:"
        '
        'txtPassConfirmar
        '
        Me.txtPassConfirmar.BackColor = System.Drawing.Color.White
        Me.txtPassConfirmar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassConfirmar.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassConfirmar.ForeColor = System.Drawing.Color.Black
        Me.txtPassConfirmar.Location = New System.Drawing.Point(281, 167)
        Me.txtPassConfirmar.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPassConfirmar.Name = "txtPassConfirmar"
        Me.txtPassConfirmar.Size = New System.Drawing.Size(338, 30)
        Me.txtPassConfirmar.TabIndex = 159
        Me.txtPassConfirmar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtPassConfirmar.UseSystemPasswordChar = True
        '
        'cambiarPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 324)
        Me.Controls.Add(Me.btnAtras)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.txtPassAnt)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtPassNuevo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtPassConfirmar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "cambiarPassword"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambiar Password"
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAtras As System.Windows.Forms.Button
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents lblMensaje As System.Windows.Forms.Label
    Friend WithEvents txtPassAnt As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ErrorProvider As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtPassNuevo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPassConfirmar As System.Windows.Forms.TextBox
End Class
