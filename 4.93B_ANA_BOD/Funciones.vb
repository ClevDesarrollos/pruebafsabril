﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Module Funciones

#Region "VARIABLES"
    Public conexion As SqlConnection
    Public CN As String = Desencriptar(ObtenerCadenaConexion())
    Dim Xml As XmlDocument
    Dim NodeList, NodeList1, NodeListR, NodeListBoleta As XmlNodeList
    Public enunciado As SqlCommand
    Public respuesta As SqlDataReader
    Public Con As String
    Public esq As String = ObtenerEsquema()
#End Region

#Region "CIFRADO"
    'Esta función encripta una cadena de caracteres.
    Public Function Encriptar(ByVal Input As String) As String

        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("softland") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("CleverLimitadaLaPazBolivia2015V1") 'No se puede alterar la cantidad de caracteres pero si la clave (32 caracteres)
        Dim buffer() As Byte = Encoding.UTF8.GetBytes(Input)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV
        Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

    End Function
    'Esta función desencripta una cadena encriptada, en caso de no encontrar una cadena válida, se retorna la cadena ingresada
    Public Function Desencriptar(ByVal Input As String) As String
        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("softland") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("CleverLimitadaLaPazBolivia2015V1") 'No se puede alterar la cantidad de caracteres pero si la clave
        Try
            Dim buffer() As Byte = Convert.FromBase64String(Input)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            des.Key = EncryptionKey
            des.IV = IV
            Return Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
        Catch ex As Exception
            Return Input
        End Try
    End Function

    Public Function Desencriptar2(ByVal Input As String) As String
        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("softland") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("CleverLimitadaLaPazBolivia2015V1") 'No se puede alterar la cantidad de caracteres pero si la clave
        Try
            Dim buffer() As Byte = Convert.FromBase64String(Input)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            des.Key = EncryptionKey
            des.IV = IV
            Return Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
        Catch ex As Exception
            Return "Error404"
        End Try
    End Function

#End Region

#Region "CONEXION A BASE DE DATOS"

    Sub conectar()
        Try
            conexion = New SqlConnection(CN)
            conexion.Open()
        Catch ex As Exception
            MsgBox("No se conecto debido a: " + ex.ToString)
        End Try
    End Sub
    Public Sub Desconectar()
        conexion.Close()
        conexion = Nothing
    End Sub
#End Region

#Region "FUNCIONES"
    'Funcion que llena el contenido de un combobox a partir de el objeto ComboBox, un query y una columna
    Sub llenarComboBox(ByVal cb As ComboBox, ByVal query As String, ByVal columna As String)
        conectar()
        Try
            enunciado = New SqlCommand(query, conexion)
            respuesta = enunciado.ExecuteReader
            While respuesta.Read
                cb.Items.Add(respuesta.Item(columna))
            End While
            respuesta.Close()
        Catch ex As Exception
            MsgBox("No se pudo llenar la lista desplegable " + cb.Name.ToString)
        End Try
        Desconectar()
    End Sub
    'Funcion que llena el contenido de un combobox a partir de el objeto ComboBox, un query y dos columnas
    Sub llenarComboBoxOpciones(ByVal cb As ComboBox, ByVal query As String, ByVal columna As String, ByVal columna2 As String)
        conectar()
        cb.Items.Clear()
        Try
            enunciado = New SqlCommand(query, conexion)

            respuesta = enunciado.ExecuteReader

            While respuesta.Read
                cb.Items.Add(respuesta.Item(columna) & " --- " & respuesta.Item(columna2))
            End While
            respuesta.Close()
        Catch ex As Exception
        End Try
        Desconectar()
    End Sub

    Public Function separarCadena(ByRef cad As String, ByRef posicion As Boolean, ByRef separador As String) As String '
        Dim b As String
        Dim pos As Integer
        Dim largo As Integer = cad.Length

        pos = InStr(cad, separador)
        If pos <> 0 Then
            If posicion = False Then
                b = cad.Substring(0, pos - 1)
            Else
                b = cad.Substring(pos, largo - pos)
                b = b.Substring(separador.Length - 1, b.Length - separador.Length + 1)
            End If
        Else
            b = "Otro..."
        End If
        Return b
    End Function

    'Función que llena el contenido de un objeto dataGridView con los datos de un query.
    Sub llenarTabla(ByVal SQL As String, ByVal dtgview As DataGridView)
        conexion = New SqlConnection
        conexion.ConnectionString = CN
        Dim micomando As New SqlCommand(SQL, conexion)
        Dim midataAdapter As New SqlDataAdapter
        conexion.Open()
        micomando.CommandTimeout = 9000
        midataAdapter.SelectCommand = micomando
        Dim midataset As New DataSet
        midataset.Clear()
        midataAdapter.Fill(midataset, "vista")
        dtgview.DataSource = midataset.Tables(0)
        conexion.Close()

    End Sub

    'Retorna el primer registro de una consulta, utilizando un query como parametro
    Public Function DevolverDatoQuery(ByVal columna As String, ByVal tabla As String, ByVal condicion As String) As String
        Dim query As String
        Try
            conectar()
            query = "SELECT " & columna & " FROM " & tabla & " " & condicion
            'MsgBox(query)
            enunciado = New SqlCommand(query, conexion)
            Dim Devolver As String = ""
            Devolver = enunciado.ExecuteScalar()
            enunciado = Nothing
            Desconectar()
            Return Devolver
        Catch ex As Exception
            MsgBox("No se pudieron obtener los siguientes datos: " & columna)
            Return ""
        End Try
    End Function

    'Funcion que inserta datos .
    Sub insertarDatos(ByVal tabla As String, ByVal columnas As String, ByVal valores As String)
        Dim query As String
        Try
            conectar()
            query = "INSERT INTO " & tabla & " (" & columnas & ") VALUES (" & valores & ") "
            enunciado = New SqlCommand(query, conexion)
            enunciado.CommandTimeout = 6000
            Dim t As Integer = enunciado.ExecuteNonQuery()
            Desconectar()
        Catch ex As Exception
            MessageBox.Show("No se pudieron insertar los datos " & valores & " en las columnas " & columnas)
        End Try

    End Sub


    'Funcion que ejecuta un comando sql, como: Insert,Update,Delete, Etc.
    Sub ejecutarComandosSQL(ByVal query As String)
        Try
            conectar()
            enunciado = New SqlCommand(query, conexion)
            enunciado.CommandTimeout = 6000
            Dim t As Integer = enunciado.ExecuteNonQuery()
            Desconectar()
        Catch ex As Exception
            MessageBox.Show(ex.Message + " *** No se pudo ejecutar la instrucción. *** " + query)
        End Try

    End Sub

    Public Function DevolverDatoQuery2(ByVal query As String) As String
        Try
            conectar()
            enunciado = New SqlCommand(query, conexion)
            Dim Devolver As String = ""
            Devolver = enunciado.ExecuteScalar()
            enunciado = Nothing
            Desconectar()
            Return Devolver
        Catch ex As Exception
            MsgBox("No se pudo resolver la consulta." + query)
            Return ""
        End Try
    End Function


#End Region

#Region "CONEXION XML"
    Public Sub AbrirXml()
        Xml = New XmlDocument()
        Dim Archivo As String
        Archivo = "Config_4.93B_ANA_BOD"
        Try
            Xml.Load(My.Application.Info.DirectoryPath & "\" & Archivo & ".xml")
        Catch ex As Exception
            MsgBox("No se pudo cargar el archivo de configuración " + My.Application.Info.DirectoryPath & "\" & Archivo & ".xml. Compruebe que el archivo no haya sido removido de la raíz del aplicativo.")
        End Try
    End Sub

    Public Sub CerrarXml()
        Xml = Nothing
    End Sub

    Public Function ObtenerNroConexion() As String
        Dim Nro As String = "100"
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/Conexiones/ConexionActiva")
            Nro = NodeList(0).Attributes.GetNamedItem("NumeroConexion").Value
        Catch ex As Exception
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return Nro
    End Function

    Public Function ObtenerCadenaConexion() As String
        Dim Cadena As String = ""
        Dim Nro As String = ObtenerNroConexion()

        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/Conexiones/Conexion" + Nro)
            Cadena = NodeList(0).Attributes.GetNamedItem("Cadena").Value
        Catch ex As Exception
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return Cadena
    End Function

    Public Sub obtenerDatosReportes(ByRef plantilla As String, ByRef extension As String, ByRef filadatos As String, ByRef columnadatos As String)
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/DatosReporte")
            plantilla = NodeList(0).Attributes.GetNamedItem("Plantilla").Value
            extension = NodeList(0).Attributes.GetNamedItem("Extension").Value
            filadatos = NodeList(0).Attributes.GetNamedItem("FilaDatos").Value
            columnadatos = NodeList(0).Attributes.GetNamedItem("ColumnaDatos").Value
        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
    End Sub

    Public Function obtenerRutaReportes() As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/RutaReportes")
            esquema = NodeList(0).Attributes.GetNamedItem("Ruta").Value
        Catch ex As Exception
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function

    Public Function obtenerEncabezadoExcel(ByRef Esquema As String) As String
        Dim encabezado As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/EncabezadoExcel ")
            Esquema = NodeList(0).Attributes.GetNamedItem("Nombre").Value
        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return encabezado
    End Function

    Public Function ObtenerEsquema() As String
        Dim esquema As String = ""
        AbrirXml()
        Try
            NodeList = Xml.SelectNodes("/Documento/EsquemaBaseDeDatos")
            esquema = NodeList(0).Attributes.GetNamedItem("Esquema").Value
        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return esquema
    End Function

    Public Sub LlenarMatrizSubtitulos(ByRef S(,) As String, ByRef TotalSubtitulos As Int16)
        Dim nodos As Int16
        Dim Concepto As String = ""
        Dim Descripcion As String = ""
        Dim texto As String

        AbrirXml()

        NodeList = Xml.SelectNodes("/Documento/SubTitulos")
        nodos = NodeList(0).ChildNodes.Count() 'muestra la cantidad de reportes

        texto = ""

        TotalSubtitulos = nodos
        Try
            For i As Int16 = 1 To nodos
                NodeList1 = Xml.SelectNodes("/Documento/SubTitulos/SubTitulo" & i)

                S(i, 0) = NodeList1(0).Attributes(0).Value
                S(i, 1) = NodeList1(0).Attributes(1).Value
                S(i, 2) = NodeList1(0).Attributes(2).Value

            Next
        Catch ex As Exception
            'Console.WriteLine(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()

    End Sub

    Public Function obtenerCodigoXML() 'obtener codigo del reporte actual
        Dim nodos As Int16
        Dim texto As String
        AbrirXml()
        NodeList = Xml.SelectNodes("/Documento/Formulario")
        nodos = NodeList(0).ChildNodes.Count()
        texto = ""
        Try
            For i As Int16 = 1 To nodos
                NodeList1 = Xml.SelectNodes("/Documento/Formulario/FormRep")
                texto = NodeList1(0).Attributes(0).Value
            Next
        Catch ex As Exception
            MsgBox(ex.GetType.ToString & vbNewLine & ex.Message.ToString)
        End Try
        CerrarXml()
        Return texto
    End Function

#End Region

End Module
