﻿Imports System.Math


Public Class Ajustes
    Dim query As String
    Dim esquema As String = ObtenerEsquema()
    ' variables para devolución de datos
    Dim tabla As String
    Dim columna As String
    Dim condicion As String

    ' variables para la inserción de datos
    Dim valores As String

    Function obtenerCodigoFechaAnterior(ByVal mes As Integer, ByVal anio As Integer, ByVal fechaDe As String) As Integer
        Dim codFec As Integer

        columna = "DISTINCT TOP 1 CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))"

        If fechaDe = "D" Then
            tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
        Else
            tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
        End If


        condicion = " WHERE CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))<CONVERT(INT,CONVERT(VARCHAR(4)," & anio & ")+RIGHT('0'+CONVERT(VARCHAR(2)," & mes & "),2)) "
        condicion += " ORDER BY CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2)) DESC"

        codFec = CInt(DevolverDatoQuery(columna, tabla, condicion))

        Return codFec

    End Function

    Function obtenerCostoFinal(ByVal codfec As Integer, ByVal bod As String, ByVal art As String, ByVal fechaDe As String) As Double
        Dim costoFin As Double = 0
        If codfec <> 0 Then
            columna = "COSTO_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND  CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codfec

            costoFin = CDbl(DevolverDatoQuery(columna, tabla, condicion))
        End If
        Return costoFin

    End Function

    Function calcularCostoEntradas(ByVal bod As String, ByVal art As String, ByVal codFecAnt As Integer, ByVal codFec As Integer, ByVal cantEntradas As Double, ByVal entradasCosto As Double, ByVal costoProm As Double, ByVal fechaDe As String) As Double


        Dim costoEntradas As Double = 0
        Dim ajuste As Integer


        columna = " ISNULL(SUM(COSTO_TOT_FISC_LOC),0)"

        tabla = esquema & ".TRANSACCION_INV"

        condicion = " WHERE NATURALEZA='C'"
        condicion += " AND ARTICULO='" & art & "'"
        If fechaDe = "D" Then
            condicion += " AND  CONVERT(INT,CONVERT(VARCHAR(4),YEAR(FECHA))+RIGHT('0'+CONVERT(VARCHAR(2),MONTH(FECHA)),2))=" & codFec
        Else
            condicion += " AND  CONVERT(INT,CONVERT(VARCHAR(4),YEAR(FECHA_HORA_TRANSAC))+RIGHT('0'+CONVERT(VARCHAR(2),MONTH(FECHA_HORA_TRANSAC)),2))=" & codFec

        End If


        ajuste = CInt(DevolverDatoQuery(columna, tabla, condicion))

        If ajuste <> 0 Then
            If bod.ToUpper.Contains("DC") Then
                'columna = "CANTIDAD_FINAL"

                'If fechaDe = "D" Then
                '    tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
                'Else
                '    tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
                'End If

                'condicion = " WHERE ARTICULO='" & art & "'"
                'condicion += " AND BODEGA='" & bod & "'"
                'condicion += " AND REVERSE(ANIO)+MES=" & codFecAnt

                'cantidadFinalAnt = CDbl(DevolverDatoQuery(columna, tabla, condicion))



                'columna = "COSTO_FINAL"
                'costoFinalAnt = CDbl(DevolverDatoQuery(columna, tabla, condicion))
                ajuste = ajuste / divisorAjuste(bod, art, codFec)
                costoEntradas = entradasCosto + ajuste '((cantEntradas + cantidadFinalAnt) * costoProm) - costoFinalAnt
                'MsgBox(costoEntradas.ToString)
                'If codFec = 6104 Then

                '    MsgBox("bodega " & bod)
                '    MsgBox("arituculo " & art)
                '    MsgBox("codfec " & codFec)
                '    MsgBox("codfecatn " & codFecAnt)
                '    MsgBox("cantEntrada " & cantEntradas)
                '    MsgBox("costoentradas " & costoEntradas.ToString)
                '    MsgBox("cantEntrada " & cantEntradas)
                '    MsgBox("costoentradas " & costoEntradas.ToString)
                '    MsgBox("cantidadfinalAnterior" & cantidadFinalAnt)
                '    MsgBox("costofinalAnterior" & cantidadFinalAnt)
                '    MsgBox("costo prom " & costoProm)

                'End If
            Else
                costoEntradas = entradasCosto
            End If
        Else
            costoEntradas = entradasCosto
        End If
        Return costoEntradas
    End Function

    Function divisorAjuste(ByVal bod As String, ByVal art As String, ByVal codFec As Integer) As Integer
        Dim divisor As Integer

        query = " SELECT count('a') FROM " & esquema & ".U_CLEV_ANA_TOT_DOC"
        query += " WHERE ARTICULO='" & art & "'"
        query += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec
        query += " AND CANTIDAD_ANTERIOR+ENTRADAS-SALIDAS<>0"

        divisor = DevolverDatoQuery2(query)
        If divisor = 0 Then
            divisor = 1
        End If

        Return divisor
    End Function

    Function calcularCostoProm(ByVal bod As String, ByVal art As String, ByVal codFec As Integer, ByVal cantEntradas As Double, ByVal costoEntradas As Double, ByVal fechaDe As String, ByVal costoPromOriginal As Double, ByVal codFecAux As Integer) As Double

        Dim costoProm As Double = 0
        Dim costoFinal As Double
        Dim cantidadFinal As Double

        costoFinal = obtenerCostoFinal(codFec, bod, art, fechaDe)

        columna = "CANTIDAD_FINAL"

        If fechaDe = "D" Then
            tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
        Else
            tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
        End If

        condicion = " WHERE ARTICULO='" & art & "'"
        condicion += " AND BODEGA='" & bod & "'"
        condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec


        cantidadFinal = CDbl(DevolverDatoQuery(columna, tabla, condicion))

        query = " SELECT SUM ((A.COSTO_TOT_FISC_LOC))"
        query += " FROM"
        query += " VITALICIA.TRANSACCION_INV A, VITALICIA.AUX_TRANS_INV B"
        query += " WHERE "
        query += " A.ARTICULO='" & art & "'"

        query += " AND NATURALEZA = 'C'"
        query += " AND A.AUDIT_TRANS_INV=B.AUDIT_TRANS_INV"
        query += " AND A.CONSECUTIVO=B.CONSECUTIVO"
        query += " AND B.REPORTE='SI'"
        query += " AND CONVERT(INT,CONVERT(VARCHAR(4),YEAR(FECHA))+RIGHT('0'+CONVERT(VARCHAR(2),MONTH(FECHA)),2))=" & codFecAux
        query += " GROUP BY ARTICULO"

        'ajuste = CDbl(DevolverDatoQuery2(query))
        If (cantidadFinal + cantEntradas) = 0 Then
            If (cantidadFinal) = 0 Then
                If (cantEntradas) = 0 Then
                    costoProm = 0

                Else
                    costoProm = Abs((costoEntradas) / (cantEntradas))

                End If

            Else
                costoProm = (costoFinal) / (cantidadFinal)

            End If

        Else
            costoProm = (costoFinal + costoEntradas) / (cantidadFinal + cantEntradas)

        End If
        If costoProm = 0 Then
            costoProm = costoPromOriginal
        End If
        'Try
        '    costoProm = (costoFinal + costoEntradas) / (cantidadFinal + cantEntradas)
        'Catch ex As DivideByZeroException
        '    Try
        '        costoProm = costoFinal / cantidadFinal
        '    Catch em As DivideByZeroException
        '        Try
        '            costoProm = costoEntradas / cantEntradas
        '        Catch en As DivideByZeroException
        '            costoProm = 0
        '        End Try
        '    End Try
        'End Try


        Return costoProm

    End Function


#Region "ADMISIBLE"
    Function obtenerCantidadAnteriorAdm(ByVal bod As String, ByVal art As String, ByVal codFec As Integer, ByVal fechaDe As String) As Double
        Dim a As Double = 0
        

        Dim presup As Double
        Dim consAnt As Double
        Dim cons As Double
        Dim cantFin As Double

        Dim consum As Double
        Dim porConsum As Double
        If codFec <> 0 Then
            columna = "PRESUPUESTADO"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_TOT_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_TOT_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec

            presup = CDbl(DevolverDatoQuery(columna, tabla, condicion))




            columna = "CONSUMIDO"
            cons = CDbl(DevolverDatoQuery(columna, tabla, condicion))
            'MsgBox(query)
            'MsgBox(cons.ToString)

            columna = "CONSUMIDO_ANT"
            consAnt = CDbl(DevolverDatoQuery(columna, tabla, condicion))
            'MsgBox(query)
            'MsgBox(consAnt.ToString)

            columna = "CANTIDAD_FINAL"
            cantFin = CDbl(DevolverDatoQuery(columna, tabla, condicion))
            'MsgBox(query)
            'MsgBox(cantFin.ToString)

            consum = consumidoObras(presup, cons, consAnt)
            porConsum = totalPorConsumir(presup, cons, consAnt)

            If cantFin > porConsum Then
                a = porConsum
            Else
                a = cantFin
            End If
        End If


        Return a

    End Function

    Function consumidoObras(ByVal presup As Double, ByVal consumido As Double, ByVal consumidoAnt As Double) As Double
        Dim a As Double

        a = presup - consumidoAnt - consumido
        If a < 0 Then
            a = presup - consumidoAnt
            If a < 0 Then
                a = 0
            End If
        End If
        Return a

    End Function

    Function totalPorConsumir(ByVal presup As Double, ByVal consumido As Double, ByVal consumidoAnt As Double) As Double
        Dim a As Double

        a = presup - consumidoAnt - consumido
        If a < 0 Then
            a = 0
        End If
        Return a

    End Function

    ' si enviamos un costoPromAux diferente de O la funci[on procesar[a el costo promedio de las cantidades admisibles, diferente a la especificaci[on original.
    Function calcularCostoPromAdm(ByVal bod As String, ByVal art As String, ByVal codFec As Integer, ByVal cantEntradas As Double, ByVal costoEntradas As Double, ByVal fechaDe As String, ByVal costoPromAux As Double) As Double


        Dim costoProm As Double = 0
        Dim cantidadFinal As Double
        Dim costoFinal As Double

        If codFec <> 0 Then

            columna = "CANTIDAD_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_ADM_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_ADM_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec

            cantidadFinal = CDbl(DevolverDatoQuery(columna, tabla, condicion))



            columna = "COSTO_FINAL"
            costoFinal = CDbl(DevolverDatoQuery(columna, tabla, condicion))



            If (cantidadFinal + cantEntradas) = 0 Then
                If (cantidadFinal) = 0 Then
                    If (cantEntradas) = 0 Then
                        costoProm = 0

                    Else
                        costoProm = Abs((costoEntradas) / (cantEntradas))

                    End If

                Else
                    costoProm = (costoFinal) / (cantidadFinal)

                End If

            Else
                costoProm = (costoFinal + costoEntradas) / (cantidadFinal + cantEntradas)

            End If
            'Try
            '    costoProm = (costoFinal + costoEntradas) / (cantidadFinal + cantEntradas)
            'Catch ex As DivideByZeroException
            '    Try
            '        costoProm = costoFinal / cantidadFinal
            '    Catch em As DivideByZeroException
            '        Try
            '            costoProm = costoEntradas / cantEntradas
            '        Catch en As DivideByZeroException
            '            costoProm = 0
            '        End Try
            '    End Try
            'End Try

            If costoPromAux <> 0 Then
                If (cantidadFinal + cantEntradas) = 0 Then
                    costoProm = 0

                Else
                    costoProm = (costoFinal + (Abs(cantEntradas) * costoProm)) / (cantidadFinal + Abs(cantEntradas))

                End If
            End If
        End If



        Return costoProm

    End Function



    Function obtenerCostoAnteriorAdm(ByVal bod As String, ByVal art As String, ByVal codFec As Integer, ByVal fechaDe As String) As Double
        Dim a As Double = 0

        'Dim cantAnterior As Double
        Dim costoProm As Double
        If codFec <> 0 Then

            columna = "COSTO_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_ADM_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_ADM_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"

            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec

            costoProm = CDbl(DevolverDatoQuery(columna, tabla, condicion))

            'cantAnterior = obtenerCantidadAnteriorAdm(bod, art, codFec, fechaDe)
            a = costoProm
        End If

        Return a
    End Function

#End Region


    Sub actualizarValoresArticulo(ByVal art As String, ByVal fechaDe As String)
        If fechaDe = "D" Then
            query = " UPDATE " & esquema & ".U_CLEV_ANA_TOT_DOC"
        Else
            query = " UPDATE " & esquema & ".U_CLEV_ANA_TOT_AUD"
        End If

        query += " SET ENTRADAS=(" & esquema & ".U_CLEV_ANA_FN_CANT_ENT (BODEGA,ARTICULO,MES,ANIO,'" & fechaDe & "')),"
        query += " SALIDAS=(" & esquema & ".U_CLEV_ANA_FN_CANT_SAL (BODEGA,ARTICULO,MES,ANIO,'" & fechaDe & "')),"
        query += " CANTIDAD_FINAL=0,"
        query += " CANTIDAD_ANTERIOR=(" & esquema & ".U_CLEV_ANA_FN_CANT_ANT(BODEGA,ARTICULO,MES,ANIO,'" & fechaDe & "')),"
        query += " COSTO_PROM=(" & esquema & ".U_CLEV_ANA_FN_COST_PROM(ARTICULO,MES,ANIO,'" & fechaDe & "')),"
        query += " COSTO_ANTERIOR=0,"
        query += " COSTO_FINAL=0,"
        query += " COSTO_ENTRADAS=(" & esquema & ".U_CLEV_ANA_FN_COST_ENT(BODEGA,ARTICULO,MES,ANIO,'" & fechaDe & "')),"
        query += " COSTO_SALIDAS=(" & esquema & ".U_CLEV_ANA_FN_COST_SAL(BODEGA,ARTICULO,MES,ANIO,'" & fechaDe & "'))"

        'query += " ,PRESUPUESTADO=(" & esquema & ".A_NA_BOD_PRESUPUESTADO(ARTICULO))"
        query += " WHERE ARTICULO='" & art & "'"
        'MsgBox(query)
        ejecutarComandosSQL(query)
        If fechaDe = "D" Then
            query = " UPDATE " & esquema & ".U_CLEV_ANA_ADM_DOC"
        Else
            query = " UPDATE " & esquema & ".U_CLEV_ANA_ADM_AUD"
        End If
        query += " SET ENTRADAS=0,"
        query += " SALIDAS=0,"
        query += " CANTIDAD_FINAL=0,"
        query += " CANTIDAD_ANTERIOR=0,"
        query += " COSTO_PROM=(" & esquema & ".U_CLEV_ANA_FN_COST_PROM(ARTICULO,MES,ANIO,'" & fechaDe & "')),"
        query += " COSTO_ANTERIOR=0,"
        query += " COSTO_FINAL=0,"
        query += " COSTO_ENTRADAS=0,"
        query += " COSTO_SALIDAS=0"

        'query += " ,PRESUPUESTADO=(" & esquema & ".A_NA_BOD_PRESUPUESTADO(ARTICULO))"
        query += " WHERE ARTICULO='" & art & "'"
        'MsgBox(query)
        ejecutarComandosSQL(query)
    End Sub

    Sub cargarValoresArticulo(ByVal art As String, ByVal fechaDe As String)

        query = " SELECT "
        query += " BODEGA, "
        query += " ARTICULO, "
        query += " ANIO,"
        query += " MES, "
        query += " CANTIDAD_ANTERIOR,"
        query += " ENTRADAS,"
        query += " SALIDAS,"
        query += " CANTIDAD_FINAL,"
        query += " COSTO_PROM,"
        query += " COSTO_ANTERIOR,"
        query += " COSTO_ENTRADAS,"
        query += " COSTO_SALIDAS,"
        query += " COSTO_FINAL,"
        query += " PRESUPUESTADO,"
        query += " CONSUMIDO,"
        query += " CONSUMIDO_ANT"
        If fechaDe = "D" Then
            query += " FROM VITALICIA.U_CLEV_ANA_TOT_DOC"
        Else
            query += " FROM VITALICIA.U_CLEV_ANA_TOT_AUD"
        End If

        query += " WHERE ARTICULO='" & art & "'"
        query += " ORDER BY ANIO,MES"

        'MsgBox(query)
        llenarTabla(query, dtgVistas)
    End Sub

    Function obtenerCantidadFinalAdm(ByVal codfec As Integer, ByVal bod As String, ByVal art As String, ByVal fechaDe As String) As Double
        Dim cantFinal As Double = 0
        If codfec <> 0 Then
            columna = "CANTIDAD_FINAL"

            If fechaDe = "D" Then
                tabla = esquema & ".U_CLEV_ANA_ADM_DOC"
            Else
                tabla = esquema & ".U_CLEV_ANA_ADM_AUD"
            End If

            condicion = " WHERE ARTICULO='" & art & "'"
            condicion += " AND BODEGA='" & bod & "'"
            condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codfec


            cantFinal = CDbl(DevolverDatoQuery(columna, tabla, condicion))
        End If
        Return cantFinal

    End Function

    Function cerrarMes(ByVal articulo As String, ByVal fechaCierre As String) As Integer
        actualizarValoresArticulo(articulo, fechaCierre)
        cargarValoresArticulo(articulo, fechaCierre)

        Dim bodega As String

        Dim mes As String
        Dim anio As String

        Dim cantAnt As Double
        Dim cantEnt As Double
        Dim cantSal As Double
        Dim cantFin As Double

        Dim costoAnt As Double
        Dim costoEnt As Double
        Dim costoSal As Double
        Dim costoFin As Double
        Dim validadorCantFin As Double
        Dim validadorCostFin As Double
        Dim validadorCostFinAdm As Double

        Dim cantAntAdm As Double
        Dim cantEntAdm As Double
        Dim cantSalAdm As Double
        Dim cantFinAdm As Double
        Dim cantEntSal As Double = 0

        Dim costoAntAdm As Double
        Dim costoEntAdm As Double
        Dim costoSalAdm As Double
        Dim costoFinAdm As Double

        Dim presupuesto As Double
        Dim consumido As Double
        Dim consumido2 As Double
        Dim consumidoAnt As Double
        Dim porConsumir As Double

        Dim costoProm As Double
        Dim costoPromNuevo As Double
        Dim costoPromNuevoAdm As Double

        Dim codFec As Integer ' = CInt(StrReverse(anio)) + CInt(mes)
        Dim codFecAnt As Integer '= obtenerCodigoFechaAnterior(CInt(mes), CInt(anio), "D")


        Dim cantFilas As Integer
        Dim cantCol As Integer

        'Dim cantFilasProm As Integer
        'Dim cantColProm As Integer

        cantFilas = dtgVistas.RowCount - 1
        cantCol = dtgVistas.ColumnCount

        progress.Maximum = cantFilas - 1
        Try
            For i As Integer = 0 To cantFilas - 1 Step 1
                progress.Value = i

                mes = dtgVistas.Rows(i).Cells("MES").Value.ToString()
                anio = dtgVistas.Rows(i).Cells("ANIO").Value.ToString()
                codFec = CInt(anio & Microsoft.VisualBasic.Strings.Right("0" & mes, 2))
                codFecAnt = obtenerCodigoFechaAnterior(CInt(mes), CInt(anio), "D")


                condicion += " AND CONVERT(INT,CONVERT(VARCHAR(4),ANIO)+RIGHT('0'+CONVERT(VARCHAR(2),MES),2))=" & codFec

                bodega = dtgVistas.Rows(i).Cells("BODEGA").Value.ToString()

                cantAnt = dtgVistas.Rows(i).Cells("CANTIDAD_ANTERIOR").Value.ToString()
                cantEnt = dtgVistas.Rows(i).Cells("ENTRADAS").Value.ToString()
                cantSal = dtgVistas.Rows(i).Cells("SALIDAS").Value.ToString()
                cantFin = cantAnt + cantEnt - cantSal

                'quitamos las cantidades negativas de los reportes

                If cantAnt < 0 Then
                    cantAnt = 0
                End If

                If cantFin < 0 Then
                    cantFin = 0
                End If

                costoProm = dtgVistas.Rows(i).Cells("COSTO_PROM").Value.ToString()

                costoAnt = obtenerCostoFinal(codFecAnt, bodega, articulo, fechaCierre)
                costoEnt = dtgVistas.Rows(i).Cells("COSTO_ENTRADAS").Value.ToString()
                costoSal = dtgVistas.Rows(i).Cells("COSTO_SALIDAS").Value.ToString()



                Try
                    costoPromNuevo = Abs(costoEnt - costoSal) / Abs(cantEnt - cantSal)
                    If Double.IsNaN(costoPromNuevo) Or Double.IsInfinity(costoPromNuevo) Then
                        costoPromNuevo = 0
                    End If
                    If costoPromNuevo = 0 Then
                        costoPromNuevo = costoProm
                    End If
                    'MsgBox(costoPromNuevo.ToString + " try")
                Catch ex As Exception
                    costoPromNuevo = costoProm



                    ' MsgBox(costoPromNuevo.ToString + " cantch")
                End Try
                ' EN caso de que el costo promedio sea 0 volvemos al procesos de obtener un costo promedio para las entradas y salidas en base a
                'la division de CostoEntradas/CantidadEntradas o CostoSalidas/CantidadSalidas
                If costoPromNuevo = 0 Then
                    If cantEnt = 0 Then
                        If cantSal = 0 Then
                            costoPromNuevo = 0
                        Else
                            costoPromNuevo = costoSal / cantSal
                        End If
                    Else
                        costoPromNuevo = costoEnt / cantEnt
                    End If
                End If

                '**************************ajustamos el cambio de cantidades finales diferentes a negativo*****************
                validadorCantFin = cantAnt + cantEnt - cantSal
                If validadorCantFin <> cantFin Then
                    validadorCantFin = cantFin - validadorCantFin
                    If validadorCantFin < 0 Then
                        If cantSal <> 0 Then
                            cantSal = cantSal + Abs(validadorCantFin)
                        Else
                            If cantEnt - Abs(validadorCantFin) >= 0 Then
                                cantEnt = cantEnt - Abs(validadorCantFin)
                            Else
                                cantSal = cantSal + Abs(validadorCantFin)
                            End If
                        End If


                    Else
                        If cantSal <> 0 Then
                            cantEnt = cantEnt + Abs(validadorCantFin)
                        Else
                            If cantSal - Abs(validadorCantFin) >= 0 Then
                                cantSal = cantSal - Abs(validadorCantFin)
                            Else
                                cantEnt = cantEnt + Abs(validadorCantFin)
                            End If
                        End If

                    End If
                End If


                '**********************************************************************



                costoEnt = cantEnt * costoPromNuevo
                costoSal = cantSal * costoPromNuevo

                costoFin = cantFin * costoProm

                validadorCostFin = costoAnt + costoEnt - costoSal
                If validadorCostFin <> costoFin Then
                    validadorCostFin = costoFin - validadorCostFin
                    If validadorCostFin < 0 Then
                        If costoSal <> 0 Then
                            costoSal = costoSal + Abs(validadorCostFin)
                        Else
                            If costoEnt - Abs(validadorCostFin) >= 0 Then
                                costoEnt = costoEnt - Abs(validadorCostFin)
                            Else
                                costoSal = costoSal + Abs(validadorCostFin)
                            End If
                        End If


                    Else
                        If costoSal <> 0 Then
                            costoEnt = costoEnt + Abs(validadorCostFin)
                        Else
                            If costoSal - Abs(validadorCostFin) >= 0 Then
                                costoSal = costoSal - Abs(validadorCostFin)
                            Else
                                costoEnt = costoEnt + Abs(validadorCostFin)
                            End If
                        End If

                    End If
                End If

                If fechaCierre = "D" Then
                    query = "UPDATE VITALICIA.U_CLEV_ANA_TOT_DOC "
                Else
                    query = "UPDATE VITALICIA.U_CLEV_ANA_TOT_AUD "
                End If
                query += "SET CANTIDAD_ANTERIOR=" & cantAnt & ",ENTRADAS=" & cantEnt & ",SALIDAS=" & cantSal & ", CANTIDAD_FINAL=" & cantFin & ", COSTO_ANTERIOR=" & costoAnt & ",COSTO_ENTRADAS=" & costoEnt & ", COSTO_SALIDAS=" & costoSal & ", COSTO_FINAL=" & costoFin & ", COSTO_PROM=" & costoPromNuevo & ""
                query += " WHERE ARTICULO ='" & articulo & "'"
                query += " AND BODEGA='" & bodega & "'"
                query += " AND ANIO ='" & anio & "'"
                query += " AND MES ='" & mes & "'"

                ejecutarComandosSQL(query)

                '************OBTENEMOS LAS CANTIDADES ADMISIBLES *****************




                cantAntAdm = obtenerCantidadFinalAdm(codFecAnt, bodega, articulo, fechaCierre)
                'If cantEnt = 0 And cantSal = 0 Then
                '    cantFinAdm = cantAntAdm
                'Else
                cantFinAdm = obtenerCantidadAnteriorAdm(bodega, articulo, codFec, fechaCierre)
                'End If

                presupuesto = dtgVistas.Rows(i).Cells("PRESUPUESTADO").Value.ToString()
                consumido = dtgVistas.Rows(i).Cells("CONSUMIDO").Value.ToString()
                consumidoAnt = dtgVistas.Rows(i).Cells("CONSUMIDO_ANT").Value.ToString()

                porConsumir = presupuesto - consumido - consumidoAnt
                If porConsumir < 0 Then
                    porConsumir = 0
                End If

                consumido2 = consumido + consumidoAnt
                'totConsumido = consumidoObras(presupuesto, consumido, consumidoAnt)
                'porConsumir = totalPorConsumir(presupuesto, consumido, consumidoAnt)
                If consumido2 > presupuesto Then
                    consumido = presupuesto - consumidoAnt
                    If consumido < 0 Then
                        consumido = 0
                    End If
                End If



                If Abs(cantAntAdm - cantSal - cantFinAdm) > cantEnt Then
                    If Abs(cantAntAdm - cantSal - cantFinAdm + cantEnt) <= porConsumir Then
                        cantSalAdm = Abs(cantAntAdm - cantSal - cantFinAdm + cantEnt)
                    Else
                        cantSalAdm = porConsumir

                    End If
                Else
                    If cantSal <= porConsumir Then
                        cantSalAdm = cantSal

                    Else
                        cantSalAdm = porConsumir

                    End If
                End If


                cantEntAdm = cantSalAdm + cantFinAdm - cantAntAdm



                'CALCULAMOS EL COSTO PROMEDIO ADMISIBLE
                costoPromNuevoAdm = costoPromNuevo 'calcularCostoPromAdm(bodega, articulo, codFecAnt, cantEntAdm, costoEntAdm, fechaCierre, costoProm)


                If cantEntAdm < 0 Then
                    cantEntSal = Abs(cantEntAdm)
                    cantEntAdm = 0
                End If

                cantSalAdm += cantEntSal

                If cantAnt = 0 Then
                    costoAntAdm = 0
                Else
                    costoAntAdm = cantAntAdm * costoAnt / cantAnt
                    If Double.IsNaN(costoAntAdm) Or Double.IsInfinity(costoAntAdm) Then
                        costoAntAdm = 0
                    End If
                End If

                If cantEnt = 0 Then
                    costoEntAdm = cantEntAdm * costoPromNuevoAdm
                Else
                    costoEntAdm = cantEntAdm * costoEnt / cantEnt
                    If Double.IsNaN(costoEntAdm) Or Double.IsInfinity(costoEntAdm) Then
                        costoEntAdm = 0
                    End If
                End If

                If cantSal = 0 Then
                    costoSalAdm = cantSalAdm * costoPromNuevoAdm
                Else
                    costoSalAdm = cantSalAdm * costoSal / cantSal
                    If Double.IsNaN(costoSalAdm) Or Double.IsInfinity(costoSalAdm) Then
                        costoSalAdm = 0
                    End If
                End If

                If cantFin = 0 Then
                    costoFinAdm = 0
                Else
                    costoFinAdm = cantFinAdm * costoFin / cantFin
                    If Double.IsNaN(costoFinAdm) Or Double.IsInfinity(costoFinAdm) Then
                        costoFinAdm = 0
                    End If

                End If

                If cantEntAdm = 0 Then
                    If costoEntAdm < 0 Then
                        costoSalAdm = costoSalAdm - costoEntAdm
                        costoEntAdm = 0
                    End If
                End If

                If cantSalAdm = 0 Then
                    If costoSalAdm < 0 Then
                        costoEntAdm = costoEntAdm - costoSalAdm
                        costoSalAdm = 0
                    End If
                End If



                validadorCostFinAdm = costoAntAdm + costoEntAdm - costoSalAdm
                If validadorCostFinAdm <> costoFinAdm Then
                    validadorCostFinAdm = costoFinAdm - validadorCostFinAdm
                    If validadorCostFinAdm < 0 Then
                        If costoSalAdm <> 0 Then
                            costoSalAdm = costoSalAdm + Abs(validadorCostFinAdm)
                        Else
                            If costoEntAdm - Abs(validadorCostFinAdm) >= 0 Then
                                costoEntAdm = costoEntAdm - Abs(validadorCostFinAdm)
                            Else
                                costoSalAdm = costoSalAdm + Abs(validadorCostFinAdm)
                            End If
                        End If


                    Else
                        If costoEntAdm <> 0 Then
                            costoEntAdm = costoEntAdm + Abs(validadorCostFinAdm)
                        Else
                            If costoSalAdm - Abs(validadorCostFinAdm) >= 0 Then
                                costoSalAdm = costoSalAdm - Abs(validadorCostFinAdm)
                            Else
                                costoEntAdm = costoEntAdm + Abs(validadorCostFinAdm)
                            End If
                        End If

                    End If
                End If


                If fechaCierre = "D" Then
                    query = "UPDATE VITALICIA.U_CLEV_ANA_ADM_DOC "
                Else
                    query = "UPDATE VITALICIA.U_CLEV_ANA_ADM_AUD "
                End If
                query += " SET CANTIDAD_ANTERIOR=" & cantAntAdm & ",ENTRADAS=" & cantEntAdm & ",SALIDAS=" & cantSalAdm & ",CANTIDAD_FINAL=" & cantFinAdm & ",COSTO_ANTERIOR=" & costoAntAdm & ",COSTO_ENTRADAS=" & costoEntAdm & ",COSTO_SALIDAS=" & costoSalAdm & ",COSTO_FINAL=" & costoFinAdm & ", COSTO_PROM=" & costoPromNuevoAdm & ", AJUSTES=" & cantEntSal & ""
                query += " WHERE ARTICULO ='" & articulo & "'"
                query += " AND BODEGA='" & bodega & "'"
                query += " AND ANIO ='" & anio & "'"
                query += " AND MES ='" & mes & "'"

                ejecutarComandosSQL(query)
                cantEntSal = 0
            Next
        Catch ex As Exception
            MsgBox("No se pudo completar el proceso de cierre.")
        End Try


        Return 0
    End Function

    Private Sub btnAplicar_Click(sender As System.Object, e As System.EventArgs) Handles btnAplicar.Click
        If txtDescDesde.Text = "ARTÍCULO NO VÁLIDO" Then
            MsgBox("Ingrese Artículos válidos.")
        Else
            cerrarMes(txtArticuloDesde.Text, "D")

            cerrarMes(txtArticuloDesde.Text, "A")
            MsgBox("OPERACIÓN CONCLUIDA")


        End If
    End Sub

    Private Sub txtArticuloDesde_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtArticuloDesde.TextChanged
        txtDescDesde.Text = mostrarDescArticulo(txtArticuloDesde.Text)
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing

        ' Estos son los valores posibles
        Select Case e.CloseReason
            Case CloseReason.ApplicationExitCall
            Case CloseReason.FormOwnerClosing
            Case CloseReason.MdiFormClosing
            Case CloseReason.None
            Case CloseReason.TaskManagerClosing
            Case CloseReason.UserClosing
                cierreMes.Enabled = True
            Case CloseReason.WindowsShutDown
        End Select
    End Sub


    Function mostrarDescArticulo(ByVal articulo As String) As String
        Dim desc As String
        If articulo = "" Then
            desc = "TODOS"
        Else

            columna = "DESCRIPCION"
            tabla = esquema & ".ARTICULO"
            condicion = " WHERE ARTICULO='" & articulo & "'"


            Try
                desc = DevolverDatoQuery(columna, tabla, condicion)
            Catch ex As Exception
                desc = ""
            End Try
        End If

        If desc = "" Then
            desc = "ARTÍCULO NO VÁLIDO"
        End If

        Return desc
    End Function

    Private Sub btnVolver_Click(sender As System.Object, e As System.EventArgs) Handles btnVolver.Click
        cierreMes.Enabled = True
        Me.Close()
    End Sub



#Region "funciones F1"
    Private Sub txtCcDesde_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtArticuloDesde.KeyUp
        ' DETERMINAR CUANDO LA TECLA F1 A SIDO PRESIONADA

        If e.KeyCode = Keys.F1 Then
            ' LLMAR A OTRO FORMULARIO ENVIANDO UN PARAMETRO INICIAL
            buscador.lblbOrigen.Text = "Ajuste"
            buscador.Show()
            buscador.txtParametro.Text = txtArticuloDesde.Text
        End If
    End Sub


#End Region

#Region "funciones doble click"
    Private Sub txtCcDesde_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtArticuloDesde.DoubleClick
        buscador.lblbOrigen.Text = "Ajuste"
        buscador.Show()
        buscador.txtParametro.Text = txtArticuloDesde.Text
    End Sub
#End Region

    Private Sub pictureBodega_Click(sender As System.Object, e As System.EventArgs) Handles pictureBodega.Click
        buscador.lblbOrigen.Text = "Ajuste"
        buscador.Show()
        buscador.txtParametro.Text = txtArticuloDesde.Text
    End Sub
End Class