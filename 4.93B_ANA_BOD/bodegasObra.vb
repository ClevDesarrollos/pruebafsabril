﻿Public Class bodegasObra
    Dim query As String
    Dim esquema As String = ObtenerEsquema()
    ' Dim presupuestos As String
    Sub cargarExistencias()
        query = " SELECT SALDO_BODEGA,CODIGO_B,BODEGA,CODIGO_A,ARTICULO FROM ("
        query += " SELECT CODIGO_B,BODEGA,CODIGO_A,ARTICULO,(ENTRADAS-SALIDAS)SALDO_BODEGA FROM"
        query += " ("
        query += " SELECT BODEGA CODIGO_B,"

        query += " (SELECT NOMBRE "
        query += " FROM VITALERP.VITALICIA.BODEGA C"
        query += " WHERE B.BODEGA=C.BODEGA)BODEGA,"

        query += " ARTICULO CODIGO_A,"
        query += " (SELECT DESCRIPCION "
        query += " FROM VITALERP.VITALICIA.ARTICULO C"
        query += " WHERE B.ARTICULO=C.ARTICULO)ARTICULO,"

        query += " ISNULL((SELECT SUM(ABS(CANTIDAD))"
        query += " FROM VITALERP.VITALICIA.TRANSACCION_INV A"
        query += " WHERE NATURALEZA='E'"
        query += " AND A.ARTICULO=B.ARTICULO"
        query += " AND A.BODEGA=B.BODEGA"
        query += " AND A.LOCALIZACION=B.LOCALIZACION),0)ENTRADAS"
        query += " ,ISNULL((SELECT SUM(ABS(CANTIDAD))"
        query += " FROM VITALERP.VITALICIA.TRANSACCION_INV A"
        query += " WHERE NATURALEZA='S'"
        query += " AND A.ARTICULO=B.ARTICULO"
        query += " AND A.BODEGA=B.BODEGA"
        query += " AND A.LOCALIZACION=B.LOCALIZACION),0)SALIDAS"
        query += " FROM VITALERP.VITALICIA.TRANSACCION_INV B"
        query += " GROUP BY BODEGA,ARTICULO,LOCALIZACION"
        query += " )VISTA1"
        query += " ) VISTA2"
        query += " WHERE CODIGO_B LIKE 'DO%'"
        query += " AND SALDO_BODEGA <>0"
        query += " ORDER BY BODEGA,ARTICULO"

        llenarTabla(query, dtgBodegas)


        dtgBodegas.Columns("CODIGO_B").HeaderText = "CÓDIGO DE BODEGA"
        dtgBodegas.Columns("BODEGA").HeaderText = "BODEGA"
        dtgBodegas.Columns("CODIGO_A").HeaderText = "CÓDIGO DE ARTÍCULO"
        dtgBodegas.Columns("ARTICULO").HeaderText = "ARTÍCULO"
        dtgBodegas.Columns("SALDO_BODEGA").HeaderText = "SALDO EN BODEGA"

        dtgBodegas.Columns("SALDO_BODEGA").DefaultCellStyle.Format = "###,##0.00 "

        dtgBodegas.Columns("CODIGO_B").Width = 100
        dtgBodegas.Columns("BODEGA").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        dtgBodegas.Columns("CODIGO_A").Width = 120
        dtgBodegas.Columns("ARTICULO").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        dtgBodegas.Columns("SALDO_BODEGA").Width = 100


    End Sub
    Private Sub bodegasObra_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cargarExistencias()
        'obtenerPresupuestos()
        If dtgBodegas.RowCount = 1 Then
            btnSi.Text = "Continuar"
            btnNo.Text = "Cancelar"
            lblMensaje.Text = "No se encontraron bodegas de obra con existencias, presione ''Continuar'' para continuar con el proceso de cierre o ''Cancelar'' para abortar el proceso."
        End If
    End Sub

    'Sub obtenerPresupuestos()
    '    Dim proyecto As String = ""
    '    Dim filas As Integer
    '    Dim cont As Integer
    '    If cierreMes.checkActivos.Checked = True And cierreMes.checkNoActivos.Checked = True And cierreMes.checkNoAutorizados.Checked = True Then
    '        query = " SELECT PROYECTO FROM VITALERP.VITALICIA.U_CLEV_ANA_PROYECTOS"
    '    Else
    '        If cierreMes.checkActivos.Checked = True And cierreMes.checkNoActivos.Checked = True Then
    '            query = " SELECT PROYECTO FROM VITALERP.VITALICIA.U_CLEV_ANA_PROYECTOS"
    '            query += " WHERE AUTORIZADO_APS=1"
    '        Else
    '            If cierreMes.checkActivos.Checked = True Then
    '                query = " SELECT PROYECTO FROM VITALERP.VITALICIA.U_CLEV_ANA_PROYECTOS"
    '                query += " WHERE AUTORIZADO_APS=1"
    '                query += " AND ACTIVO=1"
    '            Else
    '                MsgBox("error")
    '            End If
    '        End If
    '    End If
    '    llenarTabla(query, dtgProyectos)

    '    filas = dtgProyectos.RowCount - 1

    '    cont = 0
    '    For Each row As DataGridViewRow In dtgProyectos.Rows
    '        cont += 1
    '        proyecto += "''"
    '        proyecto += row.Cells("PROYECTO").Value
    '        If filas = cont Then
    '            proyecto += "''"
    '            Exit For
    '        Else
    '            proyecto += "'',"
    '        End If

    '    Next

    '    presupuestos = proyecto


    'End Sub


    Private Sub btnSi_Click(sender As System.Object, e As System.EventArgs) Handles btnSi.Click
        cierreMes.btnCerrar.Enabled = False
        'btnPass.Enabled = False
        'btnPasadas.Enabled = False

        cierreMes.anioGlobal = separarCadena(cierreMes.cmbMes.SelectedItem.ToString, True, " --- ")
        cierreMes.mesGlobal = separarCadena(cierreMes.cmbMes.SelectedItem.ToString, False, " --- ")
        ' lInfo.Text = "Espere por favor..."

        query = " EXEC " & esquema & ".U_CLEV_ANA_SP_NUEVO_MES"
        query += " @MES = " & cierreMes.mesGlobal & ", "
        query += " @ANIO = " & cierreMes.anioGlobal & ""

        ejecutarComandosSQL(query)

        cierreMes.llenarTablaMes()
        cierreMes.llenarTablaMesAud()
        cierreMes.registros = CStr(cierreMes.dtgMes.RowCount - 1)
        cierreMes.registrosAud = CStr(cierreMes.dtgMesAud.RowCount - 1)
        cierreMes.hiloSegundoPlano.RunWorkerAsync(0)

        cierreMes.Enabled = True
        Me.Close()
    End Sub

    Private Sub btnNo_Click(sender As System.Object, e As System.EventArgs) Handles btnNo.Click
        cierreMes.Enabled = True
        Me.Close()
    End Sub
End Class