﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Presupuestos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Presupuestos))
        Me.btnMarcar = New System.Windows.Forms.Button()
        Me.btnDesmarcar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.dtgBodegas = New System.Windows.Forms.DataGridView()
        Me.dtgBodegasAux = New System.Windows.Forms.DataGridView()
        CType(Me.dtgBodegas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgBodegasAux, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnMarcar
        '
        Me.btnMarcar.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMarcar.Location = New System.Drawing.Point(12, 84)
        Me.btnMarcar.Name = "btnMarcar"
        Me.btnMarcar.Size = New System.Drawing.Size(130, 36)
        Me.btnMarcar.TabIndex = 174
        Me.btnMarcar.Text = "Marcar todos"
        Me.btnMarcar.UseVisualStyleBackColor = True
        '
        'btnDesmarcar
        '
        Me.btnDesmarcar.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDesmarcar.Location = New System.Drawing.Point(148, 84)
        Me.btnDesmarcar.Name = "btnDesmarcar"
        Me.btnDesmarcar.Size = New System.Drawing.Size(130, 36)
        Me.btnDesmarcar.TabIndex = 174
        Me.btnDesmarcar.Text = "Desmarcar todos"
        Me.btnDesmarcar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 9)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(633, 72)
        Me.Label1.TabIndex = 175
        Me.Label1.Text = "DEFINICIÓN DE PROYECTOS INCLUIDOS"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnAceptar.Font = New System.Drawing.Font("Arial Narrow", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(203, 490)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(130, 36)
        Me.btnAceptar.TabIndex = 174
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnCancelar.Font = New System.Drawing.Font("Arial Narrow", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(339, 490)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(130, 36)
        Me.btnCancelar.TabIndex = 174
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'dtgBodegas
        '
        Me.dtgBodegas.AllowUserToResizeColumns = False
        Me.dtgBodegas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgBodegas.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dtgBodegas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dtgBodegas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgBodegas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgBodegas.ColumnHeadersHeight = 46
        Me.dtgBodegas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgBodegas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgBodegas.EnableHeadersVisualStyles = False
        Me.dtgBodegas.GridColor = System.Drawing.Color.Black
        Me.dtgBodegas.Location = New System.Drawing.Point(12, 126)
        Me.dtgBodegas.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtgBodegas.Name = "dtgBodegas"
        Me.dtgBodegas.RowTemplate.Height = 24
        Me.dtgBodegas.Size = New System.Drawing.Size(634, 343)
        Me.dtgBodegas.TabIndex = 177
        '
        'dtgBodegasAux
        '
        Me.dtgBodegasAux.AllowUserToResizeColumns = False
        Me.dtgBodegasAux.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgBodegasAux.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dtgBodegasAux.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dtgBodegasAux.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgBodegasAux.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dtgBodegasAux.ColumnHeadersHeight = 46
        Me.dtgBodegasAux.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgBodegasAux.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgBodegasAux.EnableHeadersVisualStyles = False
        Me.dtgBodegasAux.GridColor = System.Drawing.Color.Black
        Me.dtgBodegasAux.Location = New System.Drawing.Point(340, 75)
        Me.dtgBodegasAux.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtgBodegasAux.Name = "dtgBodegasAux"
        Me.dtgBodegasAux.RowTemplate.Height = 24
        Me.dtgBodegasAux.Size = New System.Drawing.Size(277, 33)
        Me.dtgBodegasAux.TabIndex = 177
        Me.dtgBodegasAux.Visible = False
        '
        'Presupuestos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 542)
        Me.Controls.Add(Me.dtgBodegasAux)
        Me.Controls.Add(Me.dtgBodegas)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnDesmarcar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnMarcar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Presupuestos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Presupuestos"
        CType(Me.dtgBodegas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgBodegasAux, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnMarcar As System.Windows.Forms.Button
    Friend WithEvents btnDesmarcar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents dtgBodegas As System.Windows.Forms.DataGridView
    Friend WithEvents dtgBodegasAux As System.Windows.Forms.DataGridView
End Class
