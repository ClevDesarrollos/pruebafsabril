﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class bodegasObra
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(bodegasObra))
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSi = New System.Windows.Forms.Button()
        Me.btnNo = New System.Windows.Forms.Button()
        Me.dtgBodegas = New System.Windows.Forms.DataGridView()
        Me.dtgProyectos = New System.Windows.Forms.DataGridView()
        CType(Me.dtgBodegas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgProyectos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMensaje
        '
        Me.lblMensaje.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMensaje.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.Location = New System.Drawing.Point(9, 81)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(630, 68)
        Me.lblMensaje.TabIndex = 169
        Me.lblMensaje.Text = "Se encontraron existencias en las bodegas de obra mostradas a continuación. ¿Dese" & _
    "a continuar?"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(627, 72)
        Me.Label1.TabIndex = 169
        Me.Label1.Text = "Existencias en Bodegas de Obra"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnSi
        '
        Me.btnSi.BackColor = System.Drawing.SystemColors.Control
        Me.btnSi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnSi.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSi.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSi.FlatAppearance.BorderSize = 2
        Me.btnSi.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSi.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSi.ForeColor = System.Drawing.Color.Black
        Me.btnSi.Location = New System.Drawing.Point(18, 153)
        Me.btnSi.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSi.Name = "btnSi"
        Me.btnSi.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnSi.Size = New System.Drawing.Size(134, 45)
        Me.btnSi.TabIndex = 173
        Me.btnSi.Text = "Si"
        Me.btnSi.UseVisualStyleBackColor = False
        '
        'btnNo
        '
        Me.btnNo.BackColor = System.Drawing.SystemColors.Control
        Me.btnNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnNo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNo.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnNo.FlatAppearance.BorderSize = 2
        Me.btnNo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnNo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnNo.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNo.ForeColor = System.Drawing.Color.Black
        Me.btnNo.Location = New System.Drawing.Point(160, 153)
        Me.btnNo.Margin = New System.Windows.Forms.Padding(4)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnNo.Size = New System.Drawing.Size(134, 45)
        Me.btnNo.TabIndex = 173
        Me.btnNo.Text = "No"
        Me.btnNo.UseVisualStyleBackColor = False
        '
        'dtgBodegas
        '
        Me.dtgBodegas.AllowUserToResizeColumns = False
        Me.dtgBodegas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgBodegas.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dtgBodegas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dtgBodegas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgBodegas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgBodegas.ColumnHeadersHeight = 46
        Me.dtgBodegas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgBodegas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgBodegas.EnableHeadersVisualStyles = False
        Me.dtgBodegas.GridColor = System.Drawing.Color.Black
        Me.dtgBodegas.Location = New System.Drawing.Point(12, 206)
        Me.dtgBodegas.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtgBodegas.Name = "dtgBodegas"
        Me.dtgBodegas.RowTemplate.Height = 24
        Me.dtgBodegas.Size = New System.Drawing.Size(628, 499)
        Me.dtgBodegas.TabIndex = 176
        '
        'dtgProyectos
        '
        Me.dtgProyectos.AllowUserToResizeColumns = False
        Me.dtgProyectos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgProyectos.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dtgProyectos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dtgProyectos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial Narrow", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgProyectos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dtgProyectos.ColumnHeadersHeight = 46
        Me.dtgProyectos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgProyectos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgProyectos.EnableHeadersVisualStyles = False
        Me.dtgProyectos.GridColor = System.Drawing.Color.Black
        Me.dtgProyectos.Location = New System.Drawing.Point(13, 13)
        Me.dtgProyectos.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtgProyectos.Name = "dtgProyectos"
        Me.dtgProyectos.RowTemplate.Height = 24
        Me.dtgProyectos.Size = New System.Drawing.Size(81, 35)
        Me.dtgProyectos.TabIndex = 176
        Me.dtgProyectos.Visible = False
        '
        'bodegasObra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(652, 718)
        Me.Controls.Add(Me.dtgProyectos)
        Me.Controls.Add(Me.dtgBodegas)
        Me.Controls.Add(Me.btnNo)
        Me.Controls.Add(Me.btnSi)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblMensaje)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "bodegasObra"
        Me.Text = "Existencias en bodegas de obra"
        CType(Me.dtgBodegas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgProyectos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblMensaje As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSi As System.Windows.Forms.Button
    Friend WithEvents btnNo As System.Windows.Forms.Button
    Friend WithEvents dtgBodegas As System.Windows.Forms.DataGridView
    Friend WithEvents dtgProyectos As System.Windows.Forms.DataGridView
End Class
