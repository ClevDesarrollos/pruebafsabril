﻿Imports System.Math
Public Class transaccionesPasadas
    Dim esquema As String = ObtenerEsquema()
#Region "Funciones SQL"
    Function comprobarTransacciones() As Boolean
        Dim query As String
        Dim filas As Integer
        query = " SELECT COUNT(*)"
        query += " FROM VITALICIA.U_CLEV_ANA_TRANS_INV"
        query += " WHERE REPORTE='SI'"
        filas = CInt(DevolverDatoQuery2(query))
        If filas > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub llenarTablaOtrasTransacciones()
        Dim query As String

        query = " SELECT FECHA FECHA_DOC , FECHA_HORA_TRANSAC FECHA_AUD ,BODEGA,ARTICULO,(SELECT DESCRIPCION FROM VITALERP.VITALICIA.ARTICULO B WHERE B.ARTICULO=A.ARTICULO) DESCRIPCION,CANTIDAD,COSTO_TOT_FISC_LOC COSTO, REPORTE INCLUIDO,  A.AUDIT_TRANS_INV, A.CONSECUTIVO"
        query += " FROM VITALICIA.TRANSACCION_INV A, VITALICIA.U_CLEV_ANA_TRANS_INV B"
        query += " WHERE REPORTE='NO'"
        query += " AND A.AUDIT_TRANS_INV=B.AUDIT_TRANS_INV"
        query += " AND A.CONSECUTIVO=B.CONSECUTIVO"

        llenarTabla(query, dtgOtras)
    End Sub

    Sub actualizarEstado(ByVal audit As String, ByVal consecutivo As String)

        Dim query As String
        query = " UPDATE VITALICIA.U_CLEV_ANA_TRANS_INV SET REPORTE='SI' WHERE AUDIT_TRANS_INV='" & audit & "' AND CONSECUTIVO='" & consecutivo & "'"
        ejecutarComandosSQL(query)

    End Sub

#End Region

#Region "Otras funciones"
    Sub columnaCheck()
        Dim checkBoxColumn As New DataGridViewCheckBoxColumn()
        checkBoxColumn.HeaderText = ""
        checkBoxColumn.Width = 30
        checkBoxColumn.Name = "checkBoxColumn"
        dtgOtras.Columns.Insert(0, checkBoxColumn)
        checkBoxColumn.ReadOnly = False
    End Sub
#End Region


    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Dim contador As String = 0

        For Each row As DataGridViewRow In dtgOtras.Rows

            If row.Cells("checkBoxColumn").Value = True Then
                contador += 1
            End If

        Next

        progressPrincipal.Maximum = contador
        contador = 0
        For Each row As DataGridViewRow In dtgOtras.Rows

            If row.Cells("checkBoxColumn").Value = True Then
                Try
                    actualizarEstado(row.Cells("AUDIT_TRANS_INV").Value.ToString(), (row.Cells("CONSECUTIVO").Value.ToString()))

                Catch ex As Exception

                End Try
                Try
                    Ajustes.cerrarMes(row.Cells("ARTICULO").Value.ToString(), "D")
                    Ajustes.cerrarMes(row.Cells("ARTICULO").Value.ToString(), "A")
                Catch ex As Exception

                End Try

                contador += 1
                progressPrincipal.Value = contador
            End If

        Next

        If contador = 0 Then
            MsgBox("No se actualizó ningún artículo")
        Else
            MsgBox("Se actualizaron " & contador.ToString & " artículos exitosamente")
        End If
        'actualizarArticulo("0022-0132-0001")
        'MsgBox("OPERACIÓN CONCLUIDA")
        progressPrincipal.Value = 0

        CierreMes.Enabled = True
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        cierreMes.Enabled = True
        Me.Close()
    End Sub

    Private Sub cierreMes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        columnaCheck()

        If comprobarTransacciones() Then 'si es true significa que hay transacciones pendientes
            llenarTablaOtrasTransacciones()
        End If

    End Sub

    Private Sub dtgBodegas_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgOtras.CellContentClick
        Dim row As New DataGridViewRow
        Dim fil As Integer = dtgOtras.CurrentCell.RowIndex
        If dtgOtras.Item(0, fil).Value = True Then
            dtgOtras.Item(0, fil).Value = False
        Else
            dtgOtras.Item(0, fil).Value = True
        End If
    End Sub

    Private Sub dtgPedidos_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgOtras.CellValueChanged
        If e.RowIndex = -1 Then
            Return
        End If
        Dim a As Integer
        a = dtgOtras.RowCount - 1
        For Each row As DataGridViewRow In dtgOtras.Rows
            If a <> row.Cells("checkBoxColumn").RowIndex Then

                If row.Cells("checkBoxColumn").Value = True Then
                    row.DefaultCellStyle.BackColor = Color.Green
                Else
                    row.DefaultCellStyle.BackColor = Nothing
                End If
            End If
        Next
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        For Each row As DataGridViewRow In dtgOtras.Rows
            row.Cells("checkBoxColumn").Value = False
        Next
    End Sub

    Private Sub btnMarcar_Click(sender As System.Object, e As System.EventArgs) Handles btnMarcar.Click
        Dim a As Integer
        a = dtgOtras.RowCount - 1
        For Each row As DataGridViewRow In dtgOtras.Rows
            If a <> row.Cells("checkBoxColumn").RowIndex Then
                row.Cells("checkBoxColumn").Value = True
            End If
        Next
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing

        ' Estos son los valores posibles
        Select Case e.CloseReason
            Case CloseReason.ApplicationExitCall
            Case CloseReason.FormOwnerClosing
            Case CloseReason.MdiFormClosing
            Case CloseReason.None
            Case CloseReason.TaskManagerClosing
            Case CloseReason.UserClosing
                cierreMes.Enabled = True
            Case CloseReason.WindowsShutDown
        End Select
    End Sub



End Class

'Ultima revisión 22/04/2020