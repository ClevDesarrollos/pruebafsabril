﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Ajustes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Ajustes))
        Me.progress = New System.Windows.Forms.ProgressBar()
        Me.txtDescDesde = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.pictureBodega = New System.Windows.Forms.PictureBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtArticuloDesde = New System.Windows.Forms.TextBox()
        Me.dtgVistas = New System.Windows.Forms.DataGridView()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.Panel7.SuspendLayout()
        CType(Me.pictureBodega, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgVistas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'progress
        '
        Me.progress.Location = New System.Drawing.Point(18, 101)
        Me.progress.Name = "progress"
        Me.progress.Size = New System.Drawing.Size(821, 35)
        Me.progress.TabIndex = 154
        '
        'txtDescDesde
        '
        Me.txtDescDesde.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescDesde.Location = New System.Drawing.Point(329, 38)
        Me.txtDescDesde.Name = "txtDescDesde"
        Me.txtDescDesde.Size = New System.Drawing.Size(523, 54)
        Me.txtDescDesde.TabIndex = 153
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(3, 41)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(130, 29)
        Me.Label23.TabIndex = 151
        Me.Label23.Text = "Especificar: "
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.pictureBodega)
        Me.Panel7.Controls.Add(Me.progress)
        Me.Panel7.Controls.Add(Me.txtDescDesde)
        Me.Panel7.Controls.Add(Me.Label23)
        Me.Panel7.Controls.Add(Me.Label24)
        Me.Panel7.Controls.Add(Me.txtArticuloDesde)
        Me.Panel7.Location = New System.Drawing.Point(4, 9)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(855, 149)
        Me.Panel7.TabIndex = 160
        '
        'pictureBodega
        '
        Me.pictureBodega.BackColor = System.Drawing.Color.White
        Me.pictureBodega.Image = CType(resources.GetObject("pictureBodega.Image"), System.Drawing.Image)
        Me.pictureBodega.Location = New System.Drawing.Point(300, 42)
        Me.pictureBodega.Name = "pictureBodega"
        Me.pictureBodega.Size = New System.Drawing.Size(23, 27)
        Me.pictureBodega.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureBodega.TabIndex = 157
        Me.pictureBodega.TabStop = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Arial Narrow", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(366, 9)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(87, 29)
        Me.Label24.TabIndex = 150
        Me.Label24.Text = "Artículo"
        '
        'txtArticuloDesde
        '
        Me.txtArticuloDesde.Font = New System.Drawing.Font("Arial Narrow", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArticuloDesde.Location = New System.Drawing.Point(156, 41)
        Me.txtArticuloDesde.Name = "txtArticuloDesde"
        Me.txtArticuloDesde.Size = New System.Drawing.Size(167, 28)
        Me.txtArticuloDesde.TabIndex = 108
        '
        'dtgVistas
        '
        Me.dtgVistas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgVistas.Location = New System.Drawing.Point(537, 180)
        Me.dtgVistas.Name = "dtgVistas"
        Me.dtgVistas.RowTemplate.Height = 24
        Me.dtgVistas.Size = New System.Drawing.Size(67, 26)
        Me.dtgVistas.TabIndex = 163
        Me.dtgVistas.Visible = False
        '
        'btnAplicar
        '
        Me.btnAplicar.BackColor = System.Drawing.SystemColors.Control
        Me.btnAplicar.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnAplicar.FlatAppearance.BorderSize = 2
        Me.btnAplicar.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAplicar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAplicar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAplicar.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAplicar.ForeColor = System.Drawing.Color.Black
        Me.btnAplicar.Location = New System.Drawing.Point(227, 164)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(197, 44)
        Me.btnAplicar.TabIndex = 164
        Me.btnAplicar.Text = "Aplicar Ajustes"
        Me.btnAplicar.UseVisualStyleBackColor = False
        '
        'btnVolver
        '
        Me.btnVolver.BackColor = System.Drawing.SystemColors.Control
        Me.btnVolver.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnVolver.FlatAppearance.BorderSize = 2
        Me.btnVolver.FlatAppearance.CheckedBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnVolver.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnVolver.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnVolver.Font = New System.Drawing.Font("Arial Narrow", 13.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVolver.ForeColor = System.Drawing.Color.Black
        Me.btnVolver.Location = New System.Drawing.Point(431, 164)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(197, 44)
        Me.btnVolver.TabIndex = 164
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = False
        '
        'Ajustes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(871, 242)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnAplicar)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.dtgVistas)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Ajustes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ajustes"
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.pictureBodega, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgVistas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents progress As System.Windows.Forms.ProgressBar
    Friend WithEvents txtDescDesde As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtArticuloDesde As System.Windows.Forms.TextBox
    Friend WithEvents dtgVistas As System.Windows.Forms.DataGridView
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents pictureBodega As System.Windows.Forms.PictureBox
End Class
