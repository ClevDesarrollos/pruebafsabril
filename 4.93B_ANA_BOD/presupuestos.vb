﻿Public Class Presupuestos
    Dim query As String

    Sub columnaCheck()
        Dim checkBoxColumn As New DataGridViewCheckBoxColumn()
        checkBoxColumn.HeaderText = "AUTORIZADO APS"
        checkBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        checkBoxColumn.Name = "AUT"
        dtgBodegas.Columns.Insert(1, checkBoxColumn)
        checkBoxColumn.ReadOnly = False
    End Sub

    Sub columnaCheck2()
        Dim checkBoxColumn2 As New DataGridViewCheckBoxColumn()
        checkBoxColumn2.HeaderText = "ACTIVO"
        checkBoxColumn2.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        checkBoxColumn2.Name = "ACT"
        dtgBodegas.Columns.Insert(1, checkBoxColumn2)
        checkBoxColumn2.ReadOnly = False
    End Sub
    Sub columnaCheck3()
        Dim checkBoxColumn2 As New DataGridViewCheckBoxColumn()
        checkBoxColumn2.HeaderText = "CERRAR SALDOS"
        checkBoxColumn2.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        checkBoxColumn2.Name = "SAL"
        dtgBodegas.Columns.Insert(1, checkBoxColumn2)
        checkBoxColumn2.ReadOnly = False
    End Sub


    Private Sub dtgBodegas_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgBodegas.CellValueChanged, dtgBodegasAux.CellValueChanged
        If e.RowIndex = -1 Then
            Return
        End If
        Dim a As Integer
        a = dtgBodegas.RowCount - 1
        For Each row As DataGridViewRow In dtgBodegas.Rows
            If a <> row.Cells("AUT").RowIndex Then
                If row.Cells("AUT").Value = True Then
                    row.Cells("AUTORIZADO_APS").Value = 1
                Else
                    row.Cells("AUTORIZADO_APS").Value = 0
                End If
            End If
            If a <> row.Cells("ACT").RowIndex Then
                If row.Cells("ACT").Value = True Then
                    row.Cells("ACTIVO").Value = 1
                Else
                    row.Cells("ACTIVO").Value = 0
                End If
            End If
            If a <> row.Cells("SAL").RowIndex Then
                If row.Cells("SAL").Value = True Then
                    row.Cells("SALDOS").Value = 1
                Else
                    row.Cells("SALDOS").Value = 0
                End If
            End If
        Next

    End Sub

    Private Sub dtgBodegas_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgBodegas.CellContentClick, dtgBodegasAux.CellContentClick
        Dim row As New DataGridViewRow
        Dim fil As Integer = dtgBodegas.CurrentCell.RowIndex
        If e.ColumnIndex = 1 Then
            If dtgBodegas.Item(1, fil).Value = True Then
                dtgBodegas.Item(1, fil).Value = False
            Else
                dtgBodegas.Item(1, fil).Value = True
            End If
        Else
            If e.ColumnIndex = 2 Then
                If dtgBodegas.Item(2, fil).Value = True Then
                    dtgBodegas.Item(2, fil).Value = False
                Else
                    dtgBodegas.Item(2, fil).Value = True
                End If
            Else
                If e.ColumnIndex = 3 Then
                    If dtgBodegas.Item(3, fil).Value = True Then
                        dtgBodegas.Item(3, fil).Value = False
                        query = " DELETE FROM VITALERP.VITALICIA.U_CLEV_SALDOS WHERE PROYECTO='" & dtgBodegas.Item(0, fil).Value & "'"
                        ejecutarComandosSQL(query)
                    Else
                        dtgBodegas.Item(3, fil).Value = True
                        query = " DELETE FROM VITALERP.VITALICIA.U_CLEV_SALDOS WHERE PROYECTO='" & dtgBodegas.Item(0, fil).Value & "'"
                        ejecutarComandosSQL(query)
                        query = " INSERT INTO VITALERP.VITALICIA.U_CLEV_SALDOS (PROYECTO,ARTICULO,CANTIDAD)"
                        query += " SELECT PROYECTO,ARTICULO,CASE WHEN (CANTIDAD-CANTIDAD_ACUMULADA<0) THEN 0 ELSE CANTIDAD-CANTIDAD_ACUMULADA END"
                        query += " FROM VITALERP.VITALICIA.FASE_DESGLOSE_PY"
                        query += " WHERE PROYECTO='" & dtgBodegas.Item(0, fil).Value & "'"
                        query += " AND ORDEN_CAMBIO=1"
                        ejecutarComandosSQL(query)

                    End If
                End If
            End If
        End If

    End Sub

    Private Sub btnMarcar_Click(sender As System.Object, e As System.EventArgs) Handles btnMarcar.Click
        Dim a As Integer
        a = dtgBodegas.RowCount - 1
        For Each row As DataGridViewRow In dtgBodegas.Rows
            If a <> row.Cells("ACT").RowIndex Then
                row.Cells("ACT").Value = True
                row.Cells("AUT").Value = True
                row.Cells("SAL").Value = True
            End If
        Next
    End Sub

    Private Sub btnDesmarcar_Click(sender As System.Object, e As System.EventArgs) Handles btnDesmarcar.Click
        For Each row As DataGridViewRow In dtgBodegas.Rows
            row.Cells("ACT").Value = False
            row.Cells("AUT").Value = False
            row.Cells("SAL").Value = False
        Next
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click

        Dim a As Integer

        a = dtgBodegas.RowCount - 1
        For Each row As DataGridViewRow In dtgBodegas.Rows
            If a <> row.Cells("ACT").RowIndex Then
                query = " UPDATE VITALERP.VITALICIA.U_CLEV_ANA_PROYECTOS SET AUTORIZADO_APS=" & row.Cells("AUTORIZADO_APS").Value.ToString & ", ACTIVO=" & row.Cells("ACTIVO").Value.ToString & ", SALDOS=" & row.Cells("SALDOS").Value.ToString & " WHERE PROYECTO='" & row.Cells("PROYECTO").Value.ToString & "'"
                ejecutarComandosSQL(query)

                ejecutarComandosSQL(query)
            End If

        Next
        cierreMes.Enabled = True
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        cierreMes.Enabled = True
        Me.Close()
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing

        ' Estos son los valores posibles
        Select Case e.CloseReason
            Case CloseReason.ApplicationExitCall
            Case CloseReason.FormOwnerClosing
            Case CloseReason.MdiFormClosing
            Case CloseReason.None
            Case CloseReason.TaskManagerClosing
            Case CloseReason.UserClosing
                cierreMes.Enabled = True
            Case CloseReason.WindowsShutDown
        End Select
    End Sub

    Private Sub Presupuestos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim query As String

        query = " SELECT PROYECTO, AUTORIZADO_APS, ACTIVO,SALDOS"
        query += " FROM VITALERP.VITALICIA.U_CLEV_ANA_PROYECTOS"

        llenarTabla(query, dtgBodegas)
        columnaCheck3()
        columnaCheck2()
        columnaCheck()

        llenarTabla(query, dtgBodegasAux)

        Dim a As Integer
        a = dtgBodegas.RowCount - 1
        Dim cont As Integer = 0
        For Each row As DataGridViewRow In dtgBodegasAux.Rows
            If a <> row.Cells("AUTORIZADO_APS").RowIndex Then

                If row.Cells("AUTORIZADO_APS").Value = "1" Then
                    dtgBodegas.Rows(cont).Cells("AUT").Value = True
                Else
                    dtgBodegas.Rows(cont).Cells("AUT").Value = False
                End If
                If row.Cells("ACTIVO").Value = "1" Then
                    dtgBodegas.Rows(cont).Cells("ACT").Value = True
                Else
                    dtgBodegas.Rows(cont).Cells("ACT").Value = False
                End If
                If row.Cells("SALDOS").Value = "1" Then
                    dtgBodegas.Rows(cont).Cells("SAL").Value = True
                Else
                    dtgBodegas.Rows(cont).Cells("SAL").Value = False
                End If
                cont += 1
            End If
        Next

        dtgBodegas.Columns("AUT").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        dtgBodegas.Columns("ACT").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        dtgBodegas.Columns("PROYECTO").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells


        dtgBodegas.Columns("AUTORIZADO_APS").Visible = False
        dtgBodegas.Columns("ACTIVO").Visible = False
        dtgBodegas.Columns("SALDOS").Visible = False
    End Sub
End Class