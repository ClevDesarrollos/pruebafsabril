﻿Public Class cambiarPassword

    Dim esquema As String = ObtenerEsquema()
    Dim query As String
    Function obtenerPassAnt() As String
        Dim pass As String
        query = "SELECT PASS FROM " & esquema & ".U_CLEV_REP_ACCESOS WHERE REPORTE='" & Form1.codigoReporte & "'"
        pass = DevolverDatoQuery2(query)
        Return Desencriptar2(pass)
    End Function

    Function guardarPass() As Boolean
        Dim a As Boolean
        If txtPassNuevo.Text <> "" Then
            query = "UPDATE " & esquema & ".U_CLEV_REP_ACCESOS SET PASS = '" & Encriptar(txtPassNuevo.Text) & "' WHERE REPORTE='" & Form1.codigoReporte & "'"
            ejecutarComandosSQL(query)
            a = True
        Else
            a = False
        End If
        Return a
    End Function

    Private Sub ing_cambiarPassword_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        obtenerPassAnt()

    End Sub


    Private Sub txtPassAnt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtPassAnt.Validating
        If txtPassAnt.Text <> obtenerPassAnt() Then
            ErrorProvider.SetError(Me.txtPassAnt, "Las contraseñas no coinciden")
        Else
            ErrorProvider.SetError(Me.txtPassAnt, "")
        End If
    End Sub


    Private Sub txtPassNuevo_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtPassNuevo.Validating
        If txtPassNuevo.Text = "" Then
            ErrorProvider.SetError(Me.txtPassNuevo, "Seleccione una contraseña distinta")
        Else
            ErrorProvider.SetError(Me.txtPassNuevo, "")
        End If
    End Sub

    Private Sub txtPassConfirmar_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtPassConfirmar.Validating
        If txtPassConfirmar.Text <> txtPassNuevo.Text Then
            ErrorProvider.SetError(Me.txtPassConfirmar, "Las contraseñas no coinciden")
        Else
            ErrorProvider.SetError(Me.txtPassConfirmar, "")
        End If
    End Sub


    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        If txtPassAnt.Text = obtenerPassAnt() Then
            If txtPassNuevo.Text = txtPassConfirmar.Text Then
                If guardarPass() Then
                    MsgBox("Contraseña cambiada exitosamente")
                    cierreMes.Enabled = True
                    Me.Close()


                Else
                    'MsgBox("Por favor seleccione una contraseña distinta")
                    'txtPassAnt.Text = ""
                    'txtPassNuevo.Text = ""
                    'txtPassConfirmar.Text = ""
                    lblMensaje.Visible = True
                    lblMensaje.Text = "Verifique los errores antes de continuar"
                End If
            Else
                lblMensaje.Visible = True
                lblMensaje.Text = "Verifique los errores antes de continuar"
            End If
        Else
            lblMensaje.Visible = True
            lblMensaje.Text = "Verifique los errores antes de continuar"

        End If

    End Sub

    Private Sub btnAtras_Click(sender As System.Object, e As System.EventArgs) Handles btnAtras.Click
        'Form1.Enabled = True
        CierreMes.Enabled = True
        Me.Close()
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing

        ' Estos son los valores posibles
        Select Case e.CloseReason
            Case CloseReason.ApplicationExitCall
            Case CloseReason.FormOwnerClosing
            Case CloseReason.MdiFormClosing
            Case CloseReason.None
            Case CloseReason.TaskManagerClosing
            Case CloseReason.UserClosing
                cierreMes.Enabled = True
            Case CloseReason.WindowsShutDown
        End Select
    End Sub
End Class